<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\LeagueController;
use App\Http\Controllers\FootballMatchController;
use App\Http\Controllers\LoginController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('getLeaguegroups' ,[ApplicationController::class,'getLeaguegroups' ] );
Route::get('getLeague' ,[ApplicationController::class,'getLeague' ] );
 Route::get('getLeaguebyid/{id}' ,[ApplicationController::class,'getLeaguebyid' ] );
Route::get('getfootballmatchbyid/{id}' ,[ApplicationController::class,'getfootballmatchbyid' ] );
Route::post('login' ,[LoginController::class,'login' ] );

Route::post('register' ,[LoginController::class,'register' ] );

Route::post('create_football_match_registered_users' ,[ApplicationController::class,'create_football_match_registered_users' ] );

Route::post('create_football_match_registered_groups' ,[ApplicationController::class,'create_football_match_registered_groups' ] );

Route::get('getallfootballmatches' ,[ApplicationController::class,'getallfootballmatches' ] ) ;
Route::get('updateUser' ,[ApplicationController::class,'update_User' ] ) ;
Route::post('create_group' ,[ApplicationController::class,'create_group' ] );
  //Route::group(['middleware' => 'api'], function () {
Route::post('create_groupuser' ,[ApplicationController::class,'create_groupuser' ] );
Route::get('getallfootballmatches' ,[ApplicationController::class,'getallfootballmatches' ] ) ;
Route::get('get_user' ,[ApplicationController::class,'get_user' ] ) ;
Route::get('delete_group' ,[ApplicationController::class,'delete_group' ] ) ;

Route::get('delete_group_users' ,[ApplicationController::class,'delete_group_users' ] ) ;

//});
 // Route::middleware('auth:api')->group( function () {

// });