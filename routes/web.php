<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\FootballFieldController;
use App\Http\Controllers\FootballMatchController;
use App\Http\Controllers\LeagueController;
use App\Http\Controllers\LeagueTeamController;
use App\Http\Controllers\LeagueTeamPlayerController;
use App\Http\Controllers\LeagueRegisteredUserController;
use App\Http\Controllers\LeagueRegisteredGroupController;
use App\Http\Controllers\LeagueGroupController;
use App\Http\Controllers\LeagueGroupNameController;
use App\Http\Controllers\LeagueGroupTeamController;
use App\Http\Controllers\LeagueGroupTeamPlayerController;
use App\Http\Controllers\LeagueGroupRegisteredUserController;
use App\Http\Controllers\LeagueGroupRegisteredGroupController;
use App\Http\Controllers\LeagueMatchScheduleController;
use App\Http\Controllers\LeagueMatchPointController;
<<<<<<< HEAD
use App\Http\Controllers\ApplicationController;

=======
use App\Http\Controllers\LeagueGroupMatchScheduleController;
use App\Http\Controllers\LeagueGroupMatchPointController;
use App\Http\Controllers\GroupUserController;
use App\Http\Controllers\GroupController;
>>>>>>> f14405817e86cad95dce38ffcee9e2435b951a9d
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});
Auth::routes();
Route::resource('contacts', 'App\Http\Controllers\ContactController');
Route::resource('products', ProductController::class);
Route::resource('countries', CountryController::class);
Route::resource('cities', CityController::class);
Route::resource('footballFields', FootballFieldController::class);
Route::resource('footballMatches', FootballMatchController::class);
Route::resource('leagues', LeagueController::class);
//additional routes for leagues
Route::get('/leagues/createTeam/{id}', [LeagueController::class, 'createTeam'])->name('leagues.createTeam');
Route::get('/leagues/showFinancial/{league}', [LeagueController::class, 'showFinancial'])->name('leagues.showFinancial');
Route::get('/leagues/createMatches/{league}', [LeagueController::class, 'createMatches'])->name('leagues.createMatches');
Route::get('/leagues/matchesSchedule/{league}', [LeagueController::class, 'matchesSchedule'])->name('leagues.matchesSchedule');
Route::get('/leagues/matchDetails/{leagueTeamMatch}', [LeagueController::class, 'showMatchDetails'])->name('leagues.showMatchDetails');
Route::get('/leagues/matchesPoints/{league}', [LeagueController::class, 'matchesPoints'])->name('leagues.matchesPoints');



Route::resource('leagueTeams', LeagueTeamController::class);
Route::resource('leagueTeamPlayers', LeagueTeamPlayerController::class);
Route::resource('leagueRegisteredUsers', LeagueRegisteredUserController::class);
Route::resource('leagueRegisteredGroups', LeagueRegisteredGroupController::class);
Route::resource('leagueGroups', LeagueGroupController::class);
//additional routes for league groups
Route::get('/leagueGroups/showFinancial/{leagueGroup}', [LeagueGroupController::class, 'showFinancial'])->name('leagueGroups.showFinancial');
Route::get('/leagueGroups/createMatches/{leagueGroup}', [LeagueGroupController::class, 'createMatches'])->name('leagueGroups.createMatches');
Route::get('/leagueGroups/matchesSchedule/{leagueGroup}', [LeagueGroupController::class, 'matchesSchedule'])->name('leagueGroups.matchesSchedule');
Route::get('/leagueGroups/matchDetails/{leagueGroupTeamMatch}', [LeagueGroupController::class, 'showMatchDetails'])->name('leagueGroups.showMatchDetails');
// Route::get('/leagueGroups/createTeam/{id}', [LeagueGroupController::class, 'createTeam'])->name('leagueGroups.createTeam');
Route::get('/leagueGroups/matchesPoints/{leagueGroup}', [LeagueGroupController::class, 'matchesPoints'])->name('leagueGroups.matchesPoints');

Route::resource('leagueGroupNames', LeagueGroupNameController::class);
Route::resource('leagueGroupTeams', LeagueGroupTeamController::class);
Route::resource('leagueGroupTeamPlayers', LeagueGroupTeamPlayerController::class);
Route::resource('leagueGroupRegisteredUsers', LeagueGroupRegisteredUserController::class);
Route::resource('leagueGroupRegisteredGroups', LeagueGroupRegisteredGroupController::class);
Route::resource('leagueMatchSchedules', LeagueMatchScheduleController::class);
Route::resource('leagueMatchPoints', LeagueMatchPointController::class);
Route::resource('leagueGroupMatchSchedules', LeagueGroupMatchScheduleController::class);
Route::resource('leagueGroupMatchPoints', LeagueGroupMatchPointController::class);
Route::resource('users', UserController::class);
Route::resource('groupUsers', GroupUserController::class);
Route::resource('groups', GroupController::class);

//Route::get('getallfootballmatches' ,[ApplicationController::class,'getallfootballmatches' ] ) ;

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});
