<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeagueRegisteredGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_registered_groups', function (Blueprint $table) {
            $table->id();
            $table->double('price');
            $table->foreignId('group_id')
                ->nullable()
                ->constrained('groups')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('league_id')
                ->nullable()
                ->constrained('leagues')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreignId('user_id')
                ->nullable()
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreignId('payment_method_id')
                ->nullable()
                ->constrained('payment_methods')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('league_reg_groups');
    }
}
