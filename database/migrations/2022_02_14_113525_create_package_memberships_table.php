<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_memberships', function (Blueprint $table) {
            $table->id();
            $table->double('price');
            $table->double('match_price');
            $table->string('duration_in_days');
            $table->string('matches_number');
            $table->string('background_image');
            $table->foreignId('country_id')
                ->nullable()
                ->constrained('countries')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('city_id')
                ->nullable()
                ->constrained('cities')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pakage_memberships');
    }
}
