<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFootballMatchRegOwnerPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('football_match_reg_owner_players', function (Blueprint $table) {
            $table->id();
            $table->double('price');
            $table->foreignId('football_match_id')
                ->nullable()
                ->constrained('football_matches')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('owner_player_id')
                ->nullable()
                ->constrained('owner_players')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('football_match_reg_owner_players');
    }
}
