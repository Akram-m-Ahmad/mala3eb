<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeagueGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_groups', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('name_ar');
            $table->integer('team_players_number');
            $table->string('start_at');
            $table->boolean('isHome');
            $table->double('price');
            $table->integer('groups_number');
            $table->integer('teams_number');
            $table->string('details');
            $table->string('details_ar');
            $table->string('main_image');
            $table->date('join_start_at');
            $table->date('join_end_at');
            $table->foreignId('city_id')
                ->nullable()
                ->constrained('cities')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('football_field_id')
                ->nullable()
                ->constrained('football_fields')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('league_groups');
    }
}
