<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeagueTeamMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_team_matches', function (Blueprint $table) {
            $table->id();
            $table->foreignId('league_team1_id')
            ->nullable()
            ->constrained('league_teams')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->foreignId('league_team2_id')
            ->nullable()
            ->constrained('league_teams')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('league_teams_matches');
    }
}
