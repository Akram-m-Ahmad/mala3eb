<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeagueGroupMatchRoundSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_group_match_round_schedules', function (Blueprint $table) {
            $table->id();
            $table->time('time');
            $table->date('date');
            $table->foreignId('league_g_m_round_id')
                ->nullable()
                ->constrained('league_group_match_rounds')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('league_groups_match_round_scheduals');
    }
}
