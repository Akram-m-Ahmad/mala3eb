<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFootballMatchRegisteredUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('football_match_registered_users', function (Blueprint $table) {
            $table->id();
            $table->double('price');
            $table->foreignId('user_id')
            ->nullable()
            ->constrained('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->foreignId('payment_method_id')
            ->nullable()
            ->constrained('payment_methods')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->foreignId('football_match_id')
            ->nullable()
            ->constrained('football_matches')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('football_match_registered_users');
    }
}
