<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeagueMatchPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_match_points', function (Blueprint $table) {
            $table->id();
            $table->integer('first_team_points');
            $table->integer('second_team_points');
            $table->integer('first_team_goals');
            $table->integer('second_team_goals');
            $table->foreignId('league_team_match_id')
                ->nullable()
                ->constrained('league_team_matches')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('league_matches_points');
    }
}
