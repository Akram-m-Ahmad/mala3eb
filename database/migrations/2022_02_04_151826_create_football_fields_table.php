<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFootballFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('football_fields', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('name_ar');
            $table->decimal('lng', 11, 7);
            $table->decimal('lat', 10, 7);
            $table->string('first_image');
            $table->string('second_image');
            $table->string('third_image');
            $table->string('location');
            $table->string('location_ar');
            $table->string('details');
            $table->string('details_ar');
            $table->string('area');
            $table->boolean('isActive');
            $table->foreignId('city_id')
                ->nullable()
                ->constrained('cities')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('user_id')
                ->nullable()
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('football_fields');
    }
}
