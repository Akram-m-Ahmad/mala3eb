<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FootballMatchReview extends Model
{
    use HasFactory;
    protected $fillable = ['rating','comment','football_match_id','user_id'];

    public function football_match(){
        return $this->belongsTo('App\Models\FootballMatch');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
