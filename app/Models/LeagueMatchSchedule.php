<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueMatchSchedule extends Model
{
    use HasFactory;
    protected $fillable = ['time', 'date', 'league_team_match_id'];

    public function league_team_match()
    {
        return $this->belongsTo('App\Models\LeagueTeamMatch');
    }
}
