<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'api_token',
        'phone',
        'country_id',
        'city_id',
        'date_of_birth',
        'profile_picture',
        'isActive',
        'cancel_count',
        'position'

    ];

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    //users that own football fields (owners only)
    public function football_fields()
    {
        return $this->hasMany('App\Models\FootballField');
    }
    //players added by owners
    public function owner_players()
    {
        return $this->hasMany('App\Models\OwnerPlayer');
    }

    //this is to get groups
    public function groups()
    {
        return $this->hasMany('App\Models\Group');
    }
    //this is to get group users 
    public function group_users()
    {
        return $this->belongsToMany('App\Models\Group', 'groups_users')
            ->withPivot('user_id', 'group_id', 'created_at', 'updated_at');
    }

    //matches
    public function football_match_registered_users()
    {
        return $this->belongsToMany('App\Models\FootballMatch', ' football_match_registered_users')
            ->withPivot('price', 'user_id', 'payment_method_id', 'football_match_id', 'created_at', 'updated_at');
    }

    public function football_match_registered_groups()
    {
        return $this->belongsToMany('App\Models\FootballMatch', ' football_match_registered_groups')
            ->withPivot('price', 'user_id', 'payment_method_id', 'football_match_id', 'group_id', 'created_at', 'updated_at');
    }

    // public function football_Owner()
    // {
    //     return $this->hasMany('App\Models\Football_Owner');
    // }

    //league
    public function league_registered_users()
    {
        return $this->belongsToMany('App\Models\League' . 'league_registered_users')
            ->withPivot('price', 'user_id', 'payment_method_id', 'league_id');
    }

    public function league_registered_groups()
    {
        return $this->belongsToMany('App\Models\League', 'league_registered_groups')
            ->withPivot('price', 'user_id', 'payment_method_id', 'league_id', 'group_id');
    }

    //league team player
    public function league_team_players()
    {
        return $this->belongsToMany('App\Models\LeagueTeam', 'league_team_players')
            ->withPivot('user_id', 'league_team_id');
    }

    //league groups
    public function league_group_registered_users()
    {
        return $this->belongsToMany('App\Models\LeagueGroup', 'league_group_registered_users')
            ->withPivot('price', 'user_id', 'payment_method_id', 'league_group_id');
    }

    public function league_group_registered_groups()
    {
        return $this->belongsToMany('App\Models\LeagueGroup', 'league_group_registered_groups')
            ->withPivot('user_id', 'league_group_id', 'group_id', 'price', 'payment_method_id');
    }

    //user in league group team
    public function league_group_team_player()
    {
        return $this->belongsToMany('App\Models\LeagueGroupTeam', 'league_group_team_players')
            ->withPivot('user_id', 'league_group_team_id');
    }

    //membership
    public function membership_user()
    {
        return $this->belongsToMany('App\Models\PackageMembership', 'membership_users')
            ->withPivot(
                'price',
                'start_at',
                'end_at',
                'remaining',
                'user_id',
                'pakage_membership_id',
                'payment_method_id'
            );
    }


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
