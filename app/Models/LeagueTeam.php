<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueTeam extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'name_ar', 'flag_icon', 'league_id'];


    public function league()
    {
        return $this->belongsTo('App\Models\League');
    }

    //users in this team
    public function league_team_players()
    {
        return $this->belongsToMany('App\Models\User', 'league_team_players')
            ->withPivot('id','user_id', 'league_team_id', 'created_at', 'updated_at');
    }

    public function league_team_matches1()
    {
        return $this->hasMany('App\Models\LeagueTeamMatch', 'league_team1_id');
    }
    public function league_team_matches2()
    {
        return $this->hasMany('App\Models\LeagueTeamMatch', 'league_team2_id');
    }
}
