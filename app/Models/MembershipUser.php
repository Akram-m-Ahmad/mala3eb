<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MembershipUser extends Model
{
    use HasFactory;
    protected $fillable = [
        'price', 'start_at', 'end_at', 'remaining', 'user_id',
        'pakage_membership_id', 'payment_method_id'
    ];
    public function users()
    {
        return $this->belongTo('App\Models\User');
    }

    public function pakage_membership()
    {
        return $this->belongTo('App\Models\PakageMembership');
    }
    public function payment_Methods()
    {
        return $this->belongTo('App\Models\PaymentMethod');
    }
}
