<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueGroupName extends Model
{
   use HasFactory;
   protected $fillable = ['name', 'name_ar', 'league_group_id'];

   public function league_group()
   {
      return $this->belongsTo('App\Models\LeagueGroup');
   }

   public function league_group_teams()
   {
      return $this->hasMany('App\Models\LeagueGroupTeam');
   }
}
