<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class FootballMatchRegOwnerPlayer extends Pivot
{
    use HasFactory;

    protected $fillable = ['price','football_match_id','owner_player_id'];


}
