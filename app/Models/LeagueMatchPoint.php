<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueMatchPoint extends Model
{
    use HasFactory;
    protected $fillable = [
        'first_team_points', 'second_team_points',
        'first_team_goals', 'second_team_goals',
        'league_team_match_id'
    ];
    
    public function league_team_match()
    {
        return $this->belongTo('App\Models\LeagueTeamMatch');
    }
}
