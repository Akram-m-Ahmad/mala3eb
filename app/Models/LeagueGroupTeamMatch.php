<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueGroupTeamMatch extends Model
{
    use HasFactory;
    protected $fillable = ['league_group_team1_id', 'league_group_team2_id'];


    public function league_group_match_point()
    {
        return $this->hasOne('App\Models\LeagueGroupMatchPoint');
    }
    public function league_group_match_schedule()
    {
        return $this->hasOne('App\Models\LeagueGroupMatchSchedule');
    }

    public function league_group_team1()
    {
        return $this->belongsTo('App\Models\LeagueGroupTeam', 'league_group_team1_id');
    }
    public function league_group_team2()
    {
        return $this->belongsTo('App\Models\LeagueGroupTeam', 'league_group_team2_id');
    }
}
