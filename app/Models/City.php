<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'name_ar',
        'country_id',

    ];
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
    public function football_fields()
    {
        return $this->hasMany('App\Models\FootballField');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function leagues()
    {
        return $this->hasMany('App\Models\League');
    }
    public function league_gorup()
    {
        return $this->hasMany('App\Models\LeagueGroup');
    }
    public function package_membership()
    {
        return $this->hasMany('App\Models\PackageMembership');
    }
}
