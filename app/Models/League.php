<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class League extends Model
{
    use HasFactory;
    protected $fillable = [

        'name', 'name_ar', 'isHome', 'price', 'details', 'details_ar',
        'teams_number', 'team_players_number', 'start_at', 'join_start_at', 'join_end_at',
        'football_field_id', 'city_id', 'main_image'
    ];

    public function football_field()
    {
        return $this->belongsTo('App\Models\FootballField');
    }
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    //registered users
    public function league_registered_users()
    {
        return $this->belongsToMany('App\Models\User', 'league_registered_users')
            ->withPivot('id','price', 'user_id', 'payment_method_id', 'league_id','created_at','updated_at');
    }

    //registered users as groups
    public function league_registered_groups()
    {
        return $this->belongsToMany('App\Models\User', 'league_registered_groups')
            ->withPivot('id','price', 'user_id', 'payment_method_id', 'league_id', 'group_id','created_at','updated_at');
    }

    //registered groups by users
    public function  groups()
    {
        return $this->belongsToMany('App\Models\Group', 'league_registered_groups')
            ->withPivot('id','price', 'user_id', 'payment_method_id', 'league_id', 'group_id','created_at','updated_at');
    }

    //league teams
    public function league_teams()
    {
        return $this->hasMany('App\Models\LeagueTeam')->orderByDesc('id');
    }
}
