<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupsUser extends Model
{
    use HasFactory;
    protected $table = 'groups_users';
    protected $fillable = [
        'user_id',
        'group_id',  
     ];
}
