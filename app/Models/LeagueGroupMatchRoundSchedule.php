<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueGroupMatchRoundSchedule extends Model
{
    use HasFactory;
    protected $fillable = ['time', 'date', 'league_group_match_round_id'];

    public function league_group_match_round()
    {
        return $this->belongsTo('App\Models\LeagueGroupMatchRound');
    }
}
