<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class FootballMatchRegisteredGroup extends Pivot
{
    use HasFactory;
    protected $table= 'football_match_registered_groups';

    protected $fillable = ['price', 'user_id', 'payment_method_id', 'football_match_id', 'group_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function payment_method()
    {
        return $this->belongsTo('App\Models\PaymentMethod');
    }
    // public function football_match()
    // {
    //     return $this->belongTo('App\Models\FootballMatch');
    // }
    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }
}
