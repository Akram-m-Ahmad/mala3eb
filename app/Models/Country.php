<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'name_ar', 'country_code', 'flag_icon',
        'tax_percentage', 'unit', 'unit_ar'
    ];

    public function cities()
    {
        return $this->hasMany('App\Models\City');
    }
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function package_membership()
    {
        return $this->hasMany('App\Models\PackageMembership');
    }
}
