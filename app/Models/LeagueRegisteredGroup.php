<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueRegisteredGroup extends Model
{
    use HasFactory;
    protected $fillable = ['price', 'user_id', 'payment_method_id', 'league_id', 'group_id'];
    public function users()
    {
        return $this->belongTo('App\Models\User');
    }
    public function payment_Methods()
    {
        return $this->belongTo('App\Models\PaymentMethod');
    }
    public function league()
    {
        return $this->belongTo('App\Models\League');
    }
    public function groups()
    {
        return $this->belongTo('App\Models\Group');
    }
}
