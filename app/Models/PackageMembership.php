<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageMembership extends Model
{
    use HasFactory;
    protected $fillable = [
        'price', 'match_price', 'duration_in_days',
        'matches_number', 'country_id', 'city_id','background_image'
    ];


    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
    public function city()
    {
        return $this->belongTo('App\Models\City');
    }
    public function membership_user()
    {
        return $this->belongsToMany('App\Models\User', 'membership_users')
            ->withPivot(
                'price',
                'start_at',
                'end_at',
                'remaining',
                'user_id',
                'pakage_membership_id',
                'payment_method_id'
            );
    }
}
