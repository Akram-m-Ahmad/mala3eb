<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueGroup extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'name_ar', 'team_players_number',
        'start_at', 'isHome', 'price', 'groups_number', 'teams_number',
        'details', 'details_ar', 'city_id', 'football_field_id',
        'join_start_at', 'join_end_at','main_image'
    ];

    public function football_field()
    {
        return $this->belongsTo('App\Models\FootballField');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function league_group_names()
    {
        return $this->hasMany('App\Models\LeagueGroupName');
    }

    //registered users
    public function league_group_registered_users()
    {
        return $this->belongsToMany('App\Models\User', 'league_group_registered_users')
            ->withPivot('id', 'price', 'user_id', 'payment_method_id', 'league_group_id','created_at','updated_at');
    }

    //registered users as a group
    public function league_group_registered_groups()
    {
        return $this->belongsToMany('App\Models\User', 'league_group_registered_groups')
            ->withPivot('id','user_id', 'league_group_id', 'group_id', 'price', 'payment_method_id','created_at','updated_at');
    }

    //registered groups
    public function  groups()
    {
        return $this->belongsToMany('App\Models\Group', 'league_group_registered_groups')
            ->withPivot('id','user_id', 'league_group_id', 'group_id', 'price', 'payment_method_id','created_at','updated_at');
    }
}
