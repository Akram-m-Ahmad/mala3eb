<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    protected $fillable = [

        'name',
        'name_ar',
        'profile_picture'
        //'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    // public function group_Users(){
    //     return $this->hasMany('App\Models\Group_Users');
    // }

    public function group_users()
    {
        return $this->belongsToMany('App\Models\User', 'groups_users')
            ->withPivot('id', 'user_id', 'group_id', 'created_at', 'updated_at');
    }

    //registered groups in a match
    public function football_match_registered_groups()
    {
        return $this->belongsToMany('App\Models\FootballMatch', 'football_match_registered_groups')
            ->withPivot('id','price', 'user_id', 'payment_method_id', 'football_match_id', 'group_id', 'created_at', 'updated_at');;
    }

      //registered groups in a league
    public function league_registered_groups()
    {
        return $this->belongsToMany('App\Models\League', 'league_registered_groups')
            ->withPivot('id','price', 'user_id', 'payment_method_id', 'league_id', 'group_id','created_at', 'updated_at');
    }

      //registered groups in a league group
    public function league_group_registered_groups()
    {
        return $this->belongsToMany('App\Models\LeagueGroup', 'league_group_registered_groups')
            ->withPivot('id','user_id', 'league_group_id', 'group_id','created_at', 'updated_at');
    }
}
