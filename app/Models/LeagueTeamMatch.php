<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueTeamMatch extends Model
{
    use HasFactory;
    protected $fillable = ['league_team1_id', 'league_team2_id'];

    public function league_match_point()
    {
        return $this->hasOne('App\Models\LeagueMatchPoint');
    }
    public function league_match_schedule()
    {
        return $this->hasOne('App\Models\LeagueMatchSchedule');
    }

    public function league_teams1()
    {
        return $this->belongsTo('App\Models\LeagueTeam', 'league_team1_id');
    }
    public function league_teams2()
    {
        return $this->belongsTo('App\Models\LeagueTeam', 'league_team2_id');
    }
}
