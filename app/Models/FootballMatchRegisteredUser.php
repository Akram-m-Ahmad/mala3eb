<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class FootballMatchRegisteredUser extends Pivot
{
    use HasFactory;
    protected $table= 'football_match_registered_users';

    protected $fillable = ['price', 'user_id', 'payment_method_id', 'football_match_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function payment_method()
    {
        return $this->belongsTo('App\Models\PaymentMethod');
    }

    // public function football_Match()
    // {
    //     return $this->belongTo('App\Models\Football_Match');
    // }
}
