<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class LeagueTeamPlayer extends Pivot
{
    use HasFactory;
    protected $table = 'league_team_players';

    protected $fillable = ['user_id', 'league_team_id'];

    public function league_team()
    {
        return $this->belongTo('App\Models\LeagueTeam');
    }
    public function user()
    {
        return $this->belongTo('App\Models\User');
    }
}
