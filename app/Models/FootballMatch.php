<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FootballMatch extends Model
{
    use HasFactory;
    protected $fillable = ['time', 'date', 'price', 'capacity', 'football_field_id'];

    //football field
    public function football_field()
    {
        return $this->belongsTo('App\Models\FootballField');
    }

    //users registered
    public function football_match_registered_users()
    {
        return $this->belongsToMany('App\Models\User', 'football_match_registered_users')
        ->withPivot('price', 'user_id', 'payment_method_id', 'football_match_id','created_at','updated_at');
    }

    public function football_match_registered_groups()
    {
        return $this->belongsToMany('App\Models\User', 'football_match_registered_groups')
        ->withPivot('price', 'user_id', 'payment_method_id', 'football_match_id', 'group_id','created_at','updated_at');
    }

    //groups registered
    public function groups()
    {
        return $this->belongsToMany('App\Models\Group', 'football_match_registered_groups')
        ->withPivot('price', 'user_id', 'payment_method_id', 'football_match_id', 'group_id','created_at','updated_at');;
    }

    // registered players by owners
    public function football_match_registered_owner_players()
    {
        return $this->belongsToMany('App\Models\OwnerPlayer', 'football_match_reg_owner_players')
        ->withPivot('price','football_match_id','owner_player_id');
    }
}
