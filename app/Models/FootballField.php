<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FootballField extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'name_ar', 'lng', 'lat', 'first_image',
        'second_image', 'third_image', 'location', 'location_ar', 'area',
        'details', 'details_ar', 'city_id', 'isActive', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function football_matches()
    {
        return $this->hasMany('App\Models\FootballMatch');
    }
    public function league_groups()
    {
        return $this->hasMany('App\Models\LeagueGroup');
    }
    public function leagues()
    {
        return $this->hasMany('App\Models\League');
    }
}
