<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueGroupTeam extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'name_ar', 'flag_icon', 'league_group_name_id'];


    public function league_group_name()
    {
        return $this->belongsTo('App\Models\LeagueGroupName');
    }

    //users in league group team
    public function league_group_team_players()
    {
        return $this->belongsToMany('App\Models\User', 'league_group_team_players')
            ->withPivot('id','user_id', 'league_group_team_id');
    }

    public function league_group_team_match1()
    {
        return $this->hasMany('App\Models\LeagueGroupTeamMatch', 'league_group_team1_id');
    }
    public function league_group_team_match2()
    {
        return $this->hasMany('App\Models\LeagueGroupTeamMatch', 'league_group_team2_id');
    }
}
