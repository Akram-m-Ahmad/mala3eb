<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class LeagueGroupRegisteredGroup extends Pivot
{
    use HasFactory;
    protected $fillable = ['user_id', 'league_group_id', 'group_id', 'price', 'payment_method_id'];

    public function users()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function league_group()
    {
        return $this->belongsTo('App\Models\LeagueGroup');
    }
    public function groups()
    {
        return $this->belongsTo('App\Models\Group');
    }
}
