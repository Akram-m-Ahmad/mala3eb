<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class LeagueGroupRegisteredUser extends Pivot
{
    use HasFactory;

    protected $table = 'league_group_registered_users';
    protected $fillable = ['price', 'user_id', 'payment_method_id', 'league_group_id'];

    public function users()
    {
        return $this->belongTo('App\Models\User');
    }

    public function payment_method()
    {
        return $this->belongTo('App\Models\PaymentMethod');
    }

    public function league_group()
    {
        return $this->belongTo('App\Models\LeagueGroup');
    }
}
