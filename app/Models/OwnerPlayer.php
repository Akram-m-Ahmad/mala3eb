<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OwnerPlayer extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'phone', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function football_matches()
    {
        return $this->belongsToMany('App\Models\FootballMatch', 'football_match_reg_owner_players')
        ->withPivot('price','football_match_id','owner_player_id');
    }
}
