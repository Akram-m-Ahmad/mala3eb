<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class LeagueGroupTeamPlayer extends Pivot
{
    use HasFactory;
    protected $table = 'league_group_team_players';
    protected $fillable = ['user_id', 'league_group_team_id'];

    public function league_group_team()
    {
        return $this->belongTo('App\Models\LeagueGroupTeam');
    }
    public function users()
    {
        return $this->belongTo('App\Models\User');
    }
}
