<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueGroupMatchPoint extends Model
{
   use HasFactory;
   protected $fillable = [
      'first_team_points', 'second_team_points', 'first_team_goals',
      'second_team_goals',
      'league_group_team_match_id'
   ];

   public function league_group_team_match()
   {
      return $this->belongsTo('App\Models\LeagueGroupTeamMatch');
   }
}
