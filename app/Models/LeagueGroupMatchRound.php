<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueGroupMatchRound extends Model
{
    use HasFactory;
    protected $fillable = ['league_group_team1_id', 'league_group_team2_id', 'round_type_id'];

    public function round_type()
    {
        return $this->belongsTo('App\Models\RoundType');
    }

    public function league_group_team1()
    {
        return $this->belongsTo('App\Models\LeagueGroupTeam', 'league_group_team1_id');
    }
    public function league_group_team2()
    {
        return $this->belongsTo('App\Models\LeagueGroupTeam', 'league_group_team1_id');
    }
    public function league_group_match_round_point()
    {
        return $this->hasOne('App\Models\LeagueGroupMatchRoundPoint');
    }
    public function league_group_match_round_schedule()
    {
        return $this->hasOne('App\Models\LeagueGroupMatchRoundSchedule');
    }
}
