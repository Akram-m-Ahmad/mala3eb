<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Models\City;
use App\Models\Country;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator');
    }

    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index()
    {
     
        $search = request()->query('search');
        if ($search) {
            $users = User::whereHas('country', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhereHas('city', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhere('name', 'LIKE', '%' . $search . '%')
                ->orWhere('phone', 'LIKE', '%' . $search . '%')
                ->orWhere('email', 'LIKE', '%' . $search . '%')
                ->orWhere('date_of_birth', 'LIKE', '%' . $search . '%')
                ->orWhere('position', 'LIKE', '%' . $search . '%')
                ->orWhere('position_ar', 'LIKE', '%' . $search . '%')
                ->latest()->paginate(5);
            return view('admin.users.index', compact('users'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        } else {
            $search = null;
            $users = User::latest()->paginate(5);
            return view('admin.users.index', compact('users'))
            ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        }
    }

    public function create(){
        $roles = Role::all();
        $countries = Country::latest()->get();
        $cities = City::latest()->get();
        return view('admin.users.create')->with('roles',$roles)
        ->with('countries',$countries)
        ->with('cities',$cities);
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'password' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'role' => 'required',
        ]);

        $user = new User;
        $user->name = $request->input('name');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->country_id = $request->input('country_id');
        $user->city_id = $request->input('city_id');
        $user->password = Hash::make($request->input('password'));
        $user->api_token = Str::random(80);
        $user->profile_picture = 'noimage.jpg';
        $user->isActive = true;
        $user->save();

        $user->attachRole($request->input('role'));

        return redirect('/users')->with('success','user successfully added');

    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }
}
