<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueGroupTeamPlayerStoreRequest;
use App\Models\LeagueGroupTeam;
use App\Models\LeagueGroupTeamPlayer;
use Illuminate\Http\Request;

class LeagueGroupTeamPlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leagueGroupTeamSelected = LeagueGroupTeam::where('id', request()->league_group_team_id)->first();
        $leagueGroup = $leagueGroupTeamSelected->league_group_name->league_group;
        $usersIds = array();
        //    $test = $leagueTeam->league->with('league_teams.league_team_players')->get();
        //  $test->first()->league_teams->first()->league_team_players;
        foreach ($leagueGroup->league_group_names as $leagueGroupName) {
            foreach ($leagueGroupName->league_group_teams as $leagueGroupTeam) {
                foreach ($leagueGroupTeam->league_group_team_players as $user)
                    $usersIds[] = $user->id;
            }
        }
 
       
        return view('admin.leagueGroupTeamPlayers.create', compact('leagueGroupTeamSelected'))
            ->with('usersIds', $usersIds);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeagueGroupTeamPlayerStoreRequest $request)
    {
        $vaildated = $request->validated();
        foreach ($request->users as $user) {
            $leagueGroupTeamPlayer = new LeagueGroupTeamPlayer;
            $leagueGroupTeamPlayer->league_group_team_id = $request->league_group_team_id;
            $leagueGroupTeamPlayer->user_id = $user;
            $leagueGroupTeamPlayer->save();
        }

        return redirect()->back()
            ->with('success', 'تم اضافة اللاعبين بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueGroupTeamPlayer $leagueGroupTeamPlayer)
    {
        $leagueGroupTeamPlayer->delete();
        return redirect()->back()
            ->with('delete', 'تم حذف اللاعب بنجاح ');
    }
}
