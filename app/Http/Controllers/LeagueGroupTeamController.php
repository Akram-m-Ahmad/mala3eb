<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueGroupTeamStoreRequest;
use App\Http\Requests\LeagueGroupTeamUpdateRequest;
use App\Models\LeagueGroup;
use App\Models\LeagueGroupName;
use Illuminate\Http\Request;
use App\Models\LeagueGroupTeam;
use Illuminate\Support\Facades\File;


class LeagueGroupTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->query('search');
        if ($search) {
            $leagueGroupTeams = LeagueGroupTeam::whereHas('league_group_name', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhereHas('league_group_name.league_group', function ($query1) use ($search) {
                $query1->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhere('name', 'LIKE', '%' . $search . '%')
                ->orWhere('name_ar', 'LIKE', '%' . $search . '%')
                ->latest()->paginate(5);
            return view('admin.leagueGroupTeams.index', compact('leagueGroupTeams'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        } else {
            $search = null;
            $leagueGroupTeams = LeagueGroupTeam::latest()->paginate(5);
            return view('admin.leagueGroupTeams.index', compact('leagueGroupTeams'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $leagueGroupNames = request()->league_group_id ?
            LeagueGroupName::where('league_group_id', request()->league_group_id)->get() : LeagueGroupName::latest()->get();
        return view('admin.leagueGroupTeams.create', compact('leagueGroupNames'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeagueGroupTeamStoreRequest $request)
    {
        $validated = $request->validated();

        $input = $request->all();

        if ($image = $request->file('flag_icon')) {
            $destinationPath = 'storage/images/leagueGroupTeams/';
            $profileImage = date('YmdHis') . "-" .  $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['flag_icon'] = "$profileImage";
        }

        LeagueGroupTeam::create($input);

        return redirect()->route('leagueGroupTeams.index')
            ->with('success', 'تم اضافة الفريق بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LeagueGroupTeam $leagueGroupTeam)
    {
        return view('admin.leagueGroupTeams.show', compact('leagueGroupTeam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LeagueGroupTeam $leagueGroupTeam)
    {
        $leagueGroupNames = LeagueGroupName::latest()->get();
        return view('admin.leagueGroupTeams.edit', compact('leagueGroupTeam'))
            ->with('leagueGroupNames', $leagueGroupNames);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LeagueGroupTeamUpdateRequest $request, LeagueGroupTeam $leagueGroupTeam)
    {
        $validated = $request->validated();

        $input = $request->all();

        if ($image = $request->file('flag_icon')) {
            $destinationPath = 'storage/images/leagueGroupTeams/';
            $profileImage = date('YmdHis') . "-" .  $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['flag_icon'] = "$profileImage";
            if (File::exists(public_path('storage/images/leagueGroupTeams/' . $leagueGroupTeam->flag_icon))) {
                File::delete(public_path('storage/images/leagueGroupTeams/' . $leagueGroupTeam->flag_icon));
            }
        } else {
            $input['flag_icon'] = "$leagueGroupTeam->flag_icon";
        }



        $leagueGroupTeam->update($input);

        return redirect()->route('leagueGroupTeams.index')
            ->with('success', 'تم تعديل الفريق بنجاح ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueGroupTeam $leagueGroupTeam)
    {
        if (File::exists(public_path('storage/images/leagueTeams/' . $leagueGroupTeam->flag_icon))) {
            File::delete(public_path('storage/images/leagueTeams/' . $leagueGroupTeam->flag_icon));
        }
        $leagueGroupTeam->delete();
        return redirect()->back()
            ->with('delete', 'تم حذف الفريق بنجاح ');
    }
}
