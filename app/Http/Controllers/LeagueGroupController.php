<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueGroupStoreRequest;
use App\Http\Requests\LeagueGroupUpdateRequest;
use App\Models\City;
use App\Models\FootballField;
use Illuminate\Http\Request;
use App\Models\LeagueGroup;
use App\Models\LeagueGroupName;
use App\Models\LeagueGroupTeam;
use App\Models\LeagueGroupTeamMatch;
use Illuminate\Support\Facades\File;

class LeagueGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->query('search');
        if ($search) {
            $leagueGroups = LeagueGroup::whereHas('football_field', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhereHas('city', function ($query1) use ($search) {
                $query1->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhereHas('city.country', function ($query2) use ($search) {
                $query2->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhere('name', 'LIKE', '%' . $search . '%')
                ->orWhere('name_ar', 'LIKE', '%' . $search . '%')
                ->orWhere('price', 'LIKE', '%' . $search . '%')
                ->orWhere('details', 'LIKE', '%' . $search . '%')
                ->orWhere('details_ar', 'LIKE', '%' . $search . '%')
                ->orWhere('groups_number', 'LIKE', '%' . $search . '%')
                ->orWhere('teams_number', 'LIKE', '%' . $search . '%')
                ->orWhere('team_players_number', 'LIKE', '%' . $search . '%')
                ->orWhere('start_at', 'LIKE', '%' . $search . '%')
                ->orWhere('join_start_at', 'LIKE', '%' . $search . '%')
                ->orWhere('join_end_at', 'LIKE', '%' . $search . '%')
                ->latest()->paginate(5);
                return view('admin.leagueGroups.index', compact('leagueGroups'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        } else {
            $search = null;
            $leagueGroups = LeagueGroup::latest()->paginate(5);
            return view('admin.leagueGroups.index', compact('leagueGroups'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $footballFields = FootballField::latest()->get();
        $cities = City::latest()->get();
        return view('admin.leagueGroups.create')
            ->with('footballFields', $footballFields)
            ->with('cities', $cities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeagueGroupStoreRequest $request)
    {
        $validated = $request->validated();

        $input = $request->all();

        if ($image = $request->file('main_image')) {
            $destinationPath = 'storage/images/leagueGroups/';
            $profileImage = date('YmdHis') . "-" .  $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['main_image'] = "$profileImage";
        }

        LeagueGroup::create($input);

        return redirect()->route('leagueGroups.index')
            ->with('success', 'تم اضافة دوري المجموعات بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LeagueGroup $leagueGroup)
    {

        $usersIds = array();
        //    $test = $leagueTeam->league->with('league_teams.league_team_players')->get();
        //  $test->first()->league_teams->first()->league_team_players;
        foreach ($leagueGroup->league_group_names as $leagueGroupName) {
            foreach ($leagueGroupName->league_group_teams as $leagueGroupTeam) {
                foreach ($leagueGroupTeam->league_group_team_players as $user)
                    $usersIds[] = $user->id;
            }
        }
        //    return LeagueGroup::with(['league_group_names' => function($query){
        //     $query->count('id');
        //  }])->get();
        $leagueGroupCountTeams = 0;
        $countGroupsPlayers = 0;

        foreach ($leagueGroup->groups as $group) {
            $countGroupsPlayers += $group->group_users->count() + 1;
        }


        $leagueGroupNamesTeams =  LeagueGroup::where('id', $leagueGroup->id)
            ->with(['league_group_names' => function ($query) {
                $query->with('league_group_teams');
            }])->first();

        foreach ($leagueGroupNamesTeams->league_group_names as $leagueGroupName) {
            $leagueGroupCountTeams +=  $leagueGroupName->league_group_teams->count();
        }

        $countScheduledMatches = 0;
        foreach ($leagueGroup->league_group_names as $leagueGroupName) {
            foreach ($leagueGroupName->league_group_teams as $leagueGroupTeam) {
                $countScheduledMatches +=  $leagueGroupTeam->league_group_team_match1->count();
            }
        }


        return view('admin.leagueGroups.show', compact('leagueGroup'))
            ->with('leagueGroupCountTeams', $leagueGroupCountTeams)
            ->with('leagueGroupNamesTeams', $leagueGroupNamesTeams)
            ->with('countScheduledMatches', $countScheduledMatches)
            ->with('usersIds', $usersIds);
    }

    public function showFinancial(LeagueGroup $leagueGroup)
    {
        return view('admin.leagueGroups.showFinancial', compact('leagueGroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LeagueGroup $leagueGroup)
    {
        $footballFields = FootballField::latest()->get();
        $cities = City::latest()->get();
        return view('admin.leagueGroups.edit', compact('leagueGroup'))
            ->with('footballFields', $footballFields)
            ->with('cities', $cities);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LeagueGroupUpdateRequest $request, LeagueGroup $leagueGroup)
    {
        $validated = $request->validated();

        $input = $request->all();

        if ($image = $request->file('main_image')) {
            $destinationPath = 'storage/images/leagueGroups/';
            $profileImage = date('YmdHis') . "-" .  $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['main_image'] = "$profileImage";
            if (File::exists(public_path('storage/images/leagueGroups/' . $leagueGroup->main_image))) {
                File::delete(public_path('storage/images/leagueGroups/' . $leagueGroup->main_image));
            }
        } else {
            $input['main_image'] = "$leagueGroup->main_image";
        }



        $leagueGroup->update($input);

        return redirect()->route('leagueGroups.index')
            ->with('success', 'تم تعديل دوري المجموعات بنجاح ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueGroup $leagueGroup)
    {
        if (File::exists(public_path('storage/images/leagueGroups/' . $leagueGroup->main_image))) {
            File::delete(public_path('storage/images/leagueGroups/' . $leagueGroup->main_image));
        }
        $leagueGroup->delete();
        return redirect()->route('leagueGroups.index')
            ->with('delete', 'تم حذف دوري المجموعات بنجاح ');
    }

    public function createMatches(LeagueGroup $leagueGroup)
    {

        $allLeagueGroups = LeagueGroupName::where('league_group_id', $leagueGroup->id)->get();


        if ($leagueGroup->teams_number === 4) {
            $i = 0;
            $j = 0;
            $firstLeagueGroupTeams = $allLeagueGroups->sortBy('id')->first()->league_group_teams;
            $secondLeagueGroupTeams = $allLeagueGroups->sortByDesc('id')->first()->league_group_teams;
            $tempFirstLeagueGroupTeam = new LeagueGroupTeam;
            $tempSecondLeagueGroupTeam = new LeagueGroupTeam;
            foreach ($firstLeagueGroupTeams as $firstLeagueGroupTeam) {
                if ($i === 0) {
                    $tempFirstLeagueGroupTeam = $firstLeagueGroupTeam;
                    $i++;
                } else {
                    $i++;
                    $leagueGroupTeamMatch = new LeagueGroupTeamMatch;
                    $leagueGroupTeamMatch->league_group_team1_id = $tempFirstLeagueGroupTeam->id;
                    $leagueGroupTeamMatch->league_group_team2_id = $firstLeagueGroupTeam->id;
                    $leagueGroupTeamMatch->save();
                }
            }
            foreach ($secondLeagueGroupTeams as $secondLeagueGroupTeam) {
                if ($j === 0) {
                    $tempSecondLeagueGroupTeam = $secondLeagueGroupTeam;
                    $j++;
                } else {
                    $j++;
                    $leagueGroupTeamMatch = new LeagueGroupTeamMatch;
                    $leagueGroupTeamMatch->league_group_team1_id = $tempSecondLeagueGroupTeam->id;
                    $leagueGroupTeamMatch->league_group_team2_id = $secondLeagueGroupTeam->id;
                    $leagueGroupTeamMatch->save();
                }
            }

            if ($leagueGroup->isHome == 1) {
                $i = 0;
                $j = 0;
                $firstLeagueGroupTeams = $allLeagueGroups->sortBy('id')->first()->league_group_teams;
                $secondLeagueGroupTeams = $allLeagueGroups->sortByDesc('id')->first()->league_group_teams;
                $tempFirstLeagueGroupTeam = new LeagueGroupTeam;
                $tempSecondLeagueGroupTeam = new LeagueGroupTeam;
                foreach ($firstLeagueGroupTeams as $firstLeagueGroupTeam) {
                    if ($i === 0) {
                        $tempFirstLeagueGroupTeam = $firstLeagueGroupTeam;
                        $i++;
                    } else {
                        $i++;
                        $leagueGroupTeamMatch = new LeagueGroupTeamMatch;
                        $leagueGroupTeamMatch->league_group_team1_id = $tempFirstLeagueGroupTeam->id;
                        $leagueGroupTeamMatch->league_group_team2_id = $firstLeagueGroupTeam->id;
                        $leagueGroupTeamMatch->save();
                    }
                }
                foreach ($secondLeagueGroupTeams as $secondLeagueGroupTeam) {
                    if ($j === 0) {
                        $tempSecondLeagueGroupTeam = $secondLeagueGroupTeam;
                        $j++;
                    } else {
                        $j++;
                        $leagueGroupTeamMatch = new LeagueGroupTeamMatch;
                        $leagueGroupTeamMatch->league_group_team1_id = $tempSecondLeagueGroupTeam->id;
                        $leagueGroupTeamMatch->league_group_team2_id = $secondLeagueGroupTeam->id;
                        $leagueGroupTeamMatch->save();
                    }
                }
            }
        } else {

            if ($leagueGroup->isHome == 1) {
                foreach ($allLeagueGroups as $leagueGroupName) {
                    foreach ($leagueGroupName->league_group_teams as $leagueGroupTeam) {

                        foreach ($leagueGroupName->league_group_teams as $vsLeagueGroupTeam) {
                            // return $leagueGroupTeam->league_group_team_match1->first(function ($match) use ($leagueGroupTeam, $vsLeagueGroupTeam) {
                            //     return ($match->league_group_team1_id == $leagueGroupTeam->id && $match->league_group_team2_id == $vsLeagueGroupTeam->id)
                            //     || ($match->league_group_team1_id == $vsLeagueGroupTeam->id && $match->league_group_team2_id == $leagueGroupTeam->id);
                            // });;

                            if ($leagueGroupTeam->id == $vsLeagueGroupTeam->id) {
                                continue;
                            } else {
                                $leagueGroupTeamMatch = new LeagueGroupTeamMatch;
                                $leagueGroupTeamMatch->league_group_team1_id = $leagueGroupTeam->id;
                                $leagueGroupTeamMatch->league_group_team2_id = $vsLeagueGroupTeam->id;
                                $leagueGroupTeamMatch->save();
                            }
                        }
                    }
                }
            } else {
                foreach ($allLeagueGroups as $leagueGroupName) {
                    foreach ($leagueGroupName->league_group_teams as $leagueGroupTeam) {
                        foreach ($leagueGroupName->league_group_teams as $vsLeagueGroupTeam) {
                            if ($leagueGroupTeam->id == $vsLeagueGroupTeam->id) {
                                continue;
                            } elseif ((LeagueGroupTeamMatch::where('league_group_team1_id', $leagueGroupTeam->id)->where('league_group_team2_id', $vsLeagueGroupTeam->id)->first())
                                || LeagueGroupTeamMatch::where('league_group_team1_id', $vsLeagueGroupTeam->id)->where('league_group_team2_id', $leagueGroupTeam->id)->first()
                            ) {
                                continue;
                            } else {
                                $leagueGroupTeamMatch = new LeagueGroupTeamMatch;
                                $leagueGroupTeamMatch->league_group_team1_id = $leagueGroupTeam->id;
                                $leagueGroupTeamMatch->league_group_team2_id = $vsLeagueGroupTeam->id;
                                $leagueGroupTeamMatch->save();
                            }
                        }
                    }
                }
            }

            // if ($leagueGroup->isHome == 1) {
            //     foreach ($allLeagueGroups as $leagueGroupName) {
            //         foreach ($leagueGroupName->league_group_teams as $leagueGroupTeam) {
            //             foreach ($leagueGroupName->league_group_teams as $vsLeagueGroupTeam) {
            //                 if ($leagueGroupTeam->id == $vsLeagueGroupTeam->id) {
            //                     continue;
            //                 } else {
            //                     $leagueGroupTeamMatch = new LeagueGroupTeamMatch;
            //                     $leagueGroupTeamMatch->league_group_team1_id = $leagueGroupTeam->id;
            //                     $leagueGroupTeamMatch->league_group_team2_id = $vsLeagueGroupTeam->id;
            //                     $leagueGroupTeamMatch->save();
            //                 }
            //             }
            //         }
            //     }
            // }
        }

        return redirect()->back()->with('success', 'تم إضافة المباريات ');
    }
    public function matchesSchedule(LeagueGroup $leagueGroup)
    {
        $i = 1;
        return view('admin.leagueGroups.matchesSchedule', compact('leagueGroup'))->with('i', $i);
    }

    public function showMatchDetails(LeagueGroupTeamMatch $leagueGroupTeamMatch)
    {
        return view('admin.leagueGroups.showMatchDetails', compact('leagueGroupTeamMatch'));
    }

    public function matchesPoints(LeagueGroup $leagueGroup)
    {



        $teamsScore = array();
        $teamsScore2 = array();
        foreach ($leagueGroup->league_group_names as $leagueGroupName) {
            foreach ($leagueGroupName->league_group_teams as $leagueGroupTeam) {

                $teampTeamarray = array();
                $teampTeamarray2 = array();
                $teampTeamarray['league_group_name_id'] = $leagueGroupName->id;
                $teampTeamarray['league_group_name_name'] = $leagueGroupName->name;
                $teampTeamarray['league_group_name_name_ar'] = $leagueGroupName->name_ar;
                $teampTeamarray['league_group_team_id'] = $leagueGroupTeam->id;
                $teampTeamarray['league_group_team_name'] = $leagueGroupTeam->name;
                $teampTeamarray['league_group_team_name_ar'] = $leagueGroupTeam->name_ar;
                $teampTeamarray2['league_group_team_flag_icon'] = $leagueGroupTeam->flag_icon;
                $teampTeamarray['league_group_team_points'] = 0;
                $teampTeamarray['league_group_team_goals'] = 0;
                $teampTeamarray['league_group_team_goals_on'] = 0;
                $teampTeamarray['league_group_team_goals_diff'] = 0;
                $teampTeamarray['league_group_team_matches_played'] = 0;
                $teampTeamarray['league_group_team_wins'] = 0;
                $teampTeamarray['league_group_team_loses'] = 0;
                $teampTeamarray['league_group_team_draws'] = 0;


                $teampTeamarray2['league_group_name_id'] = $leagueGroupName->id;
                $teampTeamarray2['league_group_name_name'] = $leagueGroupName->name;
                $teampTeamarray2['league_group_name_name_ar'] = $leagueGroupName->name_ar;
                $teampTeamarray2['league_group_team_id'] = $leagueGroupTeam->id;
                $teampTeamarray2['league_group_team_name'] = $leagueGroupTeam->name;
                $teampTeamarray2['league_group_team_name_ar'] = $leagueGroupTeam->name_ar;
                $teampTeamarray2['league_group_team_flag_icon'] = $leagueGroupTeam->flag_icon;
                $teampTeamarray2['league_group_team_points'] = 0;
                $teampTeamarray2['league_group_team_goals'] = 0;
                $teampTeamarray2['league_group_team_goals_on'] = 0;
                $teampTeamarray2['league_group_team_goals_diff'] = 0;
                $teampTeamarray2['league_group_team_matches_played'] = 0;
                $teampTeamarray2['league_group_team_wins'] = 0;
                $teampTeamarray2['league_group_team_loses'] = 0;
                $teampTeamarray2['league_group_team_draws'] = 0;


                // return $leagueGroupTeam->league_group_team_match1;
                foreach ($leagueGroupTeam->league_group_team_match1 as $leagueGroupTeamMatch) {
                    if ($leagueGroupTeamMatch->league_group_match_point) {
                        $teampTeamarray['league_group_team_points'] +=  $leagueGroupTeamMatch->league_group_match_point->first_team_points;
                        $teampTeamarray['league_group_team_goals'] +=  $leagueGroupTeamMatch->league_group_match_point->first_team_goals;
                        $teampTeamarray['league_group_team_goals_on'] +=  $leagueGroupTeamMatch->league_group_match_point->second_team_goals;
                        $teampTeamarray['league_group_team_goals_diff'] += $leagueGroupTeamMatch->league_group_match_point->first_team_goals - $leagueGroupTeamMatch->league_group_match_point->second_team_goals;
                        ++$teampTeamarray['league_group_team_matches_played'];
                        if ($leagueGroupTeamMatch->league_group_match_point->first_team_points === 3) {
                            $teampTeamarray['league_group_team_wins'] += 1;
                        } elseif ($leagueGroupTeamMatch->league_group_match_point->first_team_points === 0) {
                            $teampTeamarray['league_group_team_loses'] += 1;
                        } elseif ($leagueGroupTeamMatch->league_group_match_point->first_team_points === 1) {
                            $teampTeamarray['league_group_team_draws'] += 1;
                        }
                    } else {
                        $teampTeamarray['league_group_team_points'] += 0;
                        $teampTeamarray['league_group_team_goals'] += 0;
                    }
                }
                // return  $teampTeamarray['league_group_team_goals_diff'];
                array_push($teamsScore, $teampTeamarray);

                foreach ($leagueGroupTeam->league_group_team_match2 as $leagueGroupTeamMatch2) {
                    if ($leagueGroupTeamMatch2->league_group_match_point) {
                        $teampTeamarray2['league_group_team_points'] += $leagueGroupTeamMatch2->league_group_match_point->second_team_points;
                        $teampTeamarray2['league_group_team_goals'] += $leagueGroupTeamMatch2->league_group_match_point->second_team_goals;
                        $teampTeamarray2['league_group_team_goals_on'] += $leagueGroupTeamMatch2->league_group_match_point->first_team_goals;
                        $teampTeamarray2['league_group_team_goals_diff'] += $leagueGroupTeamMatch2->league_group_match_point->second_team_goals
                            - $leagueGroupTeamMatch2->league_group_match_point->first_team_goals;
                        ++$teampTeamarray2['league_group_team_matches_played'];
                        if ($leagueGroupTeamMatch2->league_group_match_point->second_team_points === 3) {
                            $teampTeamarray2['league_group_team_wins'] += 1;
                        } elseif ($leagueGroupTeamMatch2->league_group_match_point?->second_team_points === 0) {
                            $teampTeamarray2['league_group_team_loses'] += 1;
                        } elseif ($leagueGroupTeamMatch2->league_group_match_point?->second_team_points === 1) {
                            $teampTeamarray2['league_group_team_draws'] += 1;
                        }
                    } else {
                        $teampTeamarray2['league_group_team_points'] += 0;
                        $teampTeamarray2['league_group_team_goals'] += 0;
                    }
                }
                // return  $teampTeamarray2['league_group_team_goals_diff'];
                array_push($teamsScore2, $teampTeamarray2);
                // $found_key = array_search($leagueTeam->id, array_column($teamsScore, 'league_team_id'));

            }
        }

        // return $teamsScore2;

        foreach ($teamsScore as $teamScore) {
            $found_key = array_search($teamScore['league_group_team_id'], array_column($teamsScore2, 'league_group_team_id'));
            $teamsScore2[$found_key]['league_group_team_points'] += $teamScore['league_group_team_points'];
            $teamsScore2[$found_key]['league_group_team_goals'] += $teamScore['league_group_team_goals'];
            $teamsScore2[$found_key]['league_group_team_goals_on'] += $teamScore['league_group_team_goals_on'];
            $teamsScore2[$found_key]['league_group_team_goals_diff'] += $teamScore['league_group_team_goals_diff'];
            $teamsScore2[$found_key]['league_group_team_matches_played'] += $teamScore['league_group_team_matches_played'];
            $teamsScore2[$found_key]['league_group_team_wins'] += $teamScore['league_group_team_wins'];
            $teamsScore2[$found_key]['league_group_team_loses'] += $teamScore['league_group_team_loses'];
            $teamsScore2[$found_key]['league_group_team_draws'] += $teamScore['league_group_team_draws'];
        }

        // return $teamsScore2;
        // return $leagueTeamMatch->league_match_schedule;
        $league_group_team_points = array_column($teamsScore2, 'league_group_team_points');
        $league_group_team_goals_diff = array_column($teamsScore2, 'league_group_team_goals_diff');
        $league_group_names = array_column($teamsScore2, 'league_group_name_id');
        array_multisort($league_group_names, SORT_ASC, $league_group_team_points, SORT_DESC, $league_group_team_goals_diff, SORT_DESC, $teamsScore2);
        $grouped_array = array();
        foreach ($teamsScore2 as $teamScore2) {
            $grouped_array[$teamScore2['league_group_name_id']][] = $teamScore2;
        }

        return view('admin.leagueGroups.matchesPoints', compact('grouped_array'))->with('leagueGroup', $leagueGroup);
    }
}
