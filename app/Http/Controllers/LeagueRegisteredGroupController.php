<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\LeagueRegisteredGroup;
use App\Models\LeagueTeam;
use App\Models\LeagueTeamPlayer;
use Illuminate\Http\Request;

class LeagueRegisteredGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueRegisteredGroup $leagueRegisteredGroup)
    {
        $group = Group::where('id', $leagueRegisteredGroup->group_id)->first();
        $usersIds = array();
        $usersIds[] = $group->user->id;
        foreach ($group->group_users as $user) {
            $usersIds[] = $user->id;
        }

        $leagueTeams = LeagueTeam::where('league_id', $leagueRegisteredGroup->league_id)
            ->whereHas('league_team_players', function ($query) use ($usersIds) {
                $query->whereIn('user_id', $usersIds);
            })
            ->get();

        if ($leagueTeams) {
            foreach ($leagueTeams as $leagueTeam) {
                $leagueTeamPlayer = LeagueTeamPlayer::where('league_team_id', $leagueTeam->id)
                    ->whereIn('user_id', $usersIds)->get();
                if ($leagueTeamPlayer) {
                    foreach ($leagueTeamPlayer as $player) {
                        $player->delete();
                    }
                }
            }
        }

        $leagueRegisteredGroup->delete();
        return redirect()->route('leagues.showFinancial', $leagueRegisteredGroup->league_id)
            ->with('delete', 'تم حذف المجموعة بنجاح ');
    }
}
