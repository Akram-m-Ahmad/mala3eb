<?php

namespace App\Http\Controllers;

use App\Models\LeagueRegisteredUser;
use App\Models\LeagueTeam;
use App\Models\LeagueTeamPlayer;
use Illuminate\Http\Request;

class LeagueRegisteredUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueRegisteredUser $leagueRegisteredUser)
    {
        $leagueTeam = LeagueTeam::where('league_id', $leagueRegisteredUser->league_id)
            ->whereHas('league_team_players', function ($query) use ($leagueRegisteredUser) {
                $query->where('user_id', $leagueRegisteredUser->user_id);
            })
            ->first();

        if ($leagueTeam) {
            $leagueTeamPlayer = LeagueTeamPlayer::where('league_team_id', $leagueTeam->id)
                ->where('user_id', $leagueRegisteredUser->user_id)->first();
            if ($leagueTeamPlayer) {
                $leagueTeamPlayer->delete();
            }
        }

        $leagueRegisteredUser->delete();
        return redirect()->route('leagues.showFinancial', $leagueRegisteredUser->league_id)
            ->with('delete', 'تم حذف اللاعب بنجاح ');
    }
}
