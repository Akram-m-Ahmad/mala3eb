<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueGroupNameStoreRequest;
use App\Http\Requests\LeagueGroupNameUpdateRequest;
use App\Models\LeagueGroup;
use Illuminate\Http\Request;
use App\Models\LeagueGroupName;

class LeagueGroupNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $LeagueGroupNames = LeagueGroupName::latest()->paginate(5);
        // return view('admin.LeagueGroupNames.index', compact('LeagueGroupNames'))
        //     ->with('i', (request()->input('page', 1) - 1) * 5);

        $search = request()->query('search');

        if ($search) {
            $LeagueGroupNames = LeagueGroupName::whereHas('league_group', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhere('name', 'LIKE', '%' . $search . '%')
                ->orWhere('name_ar', 'LIKE', '%' . $search . '%')
                ->latest()->paginate(5);
            return view('admin.leagueGroupNames.index', compact('LeagueGroupNames'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        } else {
            $LeagueGroupNames = LeagueGroupName::latest()->paginate(5);
            $search = null;
            return view('admin.leagueGroupNames.index', compact('LeagueGroupNames'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leagueGroupSelected = request()->league_group_id ? LeagueGroup::where('id',request()->league_group_id)->first() : null;
        $leagueGroups = LeagueGroup::latest()->get();
        return view('admin.leagueGroupNames.create')
        ->with('leagueGroupSelected',$leagueGroupSelected)
        ->with('leagueGroups', $leagueGroups);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeagueGroupNameStoreRequest $request)
    {
        $validated = $request->validated();

        $input = $request->all();

        LeagueGroupName::create($input);

        return redirect()->route('leagueGroupNames.index')
            ->with('success', 'تم اضافة المجموعة بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LeagueGroupName $leagueGroupName)
    {
        return view('admin.leagueGroupNames.show', compact('leagueGroupName'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LeagueGroupName $leagueGroupName)
    {
        $leagueGroups = LeagueGroup::latest()->get();
        return view('admin.leagueGroupNames.edit', compact('leagueGroupName'))
        ->with('leagueGroups', $leagueGroups);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LeagueGroupNameUpdateRequest $request, LeagueGroupName $leagueGroupName)
    {
        $validated = $request->validated();

        $input = $request->all();
        $leagueGroupName->update($input);

        return redirect()->route('leagueGroupNames.index')
            ->with('success', 'تم تعديل المجموعة بنجاح ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueGroupName $leagueGroupName)
    {
  
        $leagueGroupName->delete();
        return redirect()->back()
            ->with('delete', 'تم حذف المجموعة بنجاح ');
    }
}
