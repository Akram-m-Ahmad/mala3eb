<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\LeagueGroupRegisteredGroup;
use App\Models\LeagueGroupTeam;
use App\Models\LeagueGroupTeamPlayer;
use Illuminate\Http\Request;

class LeagueGroupRegisteredGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueGroupRegisteredGroup $leagueGroupRegisteredGroup)
    {
        $group = Group::where('id', $leagueGroupRegisteredGroup->group_id)->first();
        $usersIds = array();
        $usersIds[] = $group->user->id;
        foreach ($group->group_users as $user) {
            $usersIds[] = $user->id;
        }

        $leagueGroupTeams = LeagueGroupTeam::where('league_group_id', $leagueGroupRegisteredGroup->league_id)
            ->whereHas('league_group_team_players', function ($query) use ($usersIds) {
                $query->whereIn('user_id', $usersIds);
            })
            ->get();

        if ($leagueGroupTeams) {
            foreach ($leagueGroupTeams as $leagueGroupTeam) {

                $leagueGroupTeamPlayer = LeagueGroupTeamPlayer::where('league_group_team_id', $leagueGroupTeam->id)
                    ->whereIn('user_id', $usersIds)->get();
                if ($leagueGroupTeamPlayer) {
                    foreach ($leagueGroupTeamPlayer as $player) {
                        $player->delete();
                    }
                }
            }
        }

        $leagueGroupRegisteredGroup->delete();
        return redirect()->route('leagueGroups.showFinancial', $leagueGroupRegisteredGroup->league_id)
            ->with('delete', 'تم حذف المجموعة بنجاح ');
    }
}
