<?php

namespace App\Http\Controllers;

use App\Models\LeagueGroupName;
use App\Models\LeagueGroupRegisteredUser;
use App\Models\LeagueGroupTeam;
use App\Models\LeagueGroupTeamPlayer;
use Illuminate\Http\Request;

class LeagueGroupRegisteredUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueGroupRegisteredUser $leagueGroupRegisteredUser)
    {
        // $leagueGroupTeam = LeagueGroupTeam::where('league_group_id', $leagueGroupRegisteredUser->league_group_id)
        //     ->whereHas('league_group_team_players', function ($query) use ($leagueGroupRegisteredUser) {
        //         $query->where('user_id', $leagueGroupRegisteredUser->user_id);
        //     })
        //     ->first();

        $leagueGroupName = LeagueGroupName::where('league_group_id', $leagueGroupRegisteredUser->league_group_id)
            ->with('league_group_teams', function ($query) use ($leagueGroupRegisteredUser) {
                $query->whereHas('league_group_team_players', function ($query) use ($leagueGroupRegisteredUser) {
                    $query->where('user_id', $leagueGroupRegisteredUser->user_id);
                });
            })
            ->first();

        if ($leagueGroupName) {
            foreach ($leagueGroupName->league_group_teams as $leagueGroupTeam) {
                $leagueGroupTeamPlayer = LeagueGroupTeamPlayer::where('league_group_team_id', $leagueGroupTeam->id)
                    ->where('user_id', $leagueGroupRegisteredUser->user_id)->first();
                if ($leagueGroupTeamPlayer) {
                    $leagueGroupTeamPlayer->delete();
                }
            }
        }

        $leagueGroupRegisteredUser->delete();
        return redirect()->route('leagueGroups.showFinancial', $leagueGroupRegisteredUser->league_group_id)
            ->with('delete', 'تم حذف اللاعب بنجاح ');
    }
}
