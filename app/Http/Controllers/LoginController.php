<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
class LoginController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
           
        ]);
   
        if($validator->fails()){
            return ('Validation Error.' );       
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
   
        
        return response()->json(
            [
                'maincode' => '1',
                'code' => 401,
                'error' => 'failed to fetch',
                'data' =>  [
                    'data' =>  $success,
                      
                    ]
            ]
        );
    }
    public function login(Request $request)
    //     if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
    //         $user = Auth::user(); 
    //         $success['token'] =  $user->createToken('MyApp')-> accessToken; 
    //         $success['name'] =  $user->name;
    //         $accessToken = auth()->user()->createToken('authToken')->accessToken;
            

    //         return response(['user' => auth()->user(), 'access_token' => $accessToken->token]);

           
    //     } 
    //     else{ 
    //         return  ('Unauthorised.' );
    //     } 
    // }
//     $data = $request->validate([
//         'email' => 'email|required',
//         'password' => 'required'
//     ]);

//     if (!auth()->attempt($data)) {
//         return response(['error_message' => 'Incorrect Details. 
//         Please try again']);
//     }
//     $token = auth()->user()->createToken('API Token')->accessToken;
//     $user= auth()->user()->update(['api_token' => $token]);

//     return response(['user' => auth()->user(), 'token' => $token]);

// }
{  $login_credentials=[
    'email'=>$request->email,
    'password'=>$request->password,
     
];
// dd( auth()->attempt($login_credentials));

if(auth()->attempt($login_credentials)){
     $user_login_token= auth()->user()->createToken('PassportExample@Section.io')->accessToken;
    //  $user = Auth::user() ;
  // $user=null;
      $user= auth()->user()->update(['api_token' => $user_login_token->token]);
      $id=auth()->user()->id;
   $user2 = User::find($id);
   
    return response()->json([
         
        'data' => $user2], 200);
}
else{
    //wrong login credentials, return, user not authorised to our system, return error code 401
    return response()->json(['error' => 'UnAuthorised Access'], 401);
}
}
}