<?php

namespace App\Http\Controllers;

use App\Models\FootballMatchRegOwnerPlayer;
use Illuminate\Http\Request;

class FootballMatchRegOwnerPlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FootballMatchRegOwnerPlayer  $FootballMatchRegOwnerPlayer
     * @return \Illuminate\Http\Response
     */
    public function show(FootballMatchRegOwnerPlayer $FootballMatchRegOwnerPlayer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FootballMatchRegisteredOwnerPlayer  $footballMatchRegisteredOwnerPlayer
     * @return \Illuminate\Http\Response
     */
    public function edit(FootballMatchRegOwnerPlayer $FootballMatchRegOwnerPlayer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FootballMatchRegOwnerPlayer  $FootballMatchRegOwnerPlayer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FootballMatchRegOwnerPlayer $FootballMatchRegOwnerPlayer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FootballMatchRegOwnerPlayer  $FootballMatchRegOwnerPlayer
     * @return \Illuminate\Http\Response
     */
    public function destroy(FootballMatchRegOwnerPlayer $FootballMatchRegOwnerPlayer)
    {
        //
    }
}
