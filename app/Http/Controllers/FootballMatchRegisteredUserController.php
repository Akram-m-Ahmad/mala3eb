<?php

namespace App\Http\Controllers;

use App\Http\Requests\FootballMatchRegisteredUserStoreRequest;
use App\Models\FootballMatch;
use App\Models\FootballMatchRegisteredGroup;
use App\Models\FootballMatchRegisteredUser;
use Illuminate\Http\Request;

class FootballMatchRegisteredUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FootballMatchRegisteredUserStoreRequest $request)
    {
        $validated = $request->validated();
        $checkRegUser =FootballMatchRegisteredUser::where('user_id',$request->user_id)->where('football_match_id',$request->football_match_id)->first();
        $checkRegGroup =FootballMatchRegisteredGroup::where('user_id',$request->user_id)->where('football_match_id',$request->football_match_id)->first();
        $checkRegUserInGroup =FootballMatch::WhereHas('groups.group_users', function ($query) use ($request) {
            $query->where('user_id', $request->user_id);
        })->first();
        if($checkRegUser || $checkRegGroup || $checkRegUserInGroup){
        return redirect()->route('footballMatches.show', $request->football_match_id)
        ->with('delete', 'المستخدم مسجل من قبل ');
       }
        $input = $request->all();
        FootballMatchRegisteredUser::create($input);

        return redirect()->route('footballMatches.show', $request->football_match_id)
            ->with('success', 'تم اضافة المستخدم بنجاح ');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FootballMatchRegisteredUser $footballMatchRegisteredUser)
    {
        $footballMatchRegisteredUser->delete();
        return redirect()->back()->with('delete','تم حذف أللاعب بنجاح');
    }
}
