<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Models\LeagueGroup;
use App\Models\League;
use App\Models\LeagueTeamPlayer;
use App\Models\User;
use App\Models\Group;
use App\Models\GroupsUser;
use App\Models\FootballField;
use App\Models\FootballMatch;
use App\Models\FootballMatchRegisteredUser;
use App\Models\FootballMatchRegisteredGroup;
use App\Models\Role;
use App\Http\Controllers\Hash;
 class ApplicationController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('role:superadministrator');
    // }

    public function getLeaguegroups(Request $request)
    {
        $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
        $user =$user->first();
         if($user){
       $leagueGroup = LeagueGroup::select('name','name_ar','join_end_at' , 'start_at')->get();
         if ($leagueGroup ) {
            return response()->json([
                'maincode' => '1',
                'code' => 200,
                'error' => null,
                'data' =>  [
                    'leagueGroup' =>  $leagueGroup,
                  ]
            ]);
        } else {
            return response()->json(
                [
                    'maincode' => '1',
                    'code' => 401,
                    'error' => 'failed to fetch',
                    'data' => null
                ]
            );
        }
    }
    else{
        return response()->json(
            [
                'maincode' => '1',
                'code' => 401,
                'error' => 'user not authorsied',
                'data' => null
            ]
        );
    }
}
public function  getallfootballmatches(Request $request){
    $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
    $user =$user->first();
     if($user){
         
    $footballMatch=FootballMatch::all();
   
    $date = Carbon::now()->toDateString();
    $footballMatchday = FootballMatch::where('date', 'like',  $date )->with('football_field')->get();
     $user = User::where('phone', $request->user_id)->first();
     if ($user) {
        // $checkPassword = Hash::check($request->password, $user->password);
        // if ($checkPassword) {
 $todayDate = Carbon::today();
        $todayTime = Carbon::now()->addHours(3)->toTimeString();
        $playedMatches = FootballMatch::whereHas('football_match_registered_users', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->orWhereHas('football_match_registered_groups', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->orWhereHas('groups.group_users', function ($query) use ($user) {
            $query->where('user_id', $user->id);
            //
        })->where('date', '<=', $todayDate)->where('time', '<=', $todayTime)->with('football_field')
            ->get();
    $startDate = Carbon::today();
   $date7 = Carbon::today()->addDays(7)->toDateString();
   $footballMatchdayafter7 = FootballMatch::where('date', '<=',  $date7 )->with('football_field')->get();
     if ($footballMatch == null ) {
    return response()->json([
        'maincode' => '1',
        'code' => 401,
        'error' =>'failed to fetch',
        'data' => null
    ]);}
    if ($footballMatch ) {
        return response()->json([
            'maincode' => '1',
            'code' => 200,
            'error' => null,
            'data' =>  [
                //'user' =>  $user,
                 'today_matches' => $footballMatchday ,
                'seven_days_matches'=> $footballMatchdayafter7,
                'playedMatches'=>$playedMatches
                ]
        ]);
    } else {
        return response()->json(
            [
                'maincode' => '1',
                'code' => 401,
                'error' => 'failed to fetch',
                'data' => null
            ]
        );
        }

    }}
    else{
        return response()->json(
            [
                'maincode' => '1',
                'code' => 401,
                'error' => 'user not authorsied',
                'data' => null
            ]
        );
    }
    // }
}

public function getLeague(Request $request)
    {
        $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
        $user =$user->first();
         if($user){
          
         $league = League::select('name','name_ar','join_end_at' , 'start_at')->get();
 
       $start_at=League::get(['join_start_at']) ;
$date = Carbon::now()->toDateString();
    
        if ($league ) {
            return response()->json([
                'maincode' => '1',
                'code' => 200,
                'error' => null,
                'data' =>  [
                    'league' =>  $league ,
                    'check' =>$date
                  ]
            ]);
        } else {
            return response()->json(
                [
                    'maincode' => '1',
                    'code' => 401,
                    'error' => 'failed to fetch',
                    'data' => null
                ]
            );
        }
        
    }
    else{
        return response()->json(
            [
                'maincode' => '1',
                'code' => 401,
                'error' => 'user not authorsied',
                'data' => null
            ]
        );
    }}
    
    
        public function getLeaguebyid(Request $request,$id)
        {
            $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){
              
            $League =League::find($id);  
           //$football_field_id= $League->football_field_id;
         //  $football_field = FootballField::find(  $football_field_id);
            $League_With_league_teams =$League ->with('league_teams')->get();
             if (  $League ) {
                return response()->json([
                    'maincode' => '1',
                    'code' => 200,
                    'error' => null,
                    'data' =>  [
                        'League' =>  $League,
                        //'football_field'=>$football_field,
                        '$League_With_league_teams'=>$League_With_league_teams,
                      ]
                ]);
            } else {
                return response()->json(
                    [
                        'maincode' => '1',
                        'code' => 401,
                        'error' => 'failed to fetch',
                        'data' => null
                    ]
                );
            }}  else{
                return response()->json(
                    [
                        'maincode' => '1',
                        'code' => 401,
                        'error' => 'user not authorsied',
                        'data' => null
                    ]
                );
            }
            
        }
       
        public function getfootballmatchbyid(Request $request,$id)
        {
            $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){
            $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){
              
            $footballmatch =FootballMatch::find($id); 
            $footballmatch=$footballmatch->with('football_field','football_match_registered_users','groups','football_match_registered_owner_players')->get();
            //  $footballmatch_With_football_field =$footballmatch->with('football_field')->get();
            //  $footballmatch_With_football_match_registered_users=$footballmatch->with('football_match_registered_users')->get();
            //   $footballmatch_With_groups=$footballmatch->with('groups')->get();
             
            //   $footballmatch_With_football_match_registered_owner_players=$footballmatch->with('football_match_registered_owner_players')->get();

            if ($footballmatch ) {
                return response()->json([
                    'maincode' => '1',
                    'code' => 200,
                    'error' => null,
                    'data' =>  [
                        'footballmatch' => $footballmatch,
                                             ]
                ]);
            } else {
                return response()->json(
                    [
                        'maincode' => '1',
                        'code' => 401,
                        'error' => 'failed to fetch',
                        'data' => null
                    ]
                );
            } }
            else{
                return response()->json(
                    [
                        'maincode' => '1',
                        'code' => 401,
                        'error' => 'user not authorsied',
                        'data' => null
                    ]
                );
            }}

             else{
                return response()->json(
                    [
                        'maincode' => '1',
                        'code' => 401,
                        'error' => 'user not authorsied',
                        'data' => null
                    ]
                );
            }
             
        }
        public function create_football_match_registered_users(Request $request){
            $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){
            $FootballMatch=FootballMatchRegisteredUser::create($request->all());
            if ( $FootballMatch) {
                return response()->json([
                    'maincode' => '1',
                    'code' => 200,
                    'error' => null,
                    'data' =>   'successfully added'
                ]);
            } else {
                return response()->json(
                    [
                        'maincode' => '1',
                        'code' => 401,
                        'error' => 'failed to add',
                        'data' => null
                    ]
                );
             
            }}  else{
                return response()->json(
                    [
                        'maincode' => '1',
                        'code' => 401,
                        'error' => 'user not authorsied',
                        'data' => null
                    ]
                );
            }
        }
        public function create_football_match_registered_groups(Request $request){
            $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){
            FootballMatchRegisteredGroup::create($request->all());
            
            return response()->json(
                [
                    'maincode' => '1',
                    'code' => 401,
                    'error' => null,
                    'data' => "success"
                ]
            );
            
        }
     else{
        return response()->json(
            [
                'maincode' => '1',
                'code' => 401,
                'error' => 'user not authorsied',
                'data' => null
            ]
        );
    }
    }
        public function update_User(Request $request, $id)
        {
            $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){
            
            $user = User::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
             $user->country_id = $request->input('country_id');
            $user->city_id= $request->input('city_id');
            $user->date_of_birth= $request->input('date_of_birth');
            $user->isActive= $request->input('isActive');
            $user->cancel_count= $request->input('cancel_count');
            $user->password=Hash::make( $request->input('password'));

 
            $user->position= $request->input('position');

  //delete image
  if (File::exists(public_path('users\image' . $user->profile_picture))) {
    File::delete(public_path('users\image' .$user->profile_picture));
}

//upload image
if ($request->hasFile('profile_picture')) {
    $file = $request->file('profile_picture');
    $fileNameToStore = time() . '_' . $file->getClientOriginalName();
    $file->move( public_path('users\image'), $fileNameToStore);
} else {
    $fileNameToStore = 'noimg.jpg';
}

$user->profile_picture=$fileNameToStore;;

 
            $user->update();
            return response()->json(
                [
                    'maincode' => '1',
                    'code' => 401,
                    'error' => null,
                    'data' => "success"
                ]);
        }
     else{
        return response()->json(
            [
                'maincode' => '1',
                'code' => 401,
                'error' => 'user not authorsied',
                'data' => null
            ]
        );
    }}

         public function create_group(Request $request){
            $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){
            $group=Group::create($request->all());
 
        if ( $group) {
            return response()->json([
                'maincode' => '1',
                'code' => 200,
                'error' => null,
                'data' =>   'successfully added'
            ]);
        } else {
            return response()->json(
                [
                    'maincode' => '1',
                    'code' => 401,
                    'error' => 'failed to add',
                    'data' => null
                ]
            );
         
        }}
      else{
        return response()->json(
            [
                'maincode' => '1',
                'code' => 401,
                'error' => 'user not authorsied',
                'data' => null
            ]
        );
    }
    }


        public function create_groupuser(Request $request){
            $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){
            $groupuser=GroupsUser::create($request->all());
 
        if ( $groupuser) {
            return response()->json([
                'maincode' => '1',
                'code' => 200,
                'error' => null,
                'data' =>   'successfully added'
            ]);
        } else {
            return response()->json(
                [
                    'maincode' => '1',
                    'code' => 401,
                    'error' => 'failed to add',
                    'data' => null
                ]
            );
         
        }}  else{
            return response()->json(
                [
                    'maincode' => '1',
                    'code' => 401,
                    'error' => 'user not authorsied',
                    'data' => null
                ]
            );
        }
    }
        public function get_user(Request $request){
            $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){
            $user=User::all( );
 $leagueTeamPlayer=LeagueTeamPlayer::count('user_id');
        if ($user) {
            return response()->json([
                'maincode' => '1',
                'code' => 200,
                'error' => null,
                'user' => $user,
               'leagueTeamPlayer'=>$leagueTeamPlayer,
            ]);
        } else {
            return response()->json(
                [
                    'maincode' => '1',
                    'code' => 401,
                    'error' => 'failed to add',
                    'data' => null
                ]
            );
         
        }}
        else{
            return response()->json(
                [
                    'maincode' => '1',
                    'code' => 401,
                    'error' => 'user not authorsied',
                    'data' => null
                ]
            );
        }
        }
        public function delete_group($id)
        {
            $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){

            $group = Group::find($id);
            $group->delete();
            if ( $group) {
                return response()->json([
                    'maincode' => '1',
                    'code' => 200,
                    'error' => null,
                    'data' =>   'successfully deleted'
                ]);
            } else {
                return response()->json(
                    [
                        'maincode' => '1',
                        'code' => 401,
                        'error' => 'data does not exist',
                        'data' => null
                    ]
                );
                }}
                else{
                    return response()->json(
                        [
                            'maincode' => '1',
                            'code' => 401,
                            'error' => 'user not authorsied',
                            'data' => null
                        ]
                    );
                }


        }
        public function delete_group_users($id)
        {  $user = User::where([['id', $request->user_id],['api_token' , $request->api_token]])->get();
       
            $user =$user->first();
             if($user){

            $group = GroupsUser::find($id);
            $group->delete();
            if ( $group) {
                return response()->json([
                    'maincode' => '1',
                    'code' => 200,
                    'error' => null,
                    'data' =>   'successfully deleted'
                ]);
            } else {
                return response()->json(
                    [
                        'maincode' => '1',
                        'code' => 401,
                        'error' => 'data does not exist',
                        'data' => null
                    ]
                );
            }
        }
        else{
            return response()->json(
                [
                    'maincode' => '1',
                    'code' => 401,
                    'error' => 'user not authorsied',
                    'data' => null
                ]
            );
        }

        }

}