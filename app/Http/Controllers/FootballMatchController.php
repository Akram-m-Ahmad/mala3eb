<?php

namespace App\Http\Controllers;

use App\Http\Requests\FootballMatchStoreRequest;
use App\Http\Requests\FootballMatchUpdateRequest;
use App\Models\FootballField;
use Illuminate\Http\Request;
use App\Models\FootballMatch;

class FootballMatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $search = request()->query('search');
        if ($search) {
            $footballMatches = FootballMatch::whereHas('football_field', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhereHas('football_field.city', function ($query2) use ($search) {
                $query2->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhereHas('football_field.city.country', function ($query3) use ($search) {
                $query3->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhere('time', 'LIKE', '%' . $search . '%')
                ->orWhere('date', 'LIKE', '%' . $search . '%')
                ->orWhere('price', 'LIKE', '%' . $search . '%')
                ->orWhere('capacity', 'LIKE', '%' . $search . '%')
                ->latest()->paginate(5);
            return view('admin.footballMatches.index', compact('footballMatches'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        } else {
            $search = null;
            $footballMatches = FootballMatch::latest()->paginate(5);
            return view('admin.footballMatches.index', compact('footballMatches'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $footballFields = FootballField::latest()->get();
        return view('admin.footballMatches.create')
            ->with('footballFields', $footballFields);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FootballMatchStoreRequest $request)
    {
        $validated = $request->validated();
        $input = $request->all();
        FootballMatch::create($input);

        return redirect()->route('footballMatches.index')
            ->with('success', 'تم اضافة المباراة بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(FootballMatch $footballMatch)
    {
        return view('admin.footballMatches.show', compact('footballMatch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(FootballMatch $footballMatch)
    {
        $footballFields = FootballField::latest()->get();
        return view('admin.footballMatches.edit', compact('footballMatch'))
            ->with('footballFields', $footballFields);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FootballMatchUpdateRequest $request, FootballMatch $footballMatch)
    {
        $validated = $request->validated();
        $input = $request->all();
        $footballMatch->update($input);

        return redirect()->route('footballMatches.index')
            ->with('success', 'تم تعديل المباراة بنجاح ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FootballMatch $footballMatch)
    {
        $footballMatch->delete();
        return redirect()->route('footballMatches.index')
            ->with('delete', 'تم حذف المباراة بنجاح ');
    }
}
