<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueTeamPlayerStoreRequest;
use Illuminate\Http\Request;
use App\Models\LeagueTeam;
use App\Models\LeagueTeamPlayer;
use Illuminate\Support\Arr;

class LeagueTeamPlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leagueTeam = LeagueTeam::where('id', request()->league_team_id)->first();
        $league = $leagueTeam->league;
        $usersIds = array();
        //    $test = $leagueTeam->league->with('league_teams.league_team_players')->get();
        //  $test->first()->league_teams->first()->league_team_players;
        foreach ($league->league_teams as $checkTeam) {
            foreach ($checkTeam->league_team_players as $user) {
                $usersIds[] = $user->id;
            }
        }
       
        return view('admin.leagueTeamPlayers.create', compact('leagueTeam'))
            ->with('usersIds', $usersIds);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeagueTeamPlayerStoreRequest $request)
    {

        $vaildated = $request->validated();

        foreach ($request->users as $user) {
            $leagueTeamPlayer = new LeagueTeamPlayer;
            $leagueTeamPlayer->league_team_id = $request->league_team_id;
            $leagueTeamPlayer->user_id = $user;
            $leagueTeamPlayer->save();
        }

        return redirect()->back()
            ->with('success', 'تم اضافة اللاعبين بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueTeamPlayer $leagueTeamPlayer)
    {
        $leagueTeamPlayer->delete();
        return redirect()->back()
            ->with('delete', 'تم حذف اللاعب بنجاح ');
    }
}
