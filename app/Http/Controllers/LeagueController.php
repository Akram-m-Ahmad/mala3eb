<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueStoreRequest;
use App\Http\Requests\LeagueUpdateRequest;
use App\Models\City;
use App\Models\FootballField;
use Illuminate\Http\Request;
use App\Models\League;
use App\Models\LeagueTeam;
use App\Models\LeagueTeamMatch;
use Illuminate\Support\Facades\File;

class LeagueController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->query('search');
        if ($search) {
            $leagues = League::whereHas('football_field', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhereHas('city', function ($query1) use ($search) {
                $query1->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhereHas('city.country', function ($query2) use ($search) {
                $query2->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhere('name', 'LIKE', '%' . $search . '%')
                ->orWhere('name_ar', 'LIKE', '%' . $search . '%')
                ->orWhere('price', 'LIKE', '%' . $search . '%')
                ->orWhere('details', 'LIKE', '%' . $search . '%')
                ->orWhere('details_ar', 'LIKE', '%' . $search . '%')
                ->orWhere('teams_number', 'LIKE', '%' . $search . '%')
                ->orWhere('team_players_number', 'LIKE', '%' . $search . '%')
                ->orWhere('start_at', 'LIKE', '%' . $search . '%')
                ->orWhere('join_start_at', 'LIKE', '%' . $search . '%')
                ->orWhere('join_end_at', 'LIKE', '%' . $search . '%')
                ->latest()->paginate(5);
                return view('admin.leagues.index', compact('leagues'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        } else {
            $search = null;
            $leagues = League::latest()->paginate(5);
            return view('admin.leagues.index', compact('leagues'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        }
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $footballFields = FootballField::latest()->get();
        $cities = City::latest()->get();
        return view('admin.leagues.create')
            ->with('footballFields', $footballFields)
            ->with('cities', $cities);
    }

    public function createTeam($id)
    {
        $league = League::where('id', $id)->first();
        return view('admin.leagues.createTeam', compact('league'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeagueStoreRequest $request)
    {
        $validated = $request->validated();

        $input = $request->all();

        if ($image = $request->file('main_image')) {
            $destinationPath = 'storage/images/leagues/';
            $profileImage = date('YmdHis') . "-" .  $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['main_image'] = "$profileImage";
        }

        League::create($input);

        return redirect()->route('leagues.index')
            ->with('success', 'تم اضافة الدوري بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(League $league)
    {
        $usersIds = array();
        //    $test = $leagueTeam->league->with('league_teams.league_team_players')->get();
        //  $test->first()->league_teams->first()->league_team_players;
        foreach ($league->league_teams as $checkTeam) {
            foreach ($checkTeam->league_team_players as $user) {
                $usersIds[] = $user->id;
            }
        }

        $countScheduledMatches = 0;

        foreach ($league->league_teams as $leagueTeam)
            $countScheduledMatches +=  $leagueTeam->league_team_matches1->count();


        return view('admin.leagues.show', compact('league'))
            ->with('usersIds', $usersIds)
            ->with('countScheduledMatches', $countScheduledMatches);
    }

    public function showFinancial(League $league)
    {
        // $usersIds = array();
        // //    $test = $leagueTeam->league->with('league_teams.league_team_players')->get();
        // //  $test->first()->league_teams->first()->league_team_players;
        // foreach ($league->league_teams as $checkTeam) {
        //     foreach ($checkTeam->league_team_players as $user) {
        //         $usersIds[] = $user->id;
        //     }
        // }

        return view('admin.leagues.showFinancial', compact('league'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(League $league)
    {
        $footballFields = FootballField::latest()->get();
        $cities = City::latest()->get();
        return view('admin.leagues.edit', compact('league'))
            ->with('footballFields', $footballFields)
            ->with('cities', $cities);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LeagueUpdateRequest $request, League $league)
    {
        $validated = $request->validated();

        $input = $request->all();

        if ($image = $request->file('main_image')) {
            $destinationPath = 'storage/images/leagues/';
            $profileImage = date('YmdHis') . "-" .  $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['main_image'] = "$profileImage";
            if (File::exists(public_path('storage/images/leagues/' . $league->main_image))) {
                File::delete(public_path('storage/images/leagues/' . $league->main_image));
            }
        } else {
            $input['main_image'] = "$league->main_image";
        }



        $league->update($input);

        return redirect()->route('leagues.index')
            ->with('success', 'تم تعديل الدوري بنجاح ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(League $league)
    {
        if (File::exists(public_path('storage/images/leagues/' . $league->main_image))) {
            File::delete(public_path('storage/images/leagues/' . $league->main_image));
        }
        $league->delete();
        return redirect()->route('leagues.index')
            ->with('delete', 'تم حذف الدوري بنجاح ');
    }

    public function createMatches(League $league)
    {

        $allTeams = LeagueTeam::where('league_id', $league->id)->get();

        if ($league->isHome == 1) {
            foreach ($allTeams as $team2) {
                foreach ($allTeams as $vsTeam2) {
                    if ($team2->id == $vsTeam2->id) {
                        continue;
                    } else {
                        $leagueTeamMatch = new LeagueTeamMatch;
                        $leagueTeamMatch->league_team1_id = $team2->id;
                        $leagueTeamMatch->league_team2_id = $vsTeam2->id;
                        $leagueTeamMatch->save();
                    }
                }
            }
        } else {
            foreach ($allTeams as $team) {
                foreach ($allTeams as $vsTeam) {
                    if ($team->id == $vsTeam->id) {
                        continue;
                    } elseif ((LeagueTeamMatch::where('league_team1_id', $team->id)->where('league_team2_id', $vsTeam->id)->first())
                        || LeagueTeamMatch::where('league_team1_id', $vsTeam->id)->where('league_team2_id', $team->id)->first()
                    ) {
                    } else {
                        $leagueTeamMatch = new LeagueTeamMatch;
                        $leagueTeamMatch->league_team1_id = $team->id;
                        $leagueTeamMatch->league_team2_id = $vsTeam->id;
                        $leagueTeamMatch->save();
                    }
                }
            }
        }
        // foreach ($allTeams as $team) {
        //     foreach ($allTeams as $vsTeam) {
        //         if ($team->id == $vsTeam->id) {
        //             continue;
        //         } else {
        //             $leagueTeamMatch = new LeagueTeamMatch;
        //             $leagueTeamMatch->league_team1_id = $team->id;
        //             $leagueTeamMatch->league_team2_id = $vsTeam->id;
        //             $leagueTeamMatch->save();
        //         }
        //     }
        // }



        return redirect()->back()->with('success', 'تم إضافة المباريات ');
    }

    public function matchesSchedule(League $league)
    {
        // foreach($league->league_teams as $leagueTeam)
        // {
        //      foreach($leagueTeam->league_team_matches1 as $leagueTeamMatch){
        //          return $leagueTeamMatch->league_teams2;
        //      }
        // }
        $i = 1;
        return view('admin.leagues.matchesSchedule', compact('league'))->with('i', $i);
    }

    public function showMatchDetails(LeagueTeamMatch $leagueTeamMatch)
    {
        // foreach($league->league_teams as $leagueTeam)
        // {
        //      foreach($leagueTeam->league_team_matches1 as $leagueTeamMatch){
        //          return $leagueTeamMatch->league_teams2;
        //      }
        // }
        // return $leagueTeamMatch->league_match_schedule;

        return view('admin.leagues.showMatchDetails', compact('leagueTeamMatch'));
    }

    public function matchesPoints(League $league)
    {

        $teamsScore = array();
        $teamsScore2 = array();
        foreach ($league->league_teams as $leagueTeam) {
            $teampTeamarray = array();
            $teampTeamarray2 = array();

            $teampTeamarray['league_team_id'] = $leagueTeam->id;
            $teampTeamarray['league_team_name'] = $leagueTeam->name;
            $teampTeamarray['league_team_name_ar'] = $leagueTeam->name_ar;
            $teampTeamarray2['league_team_flag_icon'] = $leagueTeam->flag_icon;
            $teampTeamarray['league_team_points'] = 0;
            $teampTeamarray['league_team_goals'] = 0;
            $teampTeamarray['league_team_goals_on'] = 0;
            $teampTeamarray['league_team_goals_diff'] = 0;
            $teampTeamarray['league_team_matches_played'] = 0;
            $teampTeamarray['league_team_wins'] = 0;
            $teampTeamarray['league_team_loses'] = 0;
            $teampTeamarray['league_team_draws'] = 0;

            $teampTeamarray2['league_team_id'] = $leagueTeam->id;
            $teampTeamarray2['league_team_name'] = $leagueTeam->name;
            $teampTeamarray2['league_team_name_ar'] = $leagueTeam->name_ar;
            $teampTeamarray2['league_team_flag_icon'] = $leagueTeam->flag_icon;
            $teampTeamarray2['league_team_points'] = 0;
            $teampTeamarray2['league_team_goals'] = 0;
            $teampTeamarray2['league_team_goals_on'] = 0;
            $teampTeamarray2['league_team_goals_diff'] = 0;
            $teampTeamarray2['league_team_matches_played'] = 0;
            $teampTeamarray2['league_team_wins'] = 0;
            $teampTeamarray2['league_team_loses'] = 0;
            $teampTeamarray2['league_team_draws'] = 0;


            // return $leagueTeam->league_team_matches1;
            foreach ($leagueTeam->league_team_matches1 as $leagueTeamMatch) {
                if ($leagueTeamMatch->league_match_point) {
                    $teampTeamarray['league_team_points'] +=  $leagueTeamMatch->league_match_point->first_team_points;
                    $teampTeamarray['league_team_goals'] +=  $leagueTeamMatch->league_match_point->first_team_goals;
                    $teampTeamarray['league_team_goals_on'] +=  $leagueTeamMatch->league_match_point->second_team_goals;
                    $teampTeamarray['league_team_goals_diff'] += $leagueTeamMatch->league_match_point->first_team_goals - $leagueTeamMatch->league_match_point->second_team_goals;
                    ++$teampTeamarray['league_team_matches_played'];
                    if ($leagueTeamMatch->league_match_point->first_team_points === 3) {
                        $teampTeamarray['league_team_wins'] += 1;
                    } elseif ($leagueTeamMatch->league_match_point->first_team_points === 0) {
                        $teampTeamarray['league_team_loses'] += 1;
                    } elseif ($leagueTeamMatch->league_match_point->first_team_points === 1) {
                        $teampTeamarray['league_team_draws'] += 1;
                    }
                } else {
                    $teampTeamarray['league_team_points'] += 0;
                    $teampTeamarray['league_team_goals'] += 0;
                }
                // if ($leagueTeamMatch->league_match_point) {
                //     if ($leagueTeamMatch->league_match_point->first_team_points === 3) {
                //         $teampTeamarray['league_team_wins'] += 1;
                //     } elseif ($leagueTeamMatch->league_match_point->first_team_points === 0) {
                //         $teampTeamarray['league_team_loses'] += 1;
                //     } elseif ($leagueTeamMatch->league_match_point->first_team_points === 1) {
                //         $teampTeamarray['league_team_draws'] += 1;
                //     }
                // }

                // array_push($teamsScore, $teampTeamarray);
                // return $leagueTeamMatch->league_match_point;
            }

            array_push($teamsScore, $teampTeamarray);

            foreach ($leagueTeam->league_team_matches2 as $leagueTeamMatch2) {
                if ($leagueTeamMatch2->league_match_point) {
                    $teampTeamarray2['league_team_points'] += $leagueTeamMatch2->league_match_point->second_team_points;
                    $teampTeamarray2['league_team_goals'] += $leagueTeamMatch2->league_match_point->second_team_goals;
                    $teampTeamarray2['league_team_goals_on'] +=  $leagueTeamMatch2->league_match_point->first_team_goals;
                    $teampTeamarray2['league_team_goals_diff'] += $leagueTeamMatch2->league_match_point->second_team_goals - $leagueTeamMatch2->league_match_point->first_team_goals;
                    ++$teampTeamarray2['league_team_matches_played'];
                    if ($leagueTeamMatch2->league_match_point->second_team_points === 3) {
                        $teampTeamarray2['league_team_wins'] += 1;
                    } elseif ($leagueTeamMatch2->league_match_point?->second_team_points === 0) {
                        $teampTeamarray2['league_team_loses'] += 1;
                    } elseif ($leagueTeamMatch2->league_match_point?->second_team_points === 1) {
                        $teampTeamarray2['league_team_draws'] += 1;
                    }
                } else {
                    $teampTeamarray2['league_team_points'] += 0;
                    $teampTeamarray2['league_team_goals'] += 0;
                }
                // $teampTeamarray2['league_team_points'] += $leagueTeamMatch2->league_match_point?->second_team_points ? $leagueTeamMatch2->league_match_point->second_team_points : 0;
                // $teampTeamarray2['league_team_goals'] += $leagueTeamMatch2->league_match_point?->second_team_goals ? $leagueTeamMatch2->league_match_point->second_team_goals : 0;
                // if ($leagueTeamMatch2->league_match_point?->second_team_points === 3) {
                //     $teampTeamarray2['league_team_wins'] += 1;
                // } elseif ($leagueTeamMatch2->league_match_point?->second_team_points === 0) {
                //     $teampTeamarray2['league_team_loses'] += 1;
                // } elseif ($leagueTeamMatch2->league_match_point?->second_team_points === 1) {
                //     $teampTeamarray2['league_team_draws'] += 1;
                // }
                // array_push($teamsScore, $teampTeamarray);
                // return $leagueTeamMatch->league_match_point;
            }
            array_push($teamsScore2, $teampTeamarray2);
            // $found_key = array_search($leagueTeam->id, array_column($teamsScore, 'league_team_id'));

        }

        // return $teamsScore2;

        foreach ($teamsScore as $teamScore) {
            $found_key = array_search($teamScore['league_team_id'], array_column($teamsScore2, 'league_team_id'));
            $teamsScore2[$found_key]['league_team_points'] += $teamScore['league_team_points'];
            $teamsScore2[$found_key]['league_team_goals'] += $teamScore['league_team_goals'];
            $teamsScore2[$found_key]['league_team_goals_on'] += $teamScore['league_team_goals_on'];
            $teamsScore2[$found_key]['league_team_goals_diff'] += $teamScore['league_team_goals_diff'];
            $teamsScore2[$found_key]['league_team_matches_played'] += $teamScore['league_team_matches_played'];
            $teamsScore2[$found_key]['league_team_wins'] += $teamScore['league_team_wins'];
            $teamsScore2[$found_key]['league_team_loses'] += $teamScore['league_team_loses'];
            $teamsScore2[$found_key]['league_team_draws'] += $teamScore['league_team_draws'];
        }

        // return $teamsScore2;
        // return $leagueTeamMatch->league_match_schedule;
        $league_team_points = array_column($teamsScore2, 'league_team_points');

        array_multisort($league_team_points, SORT_DESC, $teamsScore2);

        return view('admin.leagues.matchesPoints', compact('teamsScore2'))->with('league', $league);
    }
}
