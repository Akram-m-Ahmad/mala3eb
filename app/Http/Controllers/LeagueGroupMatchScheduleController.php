<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueGroupMatchScheduleStoreRequest;
use App\Models\LeagueGroupMatchSchedule;
use App\Models\LeagueGroupTeamMatch;
use Illuminate\Http\Request;

class LeagueGroupMatchScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leagueGroupTeamMatchId =  request()->league_group_team_match_id;
        $leagueGroupTeamMatch = LeagueGroupTeamMatch::where('id', $leagueGroupTeamMatchId)->first();
        return view('admin.leagueGroupMatchSchedules.create', compact('leagueGroupTeamMatchId'))
            ->with('leagueGroupTeamMatch', $leagueGroupTeamMatch);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeagueGroupMatchScheduleStoreRequest $request)
    {
        $validated = $request->validated();

        LeagueGroupMatchSchedule::create($request->all());

        $leagueGroupTeamMatch = LeagueGroupTeamMatch::where('id', $request->league_group_team_match_id)->first();

        return redirect('/leagueGroups/matchesSchedule/' . $leagueGroupTeamMatch->league_group_team1->league_group_name->league_group_id)
            ->with('success', 'تم اضافة الموعد بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueGroupMatchSchedule $leagueGroupMatchSchedule)
    {
        $leagueGroupMatchSchedule->delete();
        return redirect()->back()
            ->with('delete', 'تم حذف موعد المباراة بنجاح ');
    }
}
