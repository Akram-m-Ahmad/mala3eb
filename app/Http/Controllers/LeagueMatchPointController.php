<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueMatchPointStoreRequest;
use Illuminate\Http\Request;
use App\Models\LeagueMatchPoint;
use App\Models\LeagueTeamMatch;

class LeagueMatchPointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leagueTeamMatchId =  request()->league_team_match_id;
        $leagueTeamMatch = LeagueTeamMatch::where('id',$leagueTeamMatchId)->first();
        // return $leagueTeamMatch->league_teams1;
        return view('admin.leagueMatchPoints.create', compact('leagueTeamMatchId'))
        ->with('leagueTeamMatch',$leagueTeamMatch);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeagueMatchPointStoreRequest $request)
    {
        $validated = $request->validated();

        LeagueMatchPoint::create($request->all());

        $leagueTeamMatch = LeagueTeamMatch::where('id', $request->league_team_match_id)->first();

        return redirect('/leagues/matchesSchedule/' . $leagueTeamMatch->league_teams1->league_id)
        ->with('success', 'تم اضافة النتيجة بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueMatchPoint $leagueMatchPoint)
    {
        $leagueMatchPoint->delete();
        return redirect()->back()
            ->with('delete', 'تم حذف النتيجة بنجاح ');
    }
}
