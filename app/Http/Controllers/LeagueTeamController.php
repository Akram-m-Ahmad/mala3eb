<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueTeamStoreRequest;
use App\Http\Requests\LeagueTeamUpdateRequest;
use App\Models\League;
use App\Models\LeagueGroupTeam;
use Illuminate\Http\Request;
use App\Models\LeagueTeam;
use Illuminate\Support\Facades\File;

class LeagueTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->query('search');

        if ($search) {
            $leagueTeams = LeagueTeam::whereHas('league', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhere('name', 'LIKE', '%' . $search . '%')
                ->orWhere('name_ar', 'LIKE', '%' . $search . '%')
                ->latest()->paginate(5);
            return view('admin.leagueTeams.index', compact('leagueTeams'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        } else {
            $search = null;
            $leagueTeams = LeagueTeam::latest()->paginate(5);
            return view('admin.leagueTeams.index', compact('leagueTeams'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leagueSelected = request()->league_id ? League::where('id',request()->league_id)->first() : null;
        $leagues = League::latest()->get();
        return view('admin.leagueTeams.create', compact('leagues'))
        ->with('leagueSelected',$leagueSelected);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeagueTeamStoreRequest $request)
    {
        $validated = $request->validated();

        $input = $request->all();

        if ($image = $request->file('flag_icon')) {
            $destinationPath = 'storage/images/leagueTeams/';
            $profileImage = date('YmdHis') . "-" .  $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['flag_icon'] = "$profileImage";
        }

        LeagueTeam::create($input);

        return redirect()->route('leagueTeams.index')
            ->with('success', 'تم اضافة الفريق بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LeagueTeam $leagueTeam)
    {
        return view('admin.leagueTeams.show', compact('leagueTeam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LeagueGroupTeam $leagueGroupTeam)
    {
        $leagues = League::latest()->get();
        return view('admin.leagueTeams.edit', compact('leagueTeam'))
            ->with('leagues', $leagues);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LeagueTeamUpdateRequest $request, LeagueTeam $leagueTeam)
    {
        $validated = $request->validated();

        $input = $request->all();

        if ($image = $request->file('flag_icon')) {
            $destinationPath = 'storage/images/leagueTeams/';
            $profileImage = date('YmdHis') . "-" .  $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['flag_icon'] = "$profileImage";
            if (File::exists(public_path('storage/images/leagueTeams/' . $leagueTeam->flag_icon))) {
                File::delete(public_path('storage/images/leagueTeams/' . $leagueTeam->flag_icon));
            }
        } else {
            $input['flag_icon'] = "$leagueTeam->flag_icon";
        }



        $leagueTeam->update($input);

        return redirect()->route('leagueTeams.index')
            ->with('success', 'تم تعديل الفريق بنجاح ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeagueTeam $leagueTeam)
    {
        if (File::exists(public_path('storage/images/leagueTeams/' . $leagueTeam->flag_icon))) {
            File::delete(public_path('storage/images/leagueTeams/' . $leagueTeam->flag_icon));
        }
        $leagueTeam->delete();
        return redirect()->back()
            ->with('delete', 'تم حذف الدوري بنجاح ');
    }
}
