<?php

namespace App\Http\Controllers;

use App\Http\Requests\FootballFieldStoreRequest;
use App\Http\Requests\FootballFieldUpdateRequest;
use App\Models\City;
use Illuminate\Http\Request;
use App\Models\FootballField;
use App\Models\Role;
use Illuminate\Support\Facades\File;

class FootballFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $search = request()->query('search');
        if ($search) {
            $footballFields = FootballField::whereHas('user', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('email', 'LIKE', '%' . $search . '%')
                    ->orWhere('phone', 'LIKE', '%' . $search . '%');
            })->orWhereHas('city', function ($query1) use ($search) {
                $query1->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhereHas('city.country', function ($query2) use ($search) {
                $query2->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('name_ar', 'LIKE', '%' . $search . '%');
            })->orWhere('name', 'LIKE', '%' . $search . '%')
                ->orWhere('name_ar', 'LIKE', '%' . $search . '%')
                ->orWhere('location', 'LIKE', '%' . $search . '%')
                ->orWhere('location_ar', 'LIKE', '%' . $search . '%')
                ->latest()->paginate(5);
            return view('admin.footballFields.index', compact('footballFields'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        } else {
            $footballFields = FootballField::latest()->paginate(5);
            $search = null;
            return view('admin.footballFields.index', compact('footballFields'))
                ->with('i', (request()->input('page', 1) - 1) * 5)->with('search', $search);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::latest()->get();
        $role = Role::where('name', 'stadium_owner')->first();
        $owners = $role->users;
        return view('admin.footballFields.create')
            ->with('cities', $cities)
            ->with('owners', $owners);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FootballFieldStoreRequest $request)
    {
        $validated = $request->validated();

        $input = $request->all();

        // if ($request->file('images')) {
        //     $uploadedImages = array();
        //     foreach ($request->file('images') as $image) {
        //         $destinationPath = 'storage/images/footballFields/';
        //         $profileImage = date('YmdHis') . "." . $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
        //         $image->move($destinationPath, $profileImage);
        //         $uploadedImages[] = "$profileImage";
        //     }
        //     $input['first_image'] = "$uploadedImages[0]";
        //     $input['second_image'] = "$uploadedImages[1]";
        //     $input['third_image'] = "$uploadedImages[2] ";
        // }

        if ($image = $request->file('first_image')) {
            $destinationPath = 'storage/images/footballFields/';
            $profileImage = date('YmdHis') . "-" .  $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['first_image'] = "$profileImage";
        }
        if ($image2 = $request->file('second_image')) {
            $destinationPath2 = 'storage/images/footballFields/';
            $profileImage2 = date('YmdHis') . "-" . $image2->getClientOriginalName() . "." . $image2->getClientOriginalExtension();
            $image2->move($destinationPath2, $profileImage2);
            $input['second_image'] = "$profileImage2";
        }
        if ($image3 = $request->file('third_image')) {
            $destinationPath3 = 'storage/images/footballFields/';
            $profileImage3 = date('YmdHis') . "-" .  $image3->getClientOriginalName() . "." . $image3->getClientOriginalExtension();
            $image3->move($destinationPath3, $profileImage3);
            $input['third_image'] = "$profileImage3";
        }



        FootballField::create($input);

        return redirect()->route('footballFields.index')
            ->with('success', 'تم اضافة الملعب بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(FootballField $footballField)
    {
        return view('admin.footballFields.show', compact('footballField'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(FootballField $footballField)
    {
        $cities = City::latest()->get();
        $role = Role::where('name', 'stadium_owner')->first();
        $owners = $role->users;
        return view('admin.footballFields.edit', compact('footballField'))
            ->with('cities', $cities)
            ->with('owners', $owners);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FootballFieldUpdateRequest $request, FootballField $footballField)
    {
        $validated = $request->validated();

        $input = $request->all();

        // if ($request->file('images')) {
        //     $uploadedImages = array();
        //     foreach ($request->file('images') as $image) {
        //         $destinationPath = 'storage/images/footballFields/';
        //         $profileImage = date('YmdHis') . "." . $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
        //         $image->move($destinationPath, $profileImage);
        //         $uploadedImages[] = "$profileImage";
        //     }
        //     $input['first_image'] = "$uploadedImages[0]";
        //     $input['second_image'] = "$uploadedImages[1]";
        //     $input['third_image'] = "$uploadedImages[2] ";
        // }

        if ($image = $request->file('first_image')) {
            $destinationPath = 'storage/images/footballFields/';
            $profileImage = date('YmdHis') . "-" .  $image->getClientOriginalName() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['first_image'] = "$profileImage";
        } else {
            $input['first_image'] = "$footballField->first_image";
        }
        if ($image2 = $request->file('second_image')) {
            $destinationPath2 = 'storage/images/footballFields/';
            $profileImage2 = date('YmdHis') . "-" . $image2->getClientOriginalName() . "." . $image2->getClientOriginalExtension();
            $image2->move($destinationPath2, $profileImage2);
            $input['second_image'] = "$profileImage2";
        } else {
            $input['second_image'] = "$footballField->second_image";
        }
        if ($image3 = $request->file('third_image')) {
            $destinationPath3 = 'storage/images/footballFields/';
            $profileImage3 = date('YmdHis') . "-" .  $image3->getClientOriginalName() . "." . $image3->getClientOriginalExtension();
            $image3->move($destinationPath3, $profileImage3);
            $input['third_image'] = "$profileImage3";
        } else {
            $input['third_image'] = "$footballField->third_image";
        }

        if (File::exists(public_path('storage/images/footballFields/' . $footballField->first_image))) {
            if ($input['first_image'] != "$footballField->first_image") {
                File::delete(public_path('storage/images/footballFields/' . $footballField->first_image));
            }
        }
        if (File::exists(public_path('storage/images/footballFields/' . $footballField->second_image))) {
            if ($input['second_image'] != "$footballField->second_image") {
                File::delete(public_path('storage/images/footballFields/' . $footballField->second_image));
            }
        }
        if (File::exists(public_path('storage/images/footballFields/' . $footballField->third_image))) {
            if ($input['third_image'] != "$footballField->third_image") {
                File::delete(public_path('storage/images/footballFields/' . $footballField->third_image));
            }
        }


        $footballField->update($input);

        return redirect()->route('footballFields.index')
            ->with('success', 'تم تعديل الملعب بنجاح ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FootballField $footballField)
    {
        if (File::exists(public_path('storage/images/footballFields/' . $footballField->first_image))) {
            File::delete(public_path('storage/images/footballFields/' . $footballField->first_image));
        }
        if (File::exists(public_path('storage/images/footballFields/' . $footballField->second_image))) {
            File::delete(public_path('storage/images/footballFields/' . $footballField->second_image));
        }
        if (File::exists(public_path('storage/images/footballFields/' . $footballField->third_image))) {
            File::delete(public_path('storage/images/footballFields/' . $footballField->third_image));
        }


        $footballField->delete();
        return redirect()->route('footballFields.index')
            ->with('delete', 'تم حذف الملعب بنجاح ');
    }
}
