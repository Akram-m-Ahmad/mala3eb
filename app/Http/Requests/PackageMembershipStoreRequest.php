<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackageMembershipStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [     
            'name' => 'required',
            'name_ar' => 'required',
            'details' => 'required',
            'details_ar' => 'required',
            'price' => 'required',
            'match_price' => 'required',
            'duration_in_days' => 'required',
            'position' => 'required',
            'matches_number' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'background_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ];
    }


      /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'يجب ادخال الاسم بالانجليزي',
            'name_ar.required' => 'يجب ادخال الاسم بالعربي',
            'details.required' => 'يجب ادخال الوصف بالانجليزي',
            'details_ar.required' => 'يجب ادخال الوصف بالعربي',
            'price.required' => 'يجب ادخال سعر الباقة',
            'match_price.required' => 'يجب ادخال سعر المباراة الذي تشمله الباقة',
            'duration_in_days.required' => 'يجب ادخال مدة الباقة بالايام',
            'position.required' => 'يجب ادخال الترتيب',
            'matches_number.required' => 'يجب ادخال عدد المباريات',
            'country_id.required' => 'يجب ادخال الدولة',
            'city_id.required' => 'يجب ادخال المدينة',
            'background_image.required' => 'يجب ادخال صورة الخلفية ',
        ];
    }
}
