<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FootballMatchStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [     
            'football_field_id' => 'required',
            'date' => 'required',
            'time' => 'required',
            'price' => 'required',
            'capacity' => 'required',
        ];
    }


      /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'football_field_id.required' => 'يجب اختيار الملعب',
            'date.required' => 'يجب ادخال تاريخ المباراة',
            'time.required' => 'يجب ادخال توقيت المباراة',
            'price.required' => 'يجب ادخال السعر للشخص الواحد',
            'capacity.required' => 'يجب ادخال عدد اللاعبين للمباراة',
  
        ];
    }
}
