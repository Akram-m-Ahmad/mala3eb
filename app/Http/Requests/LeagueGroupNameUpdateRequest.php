<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeagueGroupNameUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [     
            'name' => 'required',
            'name_ar' => 'required',
            'league_group_id' => 'required',
        ];
    }


      /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'يجب ادخال الاسم بالانجليزي',
            'name_ar.required' => 'يجب ادخال الاسم بالعربي',
            'league_group_id.required' => 'يجب اختيار دوري مجموعات',
        ];
    }
}
