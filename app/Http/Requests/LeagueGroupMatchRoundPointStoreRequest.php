<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeagueGroupMatchRoundPointStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_team_goals' => 'required',
            'second_team_goals' => 'required',
            'league_g_m_round_id' => 'required',
        ];
    }


    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_team_goals.required' => 'يجب ادخال اهداف الفريق الاول',
            'second_team_goals.required' => 'يجب ادخال اهداف الفريق الثاني ',
            'league_g_m_round_id.required' => 'يجب اختيار المباراة ',
        ];
    }
}
