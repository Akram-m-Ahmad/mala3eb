<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeagueGroupMatchRoundUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'round_type_id' => 'required',
            'league_group_team1_id' => 'required',
            'league_group_team2_id' => 'required',
        ];
    }


    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'round_type_id.required' => 'يجب اختيار نوع الجولة',
            'league_group_team1_id.required' => 'يجب اختيار الفريق الاول',
            'league_group_team2_id.required' => 'يجب اختيار الفريق الثاني',
        ];
    }
}
