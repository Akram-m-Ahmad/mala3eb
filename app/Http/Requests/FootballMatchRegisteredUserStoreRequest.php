<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FootballMatchRegisteredUserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [     
            'price' => 'required',
            'user_id' => 'required',
            'payment_method_id' => 'required',
            'football_match_id' => 'required',
        ];
    }


      /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'price.required' => 'يجب ادخال السعر',
            'user_id.required' => 'يجب اختيار المستخدم',
            'payment_method_id.required' => 'يجب اختيار طريقة الدفع',
            'football_match_id.required' => 'يجب اختيار المباراة',
  
        ];
    }
}
