<style>
    .sidebar {
        direction: ltr
    }

</style>
<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <a href="" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('paper') }}/img/logo-small.png">
            </div>
        </a>
        <a href="" class="simple-text logo-normal">
            {{ __('Akram & Aboud') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'dashboard') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'countries' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'countries') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('الدول') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'cities' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'cities') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('المدن') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'footballFields' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'footballFields') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('الملاعب') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'footballMatches' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'footballMatches') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('المباريات') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'leagues' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'leagues') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('الدوريات') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'leagueTeams' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'leagueTeams') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('فرق الدوريات') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'leagueGroups' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'leagueGroups') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('دوري المجموعات') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'leagueGroupNames' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'leagueGroupNames') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('المجموعات') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'leagueGroupTeams' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'leagueGroupTeams') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('فرق المجموعات') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'users' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'users') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('إدارة المستخدمين') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'user' || $elementActive == 'profile' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#laravelExamples">
                    <i class="nc-icon"><img src="{{ asset('paper/img/laravel.svg') }}"></i>
                    <p>
                        {{ __('Laravel examples') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse show" id="laravelExamples">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'profile' ? 'active' : '' }}">
                            <a href="{{ route('profile.edit') }}">
                                <span class="sidebar-mini-icon">{{ __('UP') }}</span>
                                <span class="sidebar-normal">{{ __(' User Profile ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'user' ? 'active' : '' }}">
                            <a href="{{ route('page.index', 'user') }}">
                                <span class="sidebar-mini-icon">{{ __('U') }}</span>
                                <span class="sidebar-normal">{{ __(' User Management ') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="{{ $elementActive == 'icons' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'icons') }}">
                    <i class="nc-icon nc-diamond"></i>
                    <p>{{ __('Icons') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'map' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'map') }}">
                    <i class="nc-icon nc-pin-3"></i>
                    <p>{{ __('Maps') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'products' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'products') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>{{ __('منتجات') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'notifications' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'notifications') }}">
                    <i class="nc-icon nc-bell-55"></i>
                    <p>{{ __('Notifications') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'tables' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'tables') }}">
                    <i class="nc-icon nc-tile-56"></i>
                    <p>{{ __('Table List') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'typography' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'typography') }}">
                    <i class="nc-icon nc-caps-small"></i>
                    <p>{{ __('Typography') }}</p>
                </a>
            </li>

        </ul>
    </div>
</div>
