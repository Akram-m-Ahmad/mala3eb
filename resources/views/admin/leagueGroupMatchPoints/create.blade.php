@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueGroups'
])
@section('content')

    <div class="content">

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('أضف نتيجة للمباراة') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ url()->previous() }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger text-right">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if ($leagueGroupTeamMatch->league_group_match_point)
                <div class="row text-right m-5">
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            <span>لا يمكن اضافة نتيجة للمباراة. هنالك نتيجة تم ادخالها مسبقاً. الرجاء حذف النتيجة وادخال
                                نتيجة جديدة.</span>
                        </div>

                    </div>
                </div>
            @else
                <form class="col-md-12" action="{{ route('leagueGroupMatchPoints.store') }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-row text-right">
                            <div class="form-group col-md-12">
                                <label>{{ __('نقاط الفريق الاول') }} ({{ $leagueGroupTeamMatch->league_group_team1->name }} -
                                    {{ $leagueGroupTeamMatch->league_group_team1->name_ar }})</label>
                                <input placeholder="1" type="number" name="first_team_points" class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label>{{ __('نقاط الفريق الثاني') }} ({{ $leagueGroupTeamMatch->league_group_team2->name }} -
                                    {{ $leagueGroupTeamMatch->league_group_team2->name_ar }})</label>
                                <input placeholder="1" type="number" name="second_team_points" class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label>{{ __('اهداف الفريق الاول') }} ({{ $leagueGroupTeamMatch->league_group_team1->name }} -
                                    {{ $leagueGroupTeamMatch->league_group_team1->name_ar }})</label>
                                <input placeholder="1" type="number" name="first_team_goals" class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label>{{ __('اهداف الفريق الثاني') }} ({{ $leagueGroupTeamMatch->league_group_team2->name }} -
                                    {{ $leagueGroupTeamMatch->league_group_team2->name_ar }})</label>
                                <input placeholder="1" type="number" name="second_team_goals" class="form-control">
                                <input type="hidden" value="{{ $leagueGroupTeamMatchId }}"
                                    name="league_group_team_match_id" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-info btn-round">{{ __('أضف النتيجة') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            @endif


        </div>

    </div>




@endsection
