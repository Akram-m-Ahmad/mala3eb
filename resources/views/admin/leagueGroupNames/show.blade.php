@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueGroupNames'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

    .table-hover td:nth-of-type(1) {
        width: 250px;
    }

</style>
@section('content')
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض دوري المجموعات</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('leagueGroupNames.index') }}"> العودة</a>
                        </div>
                    </div>
                </div>

                <div class=" table-hover table-responsive text-right">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>اسم دوري المجموعات: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroupName->league_group->name_ar }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>الاسم بالانجليزي: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroupName->name }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>الاسم بالعربي: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroupName->name_ar }}
                                </td>
                            </tr>

                            <tr>
                   table for teams
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
