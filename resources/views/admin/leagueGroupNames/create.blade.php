@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueGroupNames'
])
@section('content')

    <div class="content">

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('أضف مجموعة جديدة') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ route('leagueGroupNames.index') }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger text-right">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form class="col-md-12" action="{{ route('leagueGroupNames.store') }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-row text-right">
                        <div class="form-group col-md-12">
                            <label>{{ __('دوري المجموعات') }}</label>
                            @if ($leagueGroupSelected !== null)
                                <input type="hidden" value="{{ $leagueGroupSelected->id }} " name="league_group_id"
                                    class="form-control">
                                <input
                                    value="{{ $leagueGroupSelected->name_ar }} - {{ $leagueGroupSelected->name_ar }} "
                                    type="text" name="fakeInput" disabled class="form-control">
                            @else
                                <select name="league_group_id" class="custom-select">
                                    @foreach ($leagueGroups as $leagueGroup)
                                        <option value={{ $leagueGroup->id }}>{{ $leagueGroup->name_ar }} -
                                            {{ $leagueGroup->name }}
                                        </option>
                                    @endforeach
                                </select>
                            @endif

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم المجموعة بالانجليزي') }}</label>
                            <input placeholder="Group A" type="text" name="name" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم المجموعة بالعربي') }}</label>
                            <input placeholder="المجموعة أ" type="text" name="name_ar" class="form-control">

                        </div>

                    </div>
                </div>
                <div class="card-footer ">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-info btn-round">{{ __('أضف المجموعة') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>




@endsection
@push('scripts')
    <script>
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
@endpush
