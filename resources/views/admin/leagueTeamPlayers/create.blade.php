@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagues'
])

<style>
    p {
        margin: 0 !important;
    }

</style>

@section('content')

    <div class="content">

        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('أضف لاعبين') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ route('leagues.show', $leagueTeam->league->id) }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger text-right">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            @if ($leagueTeam->league_team_players->count() == $leagueTeam->league->team_players_number)
                <div class="row text-right m-5">
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            <span>عدد اللاعبين مكتمل. لايمكن إضافة المزيد من اللاعبين الى الفريق.</span>
                        </div>
                    
                    </div>
                </div>
            @else
            <form class="col-md-12" action="{{ route('leagueTeamPlayers.store') }}" method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-row text-right">
                    <div class="form-group col-md-12">
                        <label>{{ __('اسم الفريق') }}</label>
                        <input value="{{ $leagueTeam->name_ar }}" disabled type="text" class="form-control">
                        <input value="{{ $leagueTeam->id }}" type="hidden" name="league_team_id"
                            class="form-control">
                    </div>

                    <div class="form-group col-md-12">
                        <label>{{ __('اللاعبين المسجلين كأفراد') }}</label><br />

                        @foreach ($leagueTeam->league->league_registered_users as $user)
                            @if (!in_array($user->id, $usersIds))
                                <input type="checkbox" name="users[]" value="{{ $user->id }}">
                                {{ $user->name }} {{ $user->position }} <br />
                            @endif
                        @endforeach
                    </div>

                    <div class="form-group col-md-12">
                        <label>{{ __('اللاعبين المسجلين كمجموعات') }}</label><br />
                        @foreach ($leagueTeam->league->groups as $group)
                            @php
                                $groupUsersIds = [];
                                $groupUsersIds[] = $group->user->id;
                                foreach ($group->group_users as $group_user) {
                                    $groupUsersIds[] = $group_user->id;
                                }
                                $arrDiff = array_diff($groupUsersIds, $usersIds);
                                
                            @endphp

                            @if (count($arrDiff) !== 0)
                                <p>{{ $loop->iteration }}-
                                    <strong>اسم المجموعة: {{ $group->name }}</strong>
                                </p>
                            @endif

                            @if (!in_array($group->user->id, $usersIds))
                                <input type="checkbox" name="users[]" value="{{ $group->user->id }}">
                                {{ $group->user->name }}<br />
                            @endif


                            @foreach ($group->group_users as $group_user)
                                @if (!in_array($group_user->id, $usersIds))
                                    <input type="checkbox" name="users[]" value="{{ $group_user->id }}">
                                    {{ $group_user->name }} <br />
                                @endif
                            @endforeach
                        @endforeach
                    </div>

                </div>
            </div>
            <div class="card-footer ">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-info btn-round">{{ __('أضف اللاعبين') }}</button>
                    </div>
                </div>
            </div>
        </form>
            @endif

       
        </div>

    </div>




@endsection
@push('scripts')
    <script>
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
@endpush
