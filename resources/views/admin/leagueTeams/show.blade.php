@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueTeams'
])

<style>
    p {
        margin: 0 !important;
    }

    .table-responsive {
        overflow: hidden !important;
    }

</style>
@section('content')
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض الفريق</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('leagueTeams.index') }}"> العودة</a>
                        </div>
                    </div>
                </div>

                <div class="row text-right">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>الدوري: </strong>
                            {{ $leagueTeam->league->name_ar }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>الاسم بالانجليزي: </strong>
                            {{ $leagueTeam->name }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>الاسم بالعربي: </strong>
                            {{ $leagueTeam->name_ar }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>صورة العرض: </strong>
                            <img src="{{ asset('storage/images/leagueTeams/' . $leagueTeam->flag_icon) }}" width="80"
                                height="80">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>عدد اللاعبين: </strong>
                            {{ $leagueTeam->league->team_players_number }} /
                            {{ $leagueTeam->league_team_players->count() }}
                        </div>
                    </div>
                    {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>اسماء اللاعبين: </strong>
                            @foreach ($leagueTeam->league_team_players as $user)
                                <p>
                                    {{ $user->name }}
                                </p>
                            @endforeach
                        </div>
                    </div> --}}
                </div>

                <div class="table-responsive text-right">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>الرقم</th>
                            <th>اسماء اللاعبين</th>
                            <th>الإجراءات</th>
                        </thead>
                        <tbody>


                            @foreach ($leagueTeam->league_team_players as $user)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>
                                        <form action="{{ route('leagueTeamPlayers.destroy', $user->pivot->id) }}" method="POST">
                                            {{-- <a class="btn btn-info btn-icon  "
                                                href="{{ route('leagues.show', $user->id) }}"> <i
                                                    class="fa fa-eye"></i></a>
                                            <a class="btn btn-primary btn-icon"
                                                href="{{ route('leagues.edit', $user->id) }}"><i
                                                    class="fa fa-edit"></i></a> --}}

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-icon"><i
                                                    class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
