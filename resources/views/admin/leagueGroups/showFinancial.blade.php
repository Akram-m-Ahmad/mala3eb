@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueGroups'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض التفاصيل المادية للدوري</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('leagueGroups.show', $leagueGroup->id) }}"> العودة</a>
                        </div>
                    </div>
                </div>

                <div class="row text-right mt-3">
                    <div class="col-md-12">
                        <h5>قائمة اللاعبون المسجلون بشكل فردي</h5>
                    </div>
                </div>
                <div class="table-responsive text-right">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>الرقم</th>
                            <th>اسم اللاعب</th>
                            <th>طريقة الدفع المستخدمة</th>
                            <th>المبلغ المسدد</th>
                            <th>تاريخ السداد</th>
                            <th>الإجراءات</th>
                        </thead>
                        <tbody>


                            @foreach ($leagueGroup->league_group_registered_users as $user)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ App\Models\PaymentMethod::find($user->pivot->payment_method_id)->name_ar }}
                                    </td>
                                    <td> {{ $user->pivot->price }} </td>
                                    <td> {{ $user->pivot->created_at }} </td>

                                    <td>
                                        <form action="{{ route('leagueGroupRegisteredUsers.destroy', $user->pivot->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-icon"><i
                                                    class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="row text-right mt-3">
                    <div class="col-md-12">
                        <h5>قائمة اللاعبون المسجلون بشكل مجموعات</h5>
                    </div>
                </div>
                <div class="table-responsive text-right">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>الرقم</th>
                            <th>اسم المجموعة</th>
                            <th>اسماء اللاعبين</th>
                            <th>طريقة الدفع المستخدمة</th>
                            <th>المبلغ المسدد</th>
                            <th>تاريخ السداد</th>
                            <th>الإجراءات</th>
                        </thead>
                        <tbody>


                            @foreach ($leagueGroup->groups as $group)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $group->name }}</td>
                                    <td>{{ $group->user->name }} -
                                        @foreach ($group->group_users as $group_user)
                                            {{ $group_user->name }}
                                        @endforeach
                                    </td>
                                    <td>{{ App\Models\PaymentMethod::find($group->pivot->payment_method_id)->name_ar }}
                                    </td>
                                    <td> {{ $group->pivot->price }} </td>
                                    <td> {{ $group->pivot->created_at }} </td>

                                    <td>
                                        <form action="{{ route('leagueGroupRegisteredGroups.destroy', $group->pivot->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-icon"><i
                                                    class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
