@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueGroups'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

    .table-hover td:nth-of-type(1) {
        width: 250px;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض دوري المجموعات</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('leagueGroups.index') }}"> العودة</a>
                        </div>
                    </div>
                </div>

                <div class=" table-hover table-responsive text-right">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>الاسم بالانجليزي: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->name }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>الاسم بالعربي: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->name_ar }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>ذهاب و إياب: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->isHome ? 'نعم' : 'لا' }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>السعر: </strong>

                                </td>
                                <td>
                                    {{ $leagueGroup->price }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>التفاصيل بالانجليزي: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->details }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>التفاصيل بالعربي: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->details_ar }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>عدد المجموعات: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->groups_number }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>عدد الفرق: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->teams_number }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>عدد اللاعبين في الفريق الواحد: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->team_players_number }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>عدد اللاعبين المسجلين: </strong>
                                </td>
                                <td>
                                    @php
                                        $countGroupsPlayers = 0;
                                        foreach ($leagueGroup->groups as $group) {
                                            $countGroupsPlayers += $group->group_users->count() + 1;
                                        }
                                    @endphp
                                    {{ $leagueGroup->teams_number * $leagueGroup->team_players_number }} /
                                    {{ $leagueGroup->league_group_registered_users->count() + $countGroupsPlayers }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>تاريخ بداية دوري المجموعات: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->start_at }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>تاريخ بداية الانضمام: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->join_start_at }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>تاريخ نهاية الانضمام: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->join_end_at }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>الملعب: </strong>

                                </td>
                                <td>
                                    {{ $leagueGroup->football_field->name_ar }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>المدينة: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroup->city->name_ar }} - {{ $leagueGroup->city->country->name_ar }}
                                </td>
                            </tr>


                            <tr>
                                @if ($leagueGroup->league_group_names->count() < $leagueGroup->groups_number)
                                    <td>
                                        <strong>
                                            إشعار:
                                        </strong>
                                    </td>
                                    <td>
                                        <div class="alert alert-danger mb-0">
                                            <strong>إن عدد المجموعات غير مكتمل. يجب إضافة
                                                {{ $leagueGroup->groups_number - $leagueGroup->league_group_names->count() }}
                                                مجموعات</strong>
                                            <a class="btn btn-info" target="_blank"
                                                href="{{ route('leagueGroupNames.create', ['league_group_id' => $leagueGroup->id]) }}">
                                                اضافة
                                                مجموعات</a>
                                        </div>


                                    </td>
                                @else
                                    @if ($leagueGroup->league_group_names->count() > $leagueGroup->groups_number)
                                        <td>
                                            <strong>
                                                إشعار:
                                            </strong>
                                        </td>
                                        <td>
                                            <div class="alert alert-danger mb-0">
                                                <strong>إن عدد المجموعات زائد. يجب إزالة
                                                    {{ $leagueGroup->league_group_names->count() - $leagueGroup->groups_number }}
                                                    مجموعات</strong>

                                            </div>


                                        </td>
                                    @else
                                        <td>

                                            <strong>
                                                إشعار:
                                            </strong>
                                        </td>
                                        <td>
                                            <div class="alert alert-info mb-0">
                                                <strong> عدد المجموعات مكتمل</strong>
                                            </div>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                            <tr>

                                @if ($leagueGroupCountTeams < $leagueGroup->teams_number)
                                    <td>
                                        <strong>
                                            إشعار:
                                        </strong>
                                    </td>
                                    <td>
                                        <div class="alert alert-danger mb-0">
                                            <strong>إن عدد الفرق غير مكتمل. يجب إضافة
                                                {{ $leagueGroup->teams_number - $leagueGroupCountTeams }} فرق</strong>
                                            <a class="btn btn-info" target="_blank"
                                                href="{{ route('leagueGroupTeams.create', ['league_group_id' => $leagueGroup->id]) }}">
                                                اضافة
                                                فرق</a>
                                        </div>


                                    </td>
                                @else
                                    @if ($leagueGroupCountTeams > $leagueGroup->teams_number)
                                        <td>
                                            <strong>
                                                إشعار:
                                            </strong>
                                        </td>
                                        <td>
                                            <div class="alert alert-danger mb-0">
                                                <strong>إن عدد الفرق زائد. يجب إزالة
                                                    {{ $leagueGroup->teams_number - $leagueGroupCountTeams }}
                                                    فرق</strong>
                                            </div>
                                        </td>
                                    @else
                                        <td>
                                            <strong>
                                                إشعار:
                                            </strong>
                                        </td>
                                        <td>
                                            <div class="alert alert-info mb-0">
                                                <strong> عدد الفرق مكتمل</strong>
                                            </div>

                                        </td>
                                    @endif
                                @endif
                            </tr>

                            <tr>
                                <td>
                                    إشعار:
                                </td>
                                <td>
                                    @if (count($usersIds) < $leagueGroup->league_group_registered_users->count() + $countGroupsPlayers)
                                        <div class="alert alert-danger mb-0">
                                            <strong> لم يتم إضافة جميع المستخدمين المسجلين في الدوري الى الفرق. المتبقي
                                                {{ $leagueGroup->league_group_registered_users->count() + $countGroupsPlayers - count($usersIds) }}
                                                لاعبين</strong>
                                        </div>
                                    @else
                                        @if ($leagueGroup->league_group_registered_users->count() === 0 && $leagueGroup->league_group_registered_groups->count() === 0)
                                            <div class="alert alert-info mb-0">
                                                <strong> لا يوجد لاعبين مسجلين بعد.</strong>
                                            </div>
                                        @else
                                            <div class="alert alert-info mb-0">
                                                <strong> تم إضافة جميع اللاعبين المسجلين الى الفرق</strong>
                                            </div>
                                        @endif
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>صورة العرض: </strong>
                                </td>
                                <td>
                                    <img src="{{ asset('storage/images/leagueGroups/' . $leagueGroup->main_image) }}"
                                        width="200px">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>يمكنك الاطلاع على التفاصيل المادية من هنا: </strong>
                                </td>
                                <td>
                                    <a class="btn btn-warning btn-icon"
                                        href="{{ route('leagueGroups.showFinancial', $leagueGroup->id) }}">
                                        <i class="fa fa-eye"></i></a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>
                                        إنشاء جدول المباريات:
                                    </strong>
                                </td>
                                <td>
                                    @if ($leagueGroup->isHome)
                                        @if ($countScheduledMatches === ($leagueGroup->teams_number / $leagueGroup->groups_number - 1) * ($leagueGroup->teams_number / $leagueGroup->groups_number) * $leagueGroup->groups_number)
                                            <div class="alert alert-info mb-0">
                                                <strong> تم إنشاء جدول المباريات</strong>
                                            </div>
                                        @else
                                            @if ($leagueGroup->teams_number === 4 && $leagueGroup->groups_number === 2 && $countScheduledMatches === 4)
                                                <div class="alert alert-info mb-0">
                                                    <strong> تم إنشاء جدول المباريات</strong>
                                                </div>
                                            @else
                                                <a class="btn btn-info"
                                                    href="{{ route('leagueGroups.createMatches', $leagueGroup->id) }}">
                                                    إضافة الجدول
                                                </a>
                                            @endif
                                        @endif
                                    @else
                                        @if ($countScheduledMatches === (($leagueGroup->teams_number / $leagueGroup->groups_number - 1) * ($leagueGroup->teams_number / $leagueGroup->groups_number) * $leagueGroup->groups_number)/2)
                                            <div class="alert alert-info mb-0">
                                                <strong> تم إنشاء جدول المباريات</strong>
                                            </div>
                                        @else
                                            <a class="btn btn-info"
                                                href="{{ route('leagueGroups.createMatches', $leagueGroup->id) }}">
                                                إضافة الجدول
                                            </a>
                                        @endif
                                    @endif
                                    {{-- <a class="btn btn-info"
                                    href="{{ route('leagueGroups.createMatches', $leagueGroup->id) }}">
                                    إضافة الجدول
                                </a> --}}

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        جدول المباريات:
                                    </strong>
                                </td>
                                <td>
                                    <a class="btn btn-info"
                                        href="{{ route('leagueGroups.matchesSchedule', $leagueGroup->id) }}">
                                        الجدول
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        جدول النقاط:
                                    </strong>
                                </td>
                                <td>
                                    <a class="btn btn-info"
                                        href="{{ route('leagueGroups.matchesPoints', $leagueGroup->id) }}">
                                        الجدول
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>


            </div>



            <div class="row text-right mt-3">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">

                    </div>
                </div>
            </div>

            @foreach ($leagueGroupNamesTeams->league_group_names as $leagueGroupName)
                <div class="mx-3 text-right">
                    <h5>
                        <strong>
                            {{ $leagueGroupName->name_ar }} - {{ $leagueGroupName->name }}
                        </strong>
                    </h5>
                </div>

                <div class="table-responsive text-right">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>الرقم</th>
                            <th>اسم الفريق بالانجليزي</th>
                            <th>اسم الفريق بالعربي</th>
                            <th>عدد اللاعبين</th>
                            <th>علم الفريق</th>
                            <th>الإجراءات</th>
                        </thead>
                        <tbody>

                            @foreach ($leagueGroupName->league_group_teams as $leagueGroupTeam)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $leagueGroupTeam->name_ar }} {{ $leagueGroupTeam->id }}</td>
                                    <td>{{ $leagueGroupTeam->name }}</td>
                                    <td> {{ $leagueGroup->team_players_number }} /
                                        {{ $leagueGroupTeam->league_group_team_players->count() }}</td>
                                    <td>
                                        <img width="80" height="80"
                                            src="{{ asset('storage/images/leagueGroupTeams/' . $leagueGroupTeam->flag_icon) }}">
                                    </td>
                                    <td>
                                        <form action="{{ route('leagueGroupTeams.destroy', $leagueGroupTeam->id) }}"
                                            method="POST">

                                            <a class="btn btn-success btn-icon" target="_blank"
                                                href="{{ route('leagueGroupTeamPlayers.create', ['league_group_team_id' => $leagueGroupTeam->id]) }}">
                                                <i class="nc-icon nc-simple-add"></i></a>

                                            <a class="btn btn-info btn-icon" target="_blank"
                                                href="{{ route('leagueGroupTeams.show', $leagueGroupTeam->id) }}"> <i
                                                    class="fa fa-eye"></i></a>
                                            <a class="btn btn-primary btn-icon" target="_blank"
                                                href="{{ route('leagueGroupTeams.edit', $leagueGroupTeam->id) }}"><i
                                                    class="fa fa-edit"></i></a>

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-icon"><i
                                                    class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endforeach

        </div>
    </div>
@endsection
