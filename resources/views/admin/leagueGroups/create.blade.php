@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueGroups'
])
@section('content')

    <div class="content">

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('أضف دوري مجموعات جديد') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ route('leagues.index') }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger text-right">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form class="col-md-12" action="{{ route('leagueGroups.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-row text-right">
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم دوري المجموعات بالانجليزي') }}</label>
                            <input placeholder="Champions league" type="text" name="name" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم دوري المجموعات بالعربي') }}</label>
                            <input placeholder="دوري الأبطال" type="text" name="name_ar" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('ذهاب و إياب') }}</label>
                            <select name="isHome" class="custom-select">
                                <option value="1">نعم
                                </option>
                                <option value="0">لا
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('السعر') }}</label>
                            <input placeholder="500" type="number" name="price" class="form-control">

                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('التفاصيل بالانجليزي') }}</label>
                            <input
                                placeholder="This league will have 20 matches for each team and the winner will have a prize of 5000SR"
                                type="text" name="details" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('التفاصيل بالعربي') }}</label>
                            <input
                                placeholder="هذا الدوري مكون من 20 مباراة لكل فريق وسيحصل الفائز على جائزة قدرها 5000 ريال"
                                type="text" name="details_ar" class="form-control">
                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('عدد المجموعات') }}</label>
                            <input placeholder="16" type="number" name="groups_number" class="form-control">

                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('عدد الفرق') }}</label>
                            <input placeholder="16" type="number" name="teams_number" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('عدد اللاعبين في الفريق الواحد') }}</label>
                            <input placeholder="15" type="number"
                                name="team_players_number" class="form-control">
                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('تاريخ بداية دوري المجموعات') }}</label>
                            <input type="date" name="start_at" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('تاريخ بداية الانضمام') }}</label>
                            <input type="date" name="join_start_at" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('تاريخ نهاية الانضمام') }}</label>
                            <input type="date" name="join_end_at" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('الملعب') }}</label>
                            <select name="football_field_id" class="custom-select">
                                @foreach ($footballFields as $footballField)
                                    <option value={{ $footballField->id }}>{{ $footballField->name_ar }} -
                                        {{ $footballField->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('المدينة') }}</label>
                            <select name="city_id" class="custom-select">
                                @foreach ($cities as $city)
                                    <option value={{ $city->id }}>{{ $city->name_ar }} -
                                        {{ $city->country->name_ar }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="main_image" style="cursor: pointer;">{{ __('اختر صورة العرض') }} <br> <i
                                    style="font-size:40px" class="nc-icon nc-image"></i>
                            </label>
                            <input type="file" class="form-control-file" id="main_image" name="main_image">
                        </div>
                    </div>
                    <div class="card-footer ">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-info btn-round">{{ __('أضف دوري المجموعات') }}</button>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        var loadFile = function(event) {
            var image = document.getElementById('first_image');
            image.src = URL.createObjectURL(event.target.files[0]);
            console.log(event.target.files[0])
        };
        var loadFile2 = function(event) {
            var image2 = document.getElementById('second_image');
            image2.src = URL.createObjectURL(event.target.files[1]);
            console.log(event.target.files[1])
        };
        var loadFile3 = function(event) {
            var image3 = document.getElementById('third_image');
            image3.src = URL.createObjectURL(event.target.files[2]);
            console.log(event.target.files[0])
        };
    </script>
@endpush
