@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueGroups'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

    .table-hover td:nth-of-type(1) {
        width: 250px;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض جدول المباريات</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('leagueGroups.show', $leagueGroup->id) }}">
                                العودة</a>
                        </div>
                    </div>
                </div>




                <div class="row text-right mt-3">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">

                        </div>
                    </div>
                </div>


<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Launch demo modal
  </button>
  
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
      <h5 class="modal-title " id="exampleModalLabel">إضافة نتيجة المباراة</h5>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div> --}}


                @foreach ($leagueGroup->league_group_names as $leagueGroupName)
                <div class="mx-3 text-right">
                    <h5>
                        <strong>
                            {{ $leagueGroupName->name }} - {{ $leagueGroupName->name_ar }}
                        </strong>
                    </h5>
                </div>
                    <div class="table-responsive text-right">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>الرقم</th>
                                <th>الفريق الاول</th>
                                <th>الفريق الثاني</th>
                                <th>إضافة موعد للمباراة</th>
                                <th>إضافة نتيجة المباراة</th>
                                <th>الإجراءات</th>
                            </thead>
                            <tbody>
                                @foreach ($leagueGroupName->league_group_teams as $leagueGroupTeam)
                                    @foreach ($leagueGroupTeam->league_group_team_match1 as $leagueGroupTeamMatch)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>
                                                <img width="80" height="80"
                                                    src="{{ asset('storage/images/leagueGroupTeams/' . $leagueGroupTeamMatch->league_group_team1->flag_icon) }}">
                                                {{ $leagueGroupTeamMatch->league_group_team1->name }} -
                                                {{ $leagueGroupTeamMatch->league_group_team1->name_ar }}

                                            </td>
                                            <td>
                                                <img width="80" height="80"
                                                    src="{{ asset('storage/images/leagueGroupTeams/' . $leagueGroupTeamMatch->league_group_team2->flag_icon) }}">
                                                {{ $leagueGroupTeamMatch->league_group_team2->name }} -
                                                {{ $leagueGroupTeamMatch->league_group_team2->name_ar }}
                                            </td>

                                            <td>
                                                <a class="btn btn-success btn-icon"
                                                    href="{{ route('leagueGroupMatchSchedules.create', ['league_group_team_match_id' => $leagueGroupTeamMatch->id]) }}">
                                                    <i class="nc-icon nc-simple-add"></i></a>
                                            </td>
                                            <td>

                                                <a class="btn btn-success btn-icon"
                                                    href="{{ route('leagueGroupMatchPoints.create', ['league_group_team_match_id' => $leagueGroupTeamMatch->id]) }}">
                                                    <i class="nc-icon nc-simple-add"></i></a>
                                            </td>
                                            {{-- <td> {{ $league->team_players_number }} /
                                            {{ $leagueTeam->league_team_players->count() }}</td>
                                        <td>
                                            <img width="80" height="80"
                                                src="{{ asset('storage/images/leagueTeams/' . $leagueTeam->flag_icon) }}">
                                        </td> --}}
                                            <td>
                                                {{-- <form action="{{ route('leagueTeams.destroy', $leagueTeam->id) }}"
                                                method="POST"> --}}


                                                <a class="btn btn-info btn-icon"
                                                    href="{{ route('leagueGroups.showMatchDetails', $leagueGroupTeamMatch->id) }}">
                                                    <i class="fa fa-eye"></i></a>
                                                {{-- <a class="btn btn-primary btn-icon"
                                                    href="{{ route('leagueTeams.edit', $leagueTeam->id) }}"><i
                                                        class="fa fa-edit"></i></a> --}}

                                                {{-- @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-icon"><i
                                                        class="fa fa-times"></i></button>
                                            </form> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach

            </div>

        </div>
    </div>
@endsection
