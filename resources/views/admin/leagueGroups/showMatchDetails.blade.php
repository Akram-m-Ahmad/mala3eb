@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueGroups'
])

<style>
    p {
        margin: 0 !important;
    }

    .table-responsive {
        overflow: hidden !important;
    }

    .card-div {
        background-image: linear-gradient(to bottom, #45DA9E, #008ECC);
        color: #ffffff;
        border-radius: 16px;

    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض بيانات المباراة</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary"
                                href="{{ url('/leagueGroups/matchesSchedule/' . $leagueGroupTeamMatch->league_group_team1->league_group_name->league_group_id) }}">
                                العودة</a>
                        </div>
                    </div>
                </div>




                <div class=" table-hover table-responsive text-right">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>الفريق الاول: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroupTeamMatch->league_group_team1->name }} -
                                    {{ $leagueGroupTeamMatch->league_group_team1->name_ar }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>الفريق الثاني: </strong>
                                </td>
                                <td>
                                    {{ $leagueGroupTeamMatch->league_group_team2->name }} -
                                    {{ $leagueGroupTeamMatch->league_group_team2->name_ar }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="card-div py-5">
                    <div class="row text-center">
                        <div class="col">
                            <img width="50" height="50"
                            src="{{ asset('storage/images/leagueGroupTeams/' . $leagueGroupTeamMatch->league_group_team1->flag_icon) }}">
                        </div>
                        <div class="col">
                            <div class="text-center">
                                <p>

                                    {{ $leagueGroupTeamMatch->league_group_team1->name }} -
                                    {{ $leagueGroupTeamMatch->league_group_team1->name_ar }}
                                </p>
                            </div>
                        </div>

                        <div class="col">
                            <div class="text-center">
                                <p>
                                    توقيت المباراة
                                </p>
                                <p>
                                    @if ($leagueGroupTeamMatch->league_group_match_schedule)
                                    {{ $leagueGroupTeamMatch->league_group_match_schedule->date }}
                                    {{ $leagueGroupTeamMatch->league_group_match_schedule->time }}
                                    @endif
                                    {{-- {{ $leagueGroupTeamMatch->league_group_match_schedule?->date }}
                                    {{ $leagueGroupTeamMatch->league_group_match_schedule?->time }} --}}
                                </p>
                                <p>
                                    نتيجة المباراة
                                </p>
                                <p>
                                    @if ($leagueGroupTeamMatch->league_group_match_point)
                                    {{ $leagueGroupTeamMatch->league_group_match_point->first_team_goals }}
                                    -
                                    {{ $leagueGroupTeamMatch->league_group_match_point->second_team_goals }}
                                    @endif
                                    {{-- {{ $leagueGroupTeamMatch->league_group_match_point?->first_team_goals }}
                                    -
                                    {{ $leagueGroupTeamMatch->league_group_match_point?->second_team_goals }} --}}
                                </p>
                            </div>
                        </div>

                        <div class="col">
                            <div class="text-center">
                                <p>
                                    {{ $leagueGroupTeamMatch->league_group_team2->name }} -
                                    {{ $leagueGroupTeamMatch->league_group_team2->name_ar }}
  
                                </p>
                            </div>
                        </div>

                        <div class="col">
                            <img width="50" height="50"
                            src="{{ asset('storage/images/leagueGroupTeams/' . $leagueGroupTeamMatch->league_group_team2->flag_icon) }}">
                        </div>

                    </div>
                </div>

                @if ($leagueGroupTeamMatch->league_group_match_schedule)
                    <div class="table-responsive text-right">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>تاريخ المباراة</th>
                                <th>موعد المباراة</th>
                                <th>حذف</th>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>

                                        {{ $leagueGroupTeamMatch->league_group_match_schedule->date }}
                                    </td>
                                    <td>
                                        {{ $leagueGroupTeamMatch->league_group_match_schedule->time }}
                                    </td>
                                    <td>
                                        <form
                                            action="{{ route('leagueGroupMatchSchedules.destroy', $leagueGroupTeamMatch->league_group_match_schedule->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-icon"><i
                                                    class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="row text-right m-5">
                        <div class="col-md-12">
                            <div class="alert alert-info">
                                <span>
                                    لم يتم اضافة موعد للمباراة.
                                </span>
                            </div>

                        </div>
                    </div>
                @endif


                @if ($leagueGroupTeamMatch->league_group_match_point)
                    <div class="table-responsive text-right">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>نقاط الفريق الاول</th>
                                <th>نقاط الفريق الثاني</th>
                                <th>اهداف الفريق الاول</th>
                                <th>اهداف الفريق الثاني</th>
                                <th>حذف</th>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>
                                        {{ $leagueGroupTeamMatch->league_group_match_point->first_team_points }}
                                    </td>
                                    <td>
                                        {{ $leagueGroupTeamMatch->league_group_match_point->second_team_points }}
                                    </td>
                                    <td>
                                        {{ $leagueGroupTeamMatch->league_group_match_point->first_team_goals }}
                                    </td>
                                    <td>
                                        {{ $leagueGroupTeamMatch->league_group_match_point->second_team_goals }}
                                    </td>
                                    <td>
                                        <form
                                            action="{{ route('leagueGroupMatchPoints.destroy', $leagueGroupTeamMatch->league_group_match_point->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-icon"><i
                                                    class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="row text-right m-5">
                        <div class="col-md-12">
                            <div class="alert alert-info">
                                <span>
                                    لم يتم اضافة نتيجة للمباراة.
                                </span>
                            </div>

                        </div>
                    </div>
                @endif


            </div>
        </div>
    </div>
@endsection
