@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueGroups'
])

<style>
    p {
        margin: 0 !important;
    }

    .table-responsive {
        overflow: hidden !important;
    }

    .table-hover {
        background-image: linear-gradient(to bottom, #45DA9E, #008ECC);
    }

    tbody {
        color: #ffffff;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض بيانات المباراة</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('leagueGroups.show', $leagueGroup->id) }}">
                                العودة</a>
                        </div>
                    </div>
                </div>
                @foreach ($grouped_array as $grouped_group)

                <div class="mx-3 my-3 text-right">
                    <h5>
                        <strong>
                            {{ $grouped_group[0]['league_group_name_name'] }} - {{ $grouped_group[0]['league_group_name_name_ar'] }}
                        </strong>
                    </h5>
                </div>

                    <div class=" table-hover table-responsive text-right">
                        <table class="table">
                            <thead>

                                <tr>
                                    <th>
                                        الفريق
                                    </th>
                                    <th>
                                        لعب
                                    </th>
                                    <th>
                                        تعادل
                                    </th>
                                    <th>
                                        فوز
                                    </th>
                                    <th>
                                        خسارة
                                    </th>
                                    <th>
                                        اهداف له
                                    </th>
                                    <th>
                             اهداف عليه
                                    </th>
                                    <th>
                                        الفارق
                                    </th>
                                    <th>
                                        النقاط
                                    </th>
                                </tr>

                            </thead>
                            <tbody>
                                @foreach ($grouped_group as $grouped_group_team)
                                <tr>
                                    <td>
                                        {{ $grouped_group_team['league_group_team_name'] }} - {{ $grouped_group_team['league_group_team_name_ar'] }}
                                    </td>
                                    <td>
                                        {{ $grouped_group_team['league_group_team_matches_played'] }}
                                    </td>
                                    <td>
                                        {{ $grouped_group_team['league_group_team_draws'] }}
                                    </td>
                                    <td>
                                        {{ $grouped_group_team['league_group_team_wins'] }}
                                    </td>
                                    <td>
                                        {{ $grouped_group_team['league_group_team_loses'] }}
                                    </td>
                                    <td>
                                        {{ $grouped_group_team['league_group_team_goals'] }}
                                    </td>
                                    <td>
                                        {{ $grouped_group_team['league_group_team_goals_on'] }}
                                    </td>
                                    <td>
                                        {{ $grouped_group_team['league_group_team_goals_diff'] }}
                                    </td>
                                    <td>
                                        {{ $grouped_group_team['league_group_team_points'] }}
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
