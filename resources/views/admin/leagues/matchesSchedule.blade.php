@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagues'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

    .table-hover td:nth-of-type(1) {
        width: 250px;
    }




</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض جدول المباريات</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('leagues.show',$league->id) }}"> العودة</a>
                        </div>
                    </div>
                </div>




                <div class="row text-right mt-3">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">

                        </div>
                    </div>
                </div>


                <div class="table-responsive text-right">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>الرقم</th>
                            <th>الفريق الاول</th>
                            <th>الفريق الثاني</th>
                            <th>إضافة موعد للمباراة</th>
                            <th>إضافة نتيجة المباراة</th>
                            <th>الإجراءات</th>
                        </thead>
                        <tbody>


                            @foreach ($league->league_teams as $leagueTeam)
                                @foreach ($leagueTeam->league_team_matches1 as $leagueTeamMatch)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            <img width="80" height="80"
                                                src="{{ asset('storage/images/leagueTeams/' . $leagueTeamMatch->league_teams1->flag_icon) }}">
                                            {{ $leagueTeamMatch->league_teams1->name }} -
                                            {{ $leagueTeamMatch->league_teams1->name_ar }}

                                        </td>
                                        <td>
                                            <img width="80" height="80"
                                                src="{{ asset('storage/images/leagueTeams/' . $leagueTeamMatch->league_teams2->flag_icon) }}">
                                            {{ $leagueTeamMatch->league_teams2->name }} -
                                            {{ $leagueTeamMatch->league_teams2->name_ar }}

                                        </td>
                                   
                                        <td>

                                            <a class="btn btn-success btn-icon"
                                            href="{{ route('leagueMatchSchedules.create', ['league_team_match_id' => $leagueTeamMatch->id]) }}">
                                            <i class="nc-icon nc-simple-add"></i></a>
                                        </td>
                                        <td>

                                            <a class="btn btn-success btn-icon"
                                            href="{{ route('leagueMatchPoints.create', ['league_team_match_id' => $leagueTeamMatch->id]) }}">
                                            <i class="nc-icon nc-simple-add"></i></a>
                                        </td>
                                        {{-- <td> {{ $league->team_players_number }} /
                                            {{ $leagueTeam->league_team_players->count() }}</td>
                                        <td>
                                            <img width="80" height="80"
                                                src="{{ asset('storage/images/leagueTeams/' . $leagueTeam->flag_icon) }}">
                                        </td> --}}
                                        <td>
                                            {{-- <form action="{{ route('leagueTeams.destroy', $leagueTeam->id) }}"
                                                method="POST"> --}}


                                                <a class="btn btn-info btn-icon"
                                                    href="{{ route('leagues.showMatchDetails', $leagueTeamMatch->id) }}"> <i
                                                        class="fa fa-eye"></i></a>
                                                {{-- <a class="btn btn-primary btn-icon"
                                                    href="{{ route('leagueTeams.edit', $leagueTeam->id) }}"><i
                                                        class="fa fa-edit"></i></a> --}}

                                                {{-- @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-icon"><i
                                                        class="fa fa-times"></i></button>
                                            </form> --}}
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection
