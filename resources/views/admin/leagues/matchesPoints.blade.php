@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagues'
])

<style>
    p {
        margin: 0 !important;
    }

    .table-responsive {
        overflow: hidden !important;
    }

    .table-hover {
        background-image: linear-gradient(to bottom, #45DA9E, #008ECC);
    }

    tbody {
        color: #ffffff;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض بيانات المباراة</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('leagues.show', $league->id) }}">
                                العودة</a>
                        </div>
                    </div>
                </div>

                <div class=" table-hover table-responsive text-right">
                    <table class="table">
                        <thead>

                            <tr>
                                <th>
                                    الفريق
                                </th>
                                <th>
                                    لعب
                                </th>
                                <th>
                                    تعادل
                                </th>
                                <th>
                                    فوز
                                </th>
                                <th>
                                    خسارة
                                </th>
                                <th>
                                    اهداف له
                                </th>
                                <th>
                                    اهداف عليه
                                </th>
                                <th>
                                    الفارق
                                </th>
                                <th>
                                    النقاط
                                </th>
                            </tr>

                        </thead>
                        <tbody>
                            @foreach ($teamsScore2 as $teamScore)
                                <tr>
                                    <td>
                                        {{ $teamScore['league_team_name'] }} - {{ $teamScore['league_team_name_ar'] }}
                                    </td>
                                    <td>
                                        {{ $teamScore['league_team_matches_played'] }}
                                    </td>
                                    <td>
                                        {{ $teamScore['league_team_draws'] }}
                                    </td>
                                    <td>
                                        {{ $teamScore['league_team_wins'] }}
                                    </td>
                                    <td>
                                        {{ $teamScore['league_team_loses'] }}
                                    </td>
                                    <td>
                                        {{ $teamScore['league_team_goals'] }}
                                    </td>
                                    <td>
                                        {{ $teamScore['league_team_goals_on'] }}
                                    </td>
                                    <td>
                                        {{ $teamScore['league_team_goals_diff'] }}
                                    </td>
                                    <td>
                                        {{ $teamScore['league_team_points'] }}
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
