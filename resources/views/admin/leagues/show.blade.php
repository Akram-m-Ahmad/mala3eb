@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagues'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

    .table-hover td:nth-of-type(1) {
        width: 250px;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض الدوري</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('leagues.index') }}"> العودة</a>
                        </div>
                    </div>
                </div>

                <div class=" table-hover table-responsive text-right">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>الاسم بالانجليزي: </strong>
                                </td>
                                <td>
                                    {{ $league->name }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>الاسم بالعربي: </strong>
                                </td>
                                <td>
                                    {{ $league->name_ar }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>ذهاب و إياب: </strong>
                                </td>
                                <td>
                                    {{ $league->isHome ? 'نعم' : 'لا' }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>السعر: </strong>

                                </td>
                                <td>
                                    {{ $league->price }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>التفاصيل بالانجليزي: </strong>
                                </td>
                                <td>
                                    {{ $league->details }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>التفاصيل بالعربي: </strong>
                                </td>
                                <td>
                                    {{ $league->details_ar }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>عدد الفرق: </strong>
                                </td>
                                <td>
                                    {{ $league->teams_number }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>عدد اللاعبين في الفريق الواحد: </strong>
                                </td>
                                <td>
                                    {{ $league->team_players_number }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>عدد اللاعبين المسجلين: </strong>
                                </td>
                                <td>
                                    @php
                                        $countGroupsPlayers = 0;
                                        foreach ($league->groups as $group) {
                                            $countGroupsPlayers += $group->group_users->count() + 1;
                                        }
                                    @endphp
                                    {{ $league->teams_number * $league->team_players_number }} /
                                    {{ $league->league_registered_users->count() + $countGroupsPlayers }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>تاريخ بداية الدوري: </strong>
                                </td>
                                <td>
                                    {{ $league->start_at }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>تاريخ بداية الانضمام: </strong>
                                </td>
                                <td>
                                    {{ $league->join_start_at }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>تاريخ نهاية الانضمام: </strong>
                                </td>
                                <td>
                                    {{ $league->join_end_at }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>الملعب: </strong>

                                </td>
                                <td>
                                    {{ $league->football_field->name_ar }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>المدينة: </strong>
                                </td>
                                <td>
                                    {{ $league->city->name_ar }} - {{ $league->city->country->name_ar }}
                                </td>
                            </tr>


                            <tr>

                                @if ($league->league_teams->count() < $league->teams_number)
                                    <td>
                                        <strong>
                                            إشعار:
                                        </strong>
                                    </td>
                                    <td>
                                        <div class="alert alert-danger mb-0">
                                            <strong>إن عدد الفرق غير مكتمل. يجب إضافة
                                                {{ $league->teams_number - $league->league_teams->count() }} فرق</strong>
                                            <a class="btn btn-info" target="_blank"
                                                href="{{ route('leagueTeams.create', ['league_id' => $league->id]) }}">
                                                اضافة
                                                فرق</a>
                                        </div>


                                    </td>
                                @else
                                    @if ($league->league_teams->count() > $league->teams_number)
                                        <td>
                                            <strong>
                                                إشعار:
                                            </strong>
                                        </td>

                                        <td>
                                            <div class="alert alert-danger mb-0">
                                                <strong>إن عدد الفرق زائد. يجب إزالة
                                                    {{ $league->league_teams->count() - $league->teams_number }}
                                                    فرق</strong>
                                            </div>
                                        </td>
                                    @else
                                        <td>
                                            <strong>
                                                إشعار:
                                            </strong>
                                        </td>
                                        <td>
                                            <div class="alert alert-info mb-0">
                                                <strong> عدد الفرق مكتمل</strong>
                                            </div>
                                        </td>
                                    @endif
                                @endif
                            </tr>

                            <tr>
                                <td>
                                    <strong> إشعار:</strong>
                                </td>
                                <td>
                                    @if (count($usersIds) < $league->league_registered_users->count() + $countGroupsPlayers)
                                        <div class="alert alert-danger mb-0">
                                            <strong> لم يتم إضافة جميع المستخدمين المسجلين في الدوري الى الفرق. المتبقي
                                                {{ $league->league_registered_users->count() + $countGroupsPlayers - count($usersIds) }}
                                                لاعبين</strong>
                                        </div>
                                    @else
                                        @if ($league->league_registered_users->count() === 0 && $league->league_registered_groups->count() === 0)
                                            <div class="alert alert-info mb-0">
                                                <strong> لا يوجد لاعبين مسجلين بعد.</strong>
                                            </div>
                                        @else
                                            <div class="alert alert-info mb-0">
                                                <strong> تم إضافة جميع اللاعبين المسجلين الى الفرق</strong>
                                            </div>
                                        @endif
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>صورة العرض: </strong>
                                </td>
                                <td>
                                    <img src="{{ asset('storage/images/leagues/' . $league->main_image) }}"
                                        width="200px">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>يمكنك الاطلاع على التفاصيل المادية من هنا: </strong>
                                </td>
                                <td>
                                    <a class="btn btn-warning btn-icon"
                                        href="{{ route('leagues.showFinancial', $league->id) }}">
                                        <i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        إنشاء جدول المباريات:
                                    </strong>
                                </td>
                                <td>
                                    @if ($league->isHome)
                                        @if ($countScheduledMatches === $league->teams_number * ($league->teams_number - 1))
                                            <div class="alert alert-info mb-0">
                                                <strong> تم إنشاء جدول المباريات</strong>
                                            </div>
                                        @else
                                            <a class="btn btn-info"
                                                href="{{ route('leagues.createMatches', $league->id) }}">
                                                إضافة الجدول
                                            </a>
                                        @endif
                                    @else
                                        @php
                                            $leagueTeamsMatches = 0;
                                            for ($i = $league->teams_number - 1; $i > 0; $i--) {
                                                $leagueTeamsMatches += $i;
                                            }
                                        @endphp

                                        @if ($countScheduledMatches === $leagueTeamsMatches)
                                            <div class="alert alert-info mb-0">
                                                <strong> تم إنشاء جدول المباريات</strong>
                                            </div>
                                        @else
                                            <a class="btn btn-info"
                                                href="{{ route('leagues.createMatches', $league->id) }}">
                                                إضافة الجدول
                                            </a>
                                        @endif
                                    @endif

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        جدول المباريات:
                                    </strong>
                                </td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('leagues.matchesSchedule', $league->id) }}">
                                        الجدول
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        جدول النقاط:
                                    </strong>
                                </td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('leagues.matchesPoints', $league->id) }}">
                                        الجدول
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>


            </div>



            <div class="row text-right mt-3">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">

                    </div>
                </div>
            </div>


            <div class="table-responsive text-right">
                <table class="table">
                    <thead class=" text-primary">
                        <th>الرقم</th>
                        <th>اسم الفريق بالانجليزي</th>
                        <th>اسم الفريق بالعربي</th>
                        <th>عدد اللاعبين</th>
                        <th>علم الفريق</th>
                        <th>الإجراءات</th>
                    </thead>
                    <tbody>


                        @foreach ($league->league_teams as $leagueTeam)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $leagueTeam->name_ar }}</td>
                                <td>{{ $leagueTeam->name }}</td>
                                <td> {{ $league->team_players_number }} /
                                    {{ $leagueTeam->league_team_players->count() }}</td>
                                <td>
                                    <img width="80" height="80"
                                        src="{{ asset('storage/images/leagueTeams/' . $leagueTeam->flag_icon) }}">
                                </td>
                                <td>
                                    <form action="{{ route('leagueTeams.destroy', $leagueTeam->id) }}" method="POST">

                                        <a class="btn btn-success btn-icon"
                                            href="{{ route('leagueTeamPlayers.create', ['league_team_id' => $leagueTeam->id]) }}">
                                            <i class="nc-icon nc-simple-add"></i></a>

                                        <a class="btn btn-info btn-icon"
                                            href="{{ route('leagueTeams.show', $leagueTeam->id) }}"> <i
                                                class="fa fa-eye"></i></a>
                                        <a class="btn btn-primary btn-icon"
                                            href="{{ route('leagueTeams.edit', $leagueTeam->id) }}"><i
                                                class="fa fa-edit"></i></a>

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-icon"><i
                                                class="fa fa-times"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
