@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagues'
])
@section('content')

    <div class="content">

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('تعديل بيانات الدوري') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ route('leagues.index') }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger text-right">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form class="col-md-12" action="{{ route('leagues.update', $league->id) }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-row text-right">
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم الدوري بالانجليزي') }}</label>
                            <input value="{{ $league->name }}" type="text" name="name" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم الدوري بالعربي') }}</label>
                            <input value="{{ $league->name_ar }}" type="text" name="name_ar" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('ذهاب و إياب') }}</label>
                            <select name="isHome" class="custom-select">
                                <option value="1">نعم
                                </option>
                                <option value="0">لا
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('السعر') }}</label>
                            <input value="{{ $league->price }}" type="number" name="price" class="form-control">

                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('التفاصيل بالانجليزي') }}</label>
                            <input value="{{ $league->details }}" type="text" name="details" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('التفاصيل بالعربي') }}</label>
                            <input value="{{ $league->details_ar }}" type="text" name="details_ar"
                                class="form-control">
                        </div>


                        <div class="form-group col-md-12">
                            <label>{{ __('عدد الفرق') }}</label>
                            <input value="{{ $league->teams_number }}" type="number" name="teams_number"
                                class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('عدد اللاعبين في الفريق الواحد') }}</label>
                            <input value="{{ $league->team_players_number }}" type="number" name="team_players_number"
                                class="form-control">
                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('تاريخ بداية الدوري') }}</label>
                            <input value="{{ $league->start_at }}" type="date" name="start_at" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('تاريخ بداية الانضمام') }}</label>
                            <input value="{{ $league->join_start_at }}" type="date" name="join_start_at"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('تاريخ نهاية الانضمام') }}</label>
                            <input value="{{ $league->join_end_at }}" type="date" name="join_end_at"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('الملعب') }}</label>
                            <select name="football_field_id" class="custom-select">
                                @foreach ($footballFields as $footballField)
                                    <option value={{ $footballField->id }}>{{ $footballField->name_ar }} -
                                        {{ $footballField->name_ar }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('المدينة') }}</label>
                            <select name="city_id" class="custom-select">
                                @foreach ($cities as $city)
                                    <option value={{ $city->id }}>{{ $city->name_ar }} -
                                        {{ $city->country->name_ar }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="main_image" style="cursor: pointer;">{{ __('اختر صورة العرض') }} <br> <i
                                    style="font-size:40px" class="nc-icon nc-image"></i>
                            </label>
                            <input type="file" class="form-control-file" id="main_image" name="main_image">
                        </div>
                    </div>
                    <div class="card-footer ">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-info btn-round">{{ __('تعديل البيانات') }}</button>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        var loadFile = function(event) {
            var image = document.getElementById('first_image');
            image.src = URL.createObjectURL(event.target.files[0]);
            console.log(event.target.files[0])
        };
        var loadFile2 = function(event) {
            var image2 = document.getElementById('second_image');
            image2.src = URL.createObjectURL(event.target.files[1]);
            console.log(event.target.files[1])
        };
        var loadFile3 = function(event) {
            var image3 = document.getElementById('third_image');
            image3.src = URL.createObjectURL(event.target.files[2]);
            console.log(event.target.files[0])
        };
    </script>
@endpush
