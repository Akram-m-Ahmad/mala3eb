@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagues'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif

        <div class="card">
            <div class="pull-right" style="padding-left:10px">
                <a class="btn btn-success" href="{{ route('leagues.create') }}"> إضافة دوري جديد</a>
            </div>
            <div class="card-header text-right">
                <h4 class="card-title"> قائمة الدوريات</h4>
            </div>
            <div class="row mx-3">
                <form method="GET" action="{{ route('leagues.index') }}">
                    <div class="input-group no-border">
                        <input type="text" value="" class="form-control" value="{{ request()->query('search') }}"
                            name="search" placeholder="البحث...">
                        {{-- <div class="input-group-append">
                                <div class="input-group-text">
                                    <i class="nc-icon nc-zoom-split"></i>
                                </div>
                            </div> --}}
                        <button type="submit" class="btn btn-info btn-icon">البحث</button>

                    </div>
                </form>
            </div>
            <div class="card-body">
                @if (count($leagues) > 0)
                <div class="table-responsive text-right">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>الرقم</th>
                            <th>اسم الملعب</th>
                            <th>اسم الدوري بالانجليزي</th>
                            <th>اسم الدوري بالعربي</th>
                            <th>الدولة والمدينة</th>
                            <th>الإجراءات</th>
                        </thead>
                        <tbody>


                            @foreach ($leagues as $league)
                                <tr>
                                    <td>{{ ++$i }}</td>

                                    <td>{{ $league->football_field->name_ar }}</td>
                                    <td>{{ $league->name }}</td>
                                    <td>{{ $league->name_ar }}</td>
                                    <td>{{ $league->city->name_ar }} - {{ $league->city->country->name_ar }}</td>
                                    <td>
                                        <form action="{{ route('leagues.destroy', $league->id) }}"
                                            method="POST">
                                            <a class="btn btn-info btn-icon  "
                                                href="{{ route('leagues.show', $league->id) }}"> <i
                                                    class="fa fa-eye"></i></a>
                                            <a class="btn btn-primary btn-icon"
                                                href="{{ route('leagues.edit', $league->id) }}"><i
                                                    class="fa fa-edit"></i></a>

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-icon"><i
                                                    class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                @if ($search)
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <h6>لا يوجد نتائج مطابقة لعملية البحث "{{ $search }}"</h6>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <h6>لم يتم إدخال البيانات بعد</h6>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
            </div>

        </div>
        <div>
            {{-- {!! $products->links('vendor.pagination.simple-tailwind') !!} --}}
            {!! $leagues->appends(['search' => request()->query('search')])->links('vendor.pagination.bootstrap-4') !!}
        </div>
    </div>
@endsection
