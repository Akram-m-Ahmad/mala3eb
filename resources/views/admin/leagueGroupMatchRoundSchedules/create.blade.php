@extends('layouts.app', [
'class' => '',
'elementActive' => 'leagueGroups'
])


@section('content')
<style>
 /* #mydate{
    -webkit-transform: scaleX(-1);
  transform: scaleX(-1);
 } */
</style>
    <div class="content">

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('أضف موعد للمباراة') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ url()->previous() }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger text-right">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if ($leagueGroupMatchRound->league_group_match_round_schedule)
                <div class="row text-right m-5">
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            <span>لا يمكن اضافة موعد للمباراة. هنالك موعد تم ادخاله مسبقاً. الرجاء حذف الموعد وادخال موعد جديد.</span>
                        </div>

                    </div>
                </div>
            @else
                <form class="col-md-12" action="{{ route('leagueGroupMatchRoundSchedules.store') }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-row text-right">
                            <div class="form-group col-md-12">
                                <label>{{ __('تاريخ المباراة') }}</label>
                                <input value="{{Carbon\Carbon::now()->format('Y-m-d')}}" type="date" name="date" id="mydate" class="form-control">

                            </div>
                            <div class="form-group col-md-12">
                                <label>{{ __('توقيت المباراة') }}</label>
                                <input placeholder="10:00 AM" type="time" name="time" style="direction: ltr !important" class="form-control">
                                <input type="hidden" value="{{ $leagueGroupMatchRoundId }}"
                                    name="league_g_m_round_id" class="form-control">

                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-info btn-round">{{ __('أضف الى الجدول') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
             
            @endif


        </div>

    </div>




@endsection
