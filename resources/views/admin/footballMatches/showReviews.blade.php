@extends('layouts.app', [
'class' => '',
'elementActive' => 'footballMatches'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض تعليقات وتقييمات المستخدمين</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('footballMatches.show', $footballMatch->id) }}">
                                العودة</a>
                        </div>
                    </div>
                </div>

                <div class="row text-right mt-3">
                    <div class="col-md-12">
                        <h5>قائمة اللاعبون المسجلون بشكل فردي</h5>
                    </div>
                </div>
                <div class="table-responsive text-right">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>الرقم</th>
                            <th>اسم اللاعب</th>
                            <th>التعليق</th>
                            <th>التقييم</th>
                            <th>الإجراءات</th>
                        </thead>
                        <tbody>


                            @foreach ($footballMatch->football_match_reviews as $footballMatchReview)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $footballMatchReview->user->name }} - {{ $footballMatchReview->user->phone }}
                                    </td>
                                    <td> {{ $footballMatchReview->comment }} </td>
                                    <td> {{ $footballMatchReview->rating }} </td>

                                    <td>
                                        <form
                                            action="{{ route('footballMatchReviews.destroy', $footballMatchReview->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-icon"><i
                                                    class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
