@extends('layouts.app', [
'class' => '',
'elementActive' => 'footballMatches'
])
@section('content')

    <div class="content">

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('تعديل بيانات المباراة') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ route('footballMatches.index') }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger text-right">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form class="col-md-12" action="{{ route('footballMatches.update', $footballMatch->id) }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-row text-right">
                        <div class="form-group col-md-12">
                            <label>{{ __('الملعب') }}</label>
                            <select name="football_field_id" class="custom-select">
                                @foreach ($footballFields as $footballField)
                                    <option value={{ $footballField->id }}>{{ $footballField->name }} -
                                        {{ $footballField->name_ar }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('تاريخ المباراة') }}</label>
                            <input value="{{ $footballMatch->date }}" type="date" name="date" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('وقت المباراة') }}</label>
                            <input value="{{ $footballMatch->time }}" type="time" name="time" class="form-control">

                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('سعر المباراة للشخص الواحد') }}</label>
                            <input value="{{ $footballMatch->price }}" type="number" name="price" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('عدد اللاعبين') }}</label>
                            <input value="{{ $footballMatch->capacity }}" type="number" name="capacity"
                                class="form-control">
                        </div>


                    </div>
                </div>
                <div class="card-footer ">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-info btn-round">{{ __('تعديل البيانات') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        var loadFile = function(event) {
            var image = document.getElementById('first_image');
            image.src = URL.createObjectURL(event.target.files[0]);
            console.log(event.target.files[0])
        };
        var loadFile2 = function(event) {
            var image2 = document.getElementById('second_image');
            image2.src = URL.createObjectURL(event.target.files[1]);
            console.log(event.target.files[1])
        };
        var loadFile3 = function(event) {
            var image3 = document.getElementById('third_image');
            image3.src = URL.createObjectURL(event.target.files[2]);
            console.log(event.target.files[0])
        };
    </script>
@endpush
