@extends('layouts.app', [
'class' => '',
'elementActive' => 'footballMatches'
])

<style>
    p {
        margin: 0 !important;
    }

</style>
@section('content')
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض المباراة</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('footballMatches.index') }}"> العودة</a>
                        </div>
                    </div>
                </div>


                <div class="row text-right">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>اسم الملعب: </strong>
                            {{ $footballMatch->football_field->name }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>تاريخ ووقت المباراة: </strong>
                            {{ $footballMatch->time }} - {{ $footballMatch->date }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>السعر: </strong>
                            {{ $footballMatch->price }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>عدد اللاعبين: </strong>
                            {{ $footballMatch->capacity }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong> اللاعبين المسجلين كأفراد: </strong>
                            @foreach ($footballMatch->football_match_registered_users as $football_match_registered_user)
                                <p>{{ $loop->iteration }}- {{ $football_match_registered_user->name }}</p>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong> اجمالي مبلغ اللاعبين المسجلين كأفراد: </strong>
                            <p>
                                {{ $footballMatch->football_match_registered_users->sum('pivot.price') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong> اللاعبين المسجلين كمجموعات: </strong>
                            @foreach ($footballMatch->groups as $group)
                                <p>{{ $loop->iteration }}-
                                    <strong>اسم المجموعة: {{ $group->name }}</strong></p>
                                  <p>  {{ $group->user->name }}</p>
                                @foreach ($group->group_users as $group_user)
                                    <p> {{ $group_user->name }}</p>
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong> اجمالي مبلغ اللاعبين المسجلين كمجموعات: </strong>
                            <p>
                                {{ $footballMatch->football_match_registered_groups->sum('pivot.price') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong> اللاعبين المسجلين من الملعب: </strong>
                            @foreach ($footballMatch->football_match_registered_owner_players as $player)
                                <p>{{ $loop->iteration }}- {{ $player->name }}</p>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
