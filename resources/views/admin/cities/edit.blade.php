@extends('layouts.app', [
'class' => '',
'elementActive' => 'cities'
])
@section('content')

    <div class="content">

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('تعديل بيانات المدينة') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ route('cities.index') }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger text-right">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="col-md-12" action="{{ route('cities.update',$city->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-row text-right">
                        <div class="form-group col-md-12">
                            <label>{{ __('الدولة') }}</label>
                            <select name="country_id" class="custom-select">
                                @foreach ($countries as $country)
                                    <option value={{ $country->id }}>{{ $country->name_ar }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم المدينة بالانجليزي') }}</label>
                            <input value="{{$city->name}}" placeholder="Riyadh" type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم المدينة بالعربي') }}</label>
                            <input value="{{$city->name_ar}}" placeholder="الرياض" type="text" name="name_ar" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-info btn-round">{{ __('تعديل البيانات') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <script>
        $('.select2').select2();
    </script>

@endsection
@push('scripts')
    <script>
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
@endpush
