@extends('layouts.app', [
'class' => '',
'elementActive' => 'users'
])
@section('content')

    <div class="content">

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('أضف مستخدم جديد') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ route('users.index') }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form class="col-md-12" action="{{ route('users.store') }}" method="POST"
                enctype="multipart/form-data">
                @csrf


                <div class="card-body">
                    <div class="form-row text-right">
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم المستخدم') }}</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('رقم الجوال') }}</label>
                            <input type="text" name="phone" class="form-control" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('البريد الالكتروني') }}</label>
                            <input type="email" name="email" class="form-control" required>
                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('كلمة السر') }}</label>
                            <input type="password" name="password" class="form-control" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('الدولة') }}</label>
                            <select name="country_id" class="custom-select custom-select-md">
                             @foreach ($countries as $country)
                                 <option value="{{$country->id}}">{{$country->name_ar}} - {{$country->name}}</option>
                             @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('المدينة') }}</label>
                            <select name="city_id" class="custom-select custom-select-md">
                             @foreach ($cities as $city)
                                 <option value="{{$city->id}}">{{$city->name_ar}} - {{$city->name}}</option>
                             @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('الوظيفة') }}</label>
                            <select name="role" class="custom-select custom-select-md">
                             @foreach ($roles as $role)
                                 <option value="{{$role->name}}">{{$role->name}}</option>
                             @endforeach
                            </select>
                        </div>
                    </div>
                </div>



                <div class="card-footer ">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-info btn-round">{{ __('Add User') }}</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
    </div>




@endsection
@push('scripts')
    <script>
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
@endpush
