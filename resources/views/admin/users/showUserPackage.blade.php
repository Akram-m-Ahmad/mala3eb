@extends('layouts.app', [
'class' => '',
'elementActive' => 'users'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

    .table-hover td:nth-of-type(1) {
        width: 250px;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض باقات المستخدم</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('users.show', $user->id) }}"> العودة</a>
                        </div>
                    </div>
                </div>

                {{-- <div class=" table-hover table-responsive text-right">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>اسم المستخدم: </strong>
                                </td>
                                <td>
                                    {{ $user->name }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>البريد الالكتروني: </strong>
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>رقم الجوال: </strong>
                                </td>
                                <td>
                                    {{ $user->phone }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> --}}
            </div>

            @if (count($user->membership_user) > 0)
                <div class="table-responsive text-right">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>الرقم</th>
                            <th>اسم الباقة</th>
                            <th>السعر</th>
                            <th>طريقة الدفع</th>
                            <th>عدد المباريات المتبقي</th>
                            <th>تاريخ بداية الاشتراك</th>
                            <th>تاريخ نهاية الاشتراك</th>
                            <th>الإجراءات</th>
                        </thead>
                        <tbody>


                            @foreach ($user->membership_user as $membership_user)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $membership_user->name_ar }} - {{ $membership_user->name }}</td>
                                    <td>{{ $membership_user->pivot->price }} {{ $membership_user->country->unit_ar }}
                                    </td>
                                    <td> {{ $membership_user->payment_method_membership_user->first()->name_ar }}
                                    </td>
                                    <td>{{ $membership_user->pivot->remaining }}</td>
                                    <td>{{ $membership_user->pivot->start_at }}</td>
                                    <td>{{ $membership_user->pivot->end_at }}</td>
                                    <td>
                                        <form
                                            action="{{ route('membershipUsers.destroy', $membership_user->pivot->id) }}"
                                            method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-icon"><i
                                                    class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="mx-3">
                    <div class="text-right">
                        <p>لم يقم المستخدم بالاشتراك بالباقات بعد</p>
                    </div>
                </div>
            @endif

        </div>
    </div>
@endsection
