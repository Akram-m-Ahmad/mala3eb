@extends('layouts.app', [
'class' => '',
'elementActive' => 'users'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

    .table-hover td:nth-of-type(1) {
        width: 250px;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض المستخدم</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('users.index') }}"> العودة</a>
                        </div>
                    </div>
                </div>

                <div class=" table-hover table-responsive text-right">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>اسم المستخدم: </strong>
                                </td>
                                <td>
                                    {{ $user->name }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>البريد الالكتروني: </strong>
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>رقم الجوال: </strong>
                                </td>
                                <td>
                                    {{ $user->phone }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>الدولة: </strong>

                                </td>
                                <td>
                                    {{ $user->country->name_ar }} - {{ $user->country->name }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>المدينة: </strong>
                                </td>
                                <td>
                                    {{ $user->city->name_ar }} - {{ $user->city->name }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>الوظيفة: </strong>
                                </td>
                                <td>
                                    @foreach ($user->roles as $role)
                                        {{ $role->display_name }}
                                    @endforeach
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>

            @if (count($user->groups) > 0)
                <div class="mx-3">
                    <div class="text-right">
                        <h5>
                            <strong>المجموعات: </strong>
                        </h5>
                    </div>
                </div>

                @foreach ($user->groups as $group)
                    <div class="mx-3">
                        <div class="text-right">
                            <h6>
                                <strong>{{ $loop->iteration }} -اسم المجموعة: {{ $group->name }} </strong>
                                <form class="d-inline-block" action="{{ route('groups.destroy', $group->id) }}"
                                    method="POST">
                                    <a class="btn btn-primary btn-icon" href="{{ route('users.edit', $user->id) }}"><i
                                            class="fa fa-edit"></i></a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-icon"><i
                                            class="fa fa-times"></i></button>
                                </form>
                            </h6>

                            <h6>
                                <strong>المستخدمين:</strong>
                            </h6>
                        </div>
                    </div>
                    @if (count($group->group_users) > 0)
                    <div class="table-responsive text-right">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>الرقم</th>
                                <th>اسم المستخدم</th>
                                <th>البريد الالكتروني</th>
                                <th>رقم الجوال</th>
                                <th>الإجراءات</th>
                            </thead>
                            <tbody>


                                @foreach ($group->group_users as $group_user)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $group_user->name }}</td>
                                        <td>{{ $group_user->email }}</td>
                                        <td>{{ $group_user->phone }}</td>
                                        <td>
                                            <form action="{{ route('groupUsers.destroy', $group_user->pivot->id) }}"
                                                method="POST">
                                                <a class="btn btn-info btn-icon  "
                                                    href="{{ route('users.show', $group_user->id) }}"> <i
                                                        class="fa fa-eye"></i></a>
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-icon"><i
                                                        class="fa fa-times"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="mx-3">
                        <div class="text-right">
                            <p>لا يوجد مستخدمين في المجموعة</p>
                        </div>
                    </div>
                    @endif
                 
                @endforeach
            @endif
            <div class="row text-right mt-3">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
