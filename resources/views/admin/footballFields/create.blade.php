@extends('layouts.app', [
'class' => '',
'elementActive' => 'footballFields'
])
@section('content')

    <div class="content">

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('أضف ملعب جديد') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ route('footballFields.index') }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger text-right">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form class="col-md-12" action="{{ route('footballFields.store') }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-row text-right">
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم الملعب بالانجليزي') }}</label>
                            <input placeholder="Althumamah stadium" type="text" name="name" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم الملعب بالعربي') }}</label>
                            <input placeholder="الثمامة" type="text" name="name_ar" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('المالك') }}</label>
                            <select name="user_id" class="custom-select">
                                @foreach ($owners as $owner)
                                    <option value={{ $owner->id }}>{{ $owner->name }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('احداثيات خط العرض') }}</label>
                            <input placeholder="24.7037952" type="text" name="lat" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('احداثيات خط الطول') }}</label>
                            <input placeholder="46.7140608" type="text" name="lng" class="form-control">

                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('الموقع بالانجليزي') }}</label>
                            <input placeholder="Riyadh - althumamah road - near MacDonalds" type="text" name="location"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('الموقع بالعربي') }}</label>
                            <input placeholder="الرياض - الثمامة - بالقرب من مطعم ماكدونالدز" type="text" name="location_ar"
                                class="form-control">
                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('التفاصيل بالانجليزي') }}</label>
                            <input
                                placeholder="The stadium provides a wide area for playing 10 vs 10. In addition to a place for your friends to watch you and enjoy the game."
                                type="text" name="details" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('التفاصيل بالعربي') }}</label>
                            <input
                                placeholder="يوفر الملعب مساحة واسعة للعب 10 ضد 10 بالإضافة إلى مكان لأصدقائك لمشاهدتك والاستمتاع باللعبة."
                                type="text" name="details_ar" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('المساحة بالمتر المربع') }}</label>
                            <input placeholder="200" type="text" name="area" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('المدينة') }}</label>
                            <select name="city_id" class="custom-select">
                                @foreach ($cities as $city)
                                    <option value={{ $city->id }}>{{ $city->name_ar }} -
                                        {{ $city->country->name_ar }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('مفعّل') }}</label>
                            <select name="isActive" class="custom-select">
                                {{-- @foreach ($cities as $city) --}}
                                    <option value="1">نعم
                                    </option>
                                    <option value="0">لا
                                    </option>
                                {{-- @endforeach --}}
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="first_image" style="cursor: pointer;">{{ __('اختر الصورة الاولى') }} <br> <i
                                    style="font-size:40px" class="nc-icon nc-image"></i>
                            </label>
                            <input type="file" class="form-control-file" id="first_image" name="first_image">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="second_image" style="cursor: pointer;">{{ __('اختر الصورة الثانية') }} <br> <i
                                    style="font-size:40px" class="nc-icon nc-image"></i>
                            </label>
                            <input type="file" class="form-control-file" id="second_image" name="second_image">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="third_image" style="cursor: pointer;">{{ __('اختر الصورة الثالثة') }} <br> <i
                                    style="font-size:40px" class="nc-icon nc-image"></i>
                            </label>
                            <input type="file" class="form-control-file" id="third_image" name="third_image">
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-info btn-round">{{ __('أضف الملعب') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        var loadFile = function(event) {
            var image = document.getElementById('first_image');
            image.src = URL.createObjectURL(event.target.files[0]);
            console.log(event.target.files[0])
        };
        var loadFile2 = function(event) {
            var image2 = document.getElementById('second_image');
            image2.src = URL.createObjectURL(event.target.files[1]);
            console.log(event.target.files[1])
        };
        var loadFile3 = function(event) {
            var image3 = document.getElementById('third_image');
            image3.src = URL.createObjectURL(event.target.files[2]);
            console.log(event.target.files[0])
        };
    </script>
@endpush
