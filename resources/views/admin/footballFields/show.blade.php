@extends('layouts.app', [
'class' => '',
'elementActive' => 'footballFields'
])
@section('content')
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض الملعب</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('footballFields.index') }}"> العودة</a>
                        </div>
                    </div>
                </div>


                <div class="row text-right">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>الاسم بالانجليزي: </strong>
                            {{ $footballField->name }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>الاسم بالعربي: </strong>
                            {{ $footballField->name_ar }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>اسم المالك</strong>
                            {{ $footballField->user->name }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>احداثيات خط العرض: </strong>
                            {{ $footballField->lat }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>احداثيات خط الطول: </strong>
                            {{ $footballField->lng }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>الموقع بالانجليزي: </strong>
                            {{ $footballField->location }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>الموقع بالعربي: </strong>
                            {{ $footballField->location_ar }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>التفاصيل بالانجليزي: </strong>
                            {{ $footballField->details }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>التفاصيل بالعربي: </strong>
                            {{ $footballField->details_ar }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>المساحة بالمتر المربع: </strong>
                            {{ $footballField->area }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>المدينة و الدولة</strong>
                            {{ $footballField->city->name_ar }} -   {{ $footballField->city->country->name_ar }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>مفعّل: </strong>
                            {{ $footballField->isActive ? 'نعم' : 'لا' }}
                        </div>
                    </div>
              
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>الصورة الاولى: </strong>
                            <img src="{{ asset('storage/images/footballFields/' . $footballField->first_image) }}" width="200px">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>الصورة الثانية: </strong>
                            <img src="{{ asset('storage/images/footballFields/' . $footballField->second_image) }}" width="200px">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>الصورة الثالثة: </strong>
                            <img src="{{ asset('storage/images/footballFields/' . $footballField->third_image) }}" width="200px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
