@extends('layouts.app', [
'class' => '',
'elementActive' => 'footballFields'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif

        <div class="card">
            <div class="pull-right" style="padding-left:10px">
                <a class="btn btn-success" href="{{ route('footballFields.create') }}"> إضافة ملعب جديد</a>
            </div>
            <div class="card-header text-right">
                <h4 class="card-title"> قائمة الملاعب</h4>
            </div>
            <div class="row mx-3">
                <form method="GET" action="{{ route('footballFields.index') }}">
                    <div class="input-group no-border">
                        <input type="text" value="" class="form-control" value="{{ request()->query('search') }}"
                            name="search" placeholder="البحث...">
                        {{-- <div class="input-group-append">
                                <div class="input-group-text">
                                    <i class="nc-icon nc-zoom-split"></i>
                                </div>
                            </div> --}}
                        <button type="submit" class="btn btn-info btn-icon">البحث</button>

                    </div>
                </form>
            </div>
            <div class="card-body">
                @if (count($footballFields) > 0)
                    <div class="table-responsive text-right">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>الرقم</th>
                                <th>الاسم بالانجليزي</th>
                                <th>الاسم بالعربي</th>
                                <th>المالك</th>
                                <th>رقم المالك</th>
                                <th>الإجراءات</th>
                            </thead>
                            <tbody>


                                @foreach ($footballFields as $footballField)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $footballField->name }}</td>
                                        <td>{{ $footballField->name_ar }}</td>
                                        <td>{{ $footballField->user->name }}</td>
                                        <td>{{ $footballField->user->phone }}</td>
                                        <td>
                                            <form action="{{ route('footballFields.destroy', $footballField->id) }}"
                                                method="POST">
                                                <a class="btn btn-info btn-icon  "
                                                    href="{{ route('footballFields.show', $footballField->id) }}"> <i
                                                        class="fa fa-eye"></i></a>

                                                <a class="btn btn-primary btn-icon"
                                                    href="{{ route('footballFields.edit', $footballField->id) }}"><i
                                                        class="fa fa-edit"></i></a>

                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-icon"><i
                                                        class="fa fa-times"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    @if ($search)
                        <div class="row">
                            <div class="col-12">
                                <div class="text-center">
                                    <h6>لا يوجد نتائج مطابقة لعملية البحث "{{ $search }}"</h6>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-12">
                                <div class="text-center">
                                    <h6>لم يتم إدخال البيانات بعد</h6>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
            </div>

        </div>
        <div>
            {{-- {!! $products->links('vendor.pagination.simple-tailwind') !!} --}}
            {!! $footballFields->appends(['search' => request()->query('search')])->links('vendor.pagination.bootstrap-4') !!}
        </div>
    </div>
@endsection
