@extends('layouts.app', [
'class' => '',
'elementActive' => 'packageMemberships'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif

        <div class="card">
            <div class="pull-right" style="padding-left:10px">
                <a class="btn btn-success" href="{{ route('packageMemberships.create') }}"> إضافة باقة جديدة</a>
            </div>
            <div class="card-header text-right">
                <h4 class="card-title"> قائمة الباقات</h4>
            </div>

            <div class="row mx-3">
                <form method="GET" action="{{ route('packageMemberships.index') }}">
                    <div class="input-group no-border">
                        <input type="text" value="" class="form-control" value="{{ request()->query('search') }}"
                            name="search" placeholder="البحث...">
                        {{-- <div class="input-group-append">
                                <div class="input-group-text">
                                    <i class="nc-icon nc-zoom-split"></i>
                                </div>
                            </div> --}}
                        <button type="submit" class="btn btn-info btn-icon">البحث</button>

                    </div>
                </form>
            </div>
            <div class="card-body">
                <div class="table-responsive text-right">
                    @if (count($packageMemberships) > 0)
                        <table class="table">
                            <thead class=" text-primary">
                                <th>الرقم</th>
                                <th>الاسم بالانجليزي</th>
                                <th>الاسم بالعربي</th>
                                <th>الدولة</th>
                                <th>المدينة</th>
                                <th>السعر</th>
                                <th>خلفية الباقة</th>
                                <th>الإجراءات</th>
                            </thead>
                            <tbody>


                                @foreach ($packageMemberships as $packageMembership)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $packageMembership->name }}</td>
                                        <td>{{ $packageMembership->name_ar }}</td>
                                        <td>{{ $packageMembership->country->name_ar }} -
                                            {{ $packageMembership->country->name }}</td>
                                        <td>{{ $packageMembership->city->name_ar }} -
                                            {{ $packageMembership->city->name }}</td>
                                            <td>{{ $packageMembership->price }}</td>
                                        <td><img src="{{ asset('storage/images/packageMemberships/' . $packageMembership->background_image) }}"
                                                width="100px">
                                        </td>
                                        <td>
                                            <form
                                                action="{{ route('packageMemberships.destroy', $packageMembership->id) }}"
                                                method="POST">
                                                <a class="btn btn-info btn-icon  "
                                                    href="{{ route('packageMemberships.show', $packageMembership->id) }}">
                                                    <i class="fa fa-eye"></i></a>

                                                <a class="btn btn-primary btn-icon"
                                                    href="{{ route('packageMemberships.edit', $packageMembership->id) }}"><i
                                                        class="fa fa-edit"></i></a>

                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-icon"><i
                                                        class="fa fa-times"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        @if ($search)
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <h6>لا يوجد نتائج مطابقة لعملية البحث "{{ $search }}"</h6>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <h6>لم يتم إدخال البيانات بعد</h6>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>

        </div>
        <div>
            {{-- {!! $products->links('vendor.pagination.simple-tailwind') !!} --}}
            {!! $packageMemberships->appends(['search' => request()->query('search')])->links('vendor.pagination.bootstrap-4') !!}
        </div>
    </div>
@endsection
