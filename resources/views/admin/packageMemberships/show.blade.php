@extends('layouts.app', [
'class' => '',
'elementActive' => 'packageMemberships'
])

<style>
    .table-responsive {
        overflow: hidden !important;
    }

    .table-hover td:nth-of-type(1) {
        width: 250px;
    }

</style>
@section('content')
    <div class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('updated'))
            <div class="alert alert-warning text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('delete'))
            <div class="alert alert-danger text-right">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            <h2> عرض الباقة</h2>
                        </div>
                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('packageMemberships.index') }}"> العودة</a>
                        </div>
                    </div>
                </div>

                <div class=" table-hover table-responsive text-right">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>الاسم بالانجليزي: </strong>
                                </td>
                                <td>
                                    {{ $packageMembership->name }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>الاسم بالعربي: </strong>
                                </td>
                                <td>
                                    {{ $packageMembership->name_ar }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>وصف الباقة بالانجليزي: </strong>
                                </td>
                                <td>
                                    {{ $packageMembership->details }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>وصف الباقة بالعربي: </strong>

                                </td>
                                <td>
                                    {{ $packageMembership->details_ar }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>سعر المباراة الذي تشمله الباقة: </strong>
                                </td>
                                <td>
                                    {{ $packageMembership->match_price }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>عدد المباريات: </strong>
                                </td>
                                <td>
                                    {{ $packageMembership->matches_number }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>سعر الباقة: </strong>
                                </td>
                                <td>
                                    {{ $packageMembership->price }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>مدة الباقة بالايام: </strong>
                                </td>
                                <td>
                                    {{ $packageMembership->duration_in_days }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>الترتيب: </strong>
                                </td>
                                <td>
                                    {{ $packageMembership->position }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>الدولة: </strong>
                                </td>
                                <td>
                                    {{ $packageMembership->country->name_ar }} -
                                    {{ $packageMembership->country->name }}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>المدينة: </strong>
                                </td>
                                <td>
                                    {{ $packageMembership->city->name_ar }} - {{ $packageMembership->city->name }}

                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <strong>صورة الخلفية: </strong>
                                </td>
                                <td>
                                    <img src="{{ asset('storage/images/packageMemberships/' . $packageMembership->background_image) }}"
                                        width="200px">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection
