@extends('layouts.app', [
'class' => '',
'elementActive' => 'packageMemberships'
])
@section('content')

    <div class="content">

        <div class="card">
            <div class="card-header">
                <div class="pull-right">
                    <h5 class="title">{{ __('أضف دولة جديدة') }}</h5>
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ route('packageMemberships.index') }}"> العودة</a>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger text-right">
                    <strong>عذرًا! بعض المدخلات غير صحيحة</strong><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form class="col-md-12" action="{{ route('packageMemberships.update', $packageMembership->id) }}"
                method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-row text-right">
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم الباقة بالانجليزي') }}</label>
                            <input value="{{ $packageMembership->name }}" type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('اسم الباقة بالعربي') }}</label>
                            <input value="{{ $packageMembership->name_ar }}" type="text" name="name_ar"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('وصف الباقة بالانجليزي') }}</label>
                            <input value="{{ $packageMembership->details }}" type="text" name="details"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('وصف الباقة بالعربي') }}</label>
                            <input value="{{ $packageMembership->details_ar }}" type="text" name="details_ar"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('سعر المباراة الذي تشمله الباقة') }}</label>
                            <input value="{{ $packageMembership->match_price }}" type="text" name="match_price"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('عدد المباريات') }}</label>
                            <input value="{{ $packageMembership->matches_number }}" type="text" name="matches_number"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('سعر الباقة') }}</label>
                            <input value="{{ $packageMembership->price }}" type="text" name="price" class="form-control">
                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('مدة الباقة بالايام') }}</label>
                            <input value="{{ $packageMembership->duration_in_days }}" type="text" name="duration_in_days"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-12">
                            <label>{{ __('الترتيب') }}</label>
                            <input value="{{ $packageMembership->position }}" type="text" name="position"
                                class="form-control">
                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('الدولة') }}</label>
                            <select name="country_id" class="custom-select">
                   
                                @foreach ($countries as $country)
                                    <option value={{ $country->id }}>{{ $country->name_ar }} </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label>{{ __('المدينة') }}</label>
                            <select name="city_id" class="custom-select">
                          
                                @foreach ($cities as $city)
                                    <option value={{ $city->id }}>{{ $city->name_ar }} </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="file" style="cursor: pointer;">{{ __('اختر صورة الخلفية') }} <br> <i
                                    style="font-size:40px" class="nc-icon nc-image"></i>
                            </label>
                            <input type="file" accept="image/*" name="background_image" id="file" onchange="loadFile(event)"
                                style="display: none;">
                            <img id="output" width="200" />

                        </div>
                    </div>
                </div>



                <div class="card-footer ">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-info btn-round">{{ __('تعديل البيانات') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>




@endsection
@push('scripts')
    <script>
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
@endpush
